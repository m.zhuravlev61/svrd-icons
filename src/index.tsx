import React, { FunctionComponent } from 'react';

import blocks_light, { ReactComponent as BlocksLight } from './icons/blocks_light.svg';
import blocks_regular, { ReactComponent as BlocksRegular } from './icons/blocks_regular.svg';
import blocks_solid, { ReactComponent as BlocksSolid } from './icons/blocks_solid.svg';
import camera_light, { ReactComponent as CameraLight } from './icons/camera_light.svg';
import camera_regular, { ReactComponent as CameraRegular } from './icons/camera_regular.svg';
import camera_solid, { ReactComponent as CameraSolid } from './icons/camera_solid.svg';
import clock_light, { ReactComponent as ClockLight } from './icons/clock_light.svg';
import clock_regular, { ReactComponent as ClockRegular } from './icons/clock_regular.svg';
import clock_solid, { ReactComponent as ClockSolid } from './icons/clock_solid.svg';
import cloud_solid, { ReactComponent as CloudSolid } from './icons/cloud_solid.svg';
import cube_solid, { ReactComponent as CubeSolid } from './icons/cube_solid.svg';
import diogramm_regular, { ReactComponent as DiogrammRegular } from './icons/diogramm_regular.svg';
import home_light, { ReactComponent as HomeLight } from './icons/home_light.svg';
import home_regular, { ReactComponent as HomeRegular } from './icons/home_regular.svg';
import home_solid, { ReactComponent as HomeSolid } from './icons/home_solid.svg';
import images_regular, { ReactComponent as ImagesRegular } from './icons/images-regular.svg';
import issue_light, { ReactComponent as IssueLight } from './icons/issue_light.svg';
import issue_regular, { ReactComponent as IssueRegular } from './icons/issue_regular.svg';
import issue_solid, { ReactComponent as IssueSolid } from './icons/issue_solid.svg';
import prescription_light, { ReactComponent as PrescriptionLight } from './icons/prescription_light.svg';
import prescription_regular, { ReactComponent as PrescriptionRegular } from './icons/prescription_regular.svg';
import prescription_solid, { ReactComponent as PrescriptionSolid } from './icons/prescription_solid.svg';
import rub_light, { ReactComponent as RubLight } from './icons/rub_light.svg';
import rub_regular, { ReactComponent as RubRegular } from './icons/rub_regular.svg';
import rub_solid, { ReactComponent as RubSolid } from './icons/rub_solid.svg';
import user_light, { ReactComponent as UserLight } from './icons/user_light.svg';
import user_regular, { ReactComponent as UserRegular } from './icons/user_regular.svg';
import user_solid, { ReactComponent as UserSolid } from './icons/user_solid.svg';
import chart_bars, { ReactComponent as ChartBars } from './icons/chart_bars.svg';
import chart_line from './icons/chart_line.svg';
import doughnut_chart, { ReactComponent as DoughnutChart } from './icons/doughnut_chart.svg';
import person_message, { ReactComponent as PersonMessage } from './icons/person_message.svg';
import piechart, { ReactComponent as Piechart } from './icons/piechart.svg';
import webcam, { ReactComponent as Webcam } from './icons/webcam.svg';
import important, { ReactComponent as Important } from './icons/important.svg';
import palette_solid, { ReactComponent as PaletteSolid } from './icons/palette-solid.svg';
import truck, { ReactComponent as Truck } from './icons/reconstruction/truck.svg';
import crane, { ReactComponent as Crane } from './icons/reconstruction/crane.svg';
import demolition, { ReactComponent as Demolition } from './icons/reconstruction/demolition.svg';
import electric, { ReactComponent as Electric } from './icons/reconstruction/electric.svg';
import finishing, { ReactComponent as Finishing } from './icons/reconstruction/finishing.svg';
import floorCement, { ReactComponent as FloorCement } from './icons/reconstruction/floorCement.svg';
import layers, { ReactComponent as Layers } from './icons/reconstruction/layers.svg';
import paintWall, { ReactComponent as PaintWall } from './icons/reconstruction/paintWall.svg';
import pneumaticHammer, { ReactComponent as PneumaticHammer } from './icons/reconstruction/pneumaticHammer.svg';
import plumbing, { ReactComponent as Plumbing } from './icons/reconstruction/plumbing.svg';
import tiles, { ReactComponent as Tiles } from './icons/reconstruction/tiles.svg';
import trashBin, { ReactComponent as TrashBin } from './icons/reconstruction/trashBin.svg';
import ventilation, { ReactComponent as Ventilation } from './icons/reconstruction/ventilation.svg';
import wall, { ReactComponent as Wall } from './icons/reconstruction/wall.svg';
import camera, { ReactComponent as Camera } from './icons/reconstruction/camera.svg';

interface WrapperProps extends React.SVGAttributes<SVGElement> {
  color?: string;
}

const getWrappedComponentWithStroke =
  (SvgComponent: React.StatelessComponent<React.SVGAttributes<SVGElement>>):FunctionComponent<WrapperProps> =>
    ({color, style, ...rest}) => (
      <SvgComponent {...rest} style={{ ...style, stroke: color || '#000' }} />
)

const getWrappedComponentFill =
  (SvgComponent: React.StatelessComponent<React.SVGAttributes<SVGElement>>):FunctionComponent<WrapperProps> =>
    ({color, style, ...rest}) => (
      <SvgComponent {...rest} style={{ ...style, fill: color || '#000', color: color || '#000' }} />
)

const CraneComponent = getWrappedComponentWithStroke(Crane);
const DemolitionComponent = getWrappedComponentWithStroke(Demolition);
const ElectricComponent = getWrappedComponentWithStroke(Electric);
const FinishingComponent = getWrappedComponentWithStroke(Finishing);
const FloorCementComponent = getWrappedComponentWithStroke(FloorCement);
const LayersComponent = getWrappedComponentWithStroke(Layers);
const PaintWallComponent = getWrappedComponentWithStroke(PaintWall);
const PneumaticHammerComponent = getWrappedComponentWithStroke(PneumaticHammer);
const PlumbingComponent = getWrappedComponentWithStroke(Plumbing);
const TilesComponent = getWrappedComponentWithStroke(Tiles);
const TrashBinComponent = getWrappedComponentWithStroke(TrashBin);
const VentilationComponent = getWrappedComponentWithStroke(Ventilation);
const WallComponent = getWrappedComponentWithStroke(Wall);
const CameraComponent = getWrappedComponentWithStroke(Camera);
const BlocksLightComponent = getWrappedComponentFill(BlocksLight);
const BlocksRegularComponent = getWrappedComponentFill(BlocksRegular);
const BlocksSolidComponent = getWrappedComponentFill(BlocksSolid);
const CameraLightComponent = getWrappedComponentFill(CameraLight);
const CameraRegularComponent = getWrappedComponentFill(CameraRegular);
const CameraSolidComponent = getWrappedComponentFill(CameraSolid);
const ClockLightComponent = getWrappedComponentFill(ClockLight);
const ClockRegularComponent = getWrappedComponentFill(ClockRegular);
const ClockSolidComponent = getWrappedComponentFill(ClockSolid);
const CloudSolidComponent = getWrappedComponentFill(CloudSolid);
const CubeSolidComponent = getWrappedComponentFill(CubeSolid);
const DiogrammRegularComponent = getWrappedComponentFill(DiogrammRegular);
const HomeLightComponent = getWrappedComponentFill(HomeLight);
const HomeRegularComponent = getWrappedComponentFill(HomeRegular);
const HomeSolidComponent = getWrappedComponentFill(HomeSolid);
const ImagesRegularComponent = getWrappedComponentFill(ImagesRegular);
const IssueLightComponent = getWrappedComponentFill(IssueLight);
const IssueRegularComponent = getWrappedComponentFill(IssueRegular);
const IssueSolidComponent = getWrappedComponentFill(IssueSolid);
const PrescriptionLightComponent = getWrappedComponentFill(PrescriptionLight);
const PrescriptionRegularComponent = getWrappedComponentFill(PrescriptionRegular);
const PrescriptionSolidComponent = getWrappedComponentFill(PrescriptionSolid);
const RubLightComponent = getWrappedComponentFill(RubLight);
const RubRegularComponent = getWrappedComponentFill(RubRegular);
const RubSolidComponent = getWrappedComponentFill(RubSolid);
const UserLightComponent = getWrappedComponentFill(UserLight);
const UserRegularComponent = getWrappedComponentFill(UserRegular);
const UserSolidComponent = getWrappedComponentFill(UserSolid);
const ChartBarsComponent = getWrappedComponentFill(ChartBars);
const DoughnutChartComponent = getWrappedComponentFill(DoughnutChart);
const PersonMessageComponent = getWrappedComponentFill(PersonMessage);
const PiechartComponent = getWrappedComponentFill(Piechart);
const WebcamComponent = getWrappedComponentFill(Webcam);
const ImportantComponent = getWrappedComponentFill(Important);
const PaletteSolidComponent = getWrappedComponentFill(PaletteSolid);
const TruckComponent = getWrappedComponentFill(Truck);

interface ChartLineProps extends React.SVGAttributes<SVGElement> {
  color?: string;
}

const ChartLine: FunctionComponent<ChartLineProps> = ({color, ...rest}) => (
  <svg {...rest} width="20px" height="14px" viewBox="0 0 20 14" version="1.1">
    <title>show_chart</title>
    <desc>Created with Sketch.</desc>
    <g id="Icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g id="Outlined" transform="translate(-510.000000, -2107.000000)">
            <g id="Editor" transform="translate(100.000000, 1960.000000)">
                <g id="Outlined-/-Editor-/-show_chart" transform="translate(408.000000, 142.000000)">
                    <g>
                        <polygon id="Path" points="0 0 24 0 24 24 0 24"></polygon>
                        <polygon id="🔹-Icon-Color" fill={color || '#000'} points="3.5 18.49 9.5 12.48 13.5 16.48 22 6.92 20.59 5.51 13.5 13.48 9.5 9.48 2 16.99"></polygon>
                    </g>
                </g>
            </g>
        </g>
    </g>
  </svg>
)

export {
  ChartLine,
  BlocksLightComponent,
  BlocksRegularComponent,
  BlocksSolidComponent,
  CameraLightComponent,
  CameraRegularComponent,
  CameraSolidComponent,
  ClockLightComponent,
  ClockRegularComponent,
  ClockSolidComponent,
  CloudSolidComponent,
  CubeSolidComponent,
  DiogrammRegularComponent,
  HomeLightComponent,
  HomeRegularComponent,
  HomeSolidComponent,
  ImagesRegularComponent,
  IssueLightComponent,
  IssueRegularComponent,
  IssueSolidComponent,
  PrescriptionLightComponent,
  PrescriptionRegularComponent,
  PrescriptionSolidComponent,
  RubLightComponent,
  RubRegularComponent,
  RubSolidComponent,
  UserLightComponent,
  UserRegularComponent,
  UserSolidComponent,
  ChartBarsComponent,
  DoughnutChartComponent,
  PersonMessageComponent,
  PiechartComponent,
  WebcamComponent,
  ImportantComponent,
  PaletteSolidComponent,
  TruckComponent,
  CraneComponent,
  DemolitionComponent,
  ElectricComponent,
  FinishingComponent,
  FloorCementComponent,
  LayersComponent,
  PaintWallComponent,
  PneumaticHammerComponent,
  PlumbingComponent,
  TilesComponent,
  TrashBinComponent,
  VentilationComponent,
  WallComponent,
  CameraComponent,
};

export default '';

export interface Icon {
  src: string;
  Component: FunctionComponent<WrapperProps>;
}

export const iconNames = [
    'blocksLight',
    'blocksRegular',
    'blocksSolid',
    'cameraLight',
    'cameraRegular',
    'cameraSolid',
    'clockLight',
    'clockRegular',
    'clockSolid',
    'cloudSolid',
    'cubeSolid',
    'diagramRegular',
    'homeLight',
    'homeRegular',
    'homeSolid',
    'imagesRegular',
    'issueLight',
    'issueRegular',
    'issueSolid',
    'prescriptionLight',
    'prescriptionRegular',
    'prescriptionSolid',
    'rubLight',
    'rubRegular',
    'rubSolid',
    'userLight',
    'userRegular',
    'userSolid',
    'chartBars',
    'chartLine',
    'doughnutChart',
    'personMessage',
    'piechart',
    'webcam',
    'important',
    'paletteSolid',
    'truck',
    'camera',
    'demolition',
    'finishing',
    'layers',
    'pneumaticHammer',
    'tiles',
    'wall',
    'crane',
    'electric',
    'floorCement',
    'paintWall',
    'plumbing',
    'trashBin',
    'ventilation'] as const;

export type IconNames = typeof iconNames[number];

export type CardIcons = Record<IconNames, Icon>;

const iconsMap: CardIcons = {
  chartBars: { src: chart_bars, Component: ChartBarsComponent },
  chartLine: { src: chart_line, Component: ChartLine },
  doughnutChart: { src: doughnut_chart, Component: DoughnutChartComponent },
  personMessage: { src: person_message, Component: PersonMessageComponent },
  piechart: { src: piechart, Component: PiechartComponent },
  webcam: { src: webcam, Component: WebcamComponent},
  important: { src: important, Component: ImportantComponent },
  blocksLight: { src: blocks_light, Component: BlocksLightComponent },
  blocksRegular: { src: blocks_regular, Component: BlocksRegularComponent },
  blocksSolid: { src: blocks_solid, Component: BlocksSolidComponent },
  cameraLight: { src: camera_light, Component: CameraLightComponent },
  cameraRegular: { src: camera_regular, Component: CameraRegularComponent },
  cameraSolid: { src: camera_solid, Component: CameraSolidComponent },
  clockLight: { src: clock_light, Component: ClockLightComponent },
  clockRegular: { src: clock_regular, Component: ClockRegularComponent },
  clockSolid: { src: clock_solid, Component: ClockSolidComponent },
  cloudSolid: { src: cloud_solid, Component: CloudSolidComponent },
  cubeSolid: { src: cube_solid, Component: CubeSolidComponent },
  diagramRegular: { src: diogramm_regular, Component:  DiogrammRegularComponent },
  homeLight: { src: home_light, Component: HomeLightComponent },
  homeRegular: { src: home_regular, Component: HomeRegularComponent },
  homeSolid: { src: home_solid, Component: HomeSolidComponent},
  imagesRegular: { src: images_regular, Component: ImagesRegularComponent },
  issueLight: { src: issue_light, Component: IssueLightComponent },
  issueRegular: { src: issue_regular, Component: IssueRegularComponent },
  issueSolid: { src: issue_solid, Component: IssueSolidComponent },
  prescriptionLight: { src: prescription_light, Component: PrescriptionLightComponent },
  prescriptionRegular: { src: prescription_regular, Component: PrescriptionRegularComponent },
  prescriptionSolid: { src: prescription_solid, Component: PrescriptionSolidComponent },
  rubLight: { src: rub_light, Component: RubLightComponent },
  rubRegular: { src: rub_regular, Component: RubRegularComponent },
  rubSolid: { src: rub_solid, Component: RubSolidComponent },
  userLight: { src: user_light, Component: UserLightComponent },
  userRegular: { src: user_regular, Component: UserRegularComponent },
  userSolid: { src: user_solid, Component: UserSolidComponent },
  paletteSolid: { src: palette_solid, Component: PaletteSolidComponent },
  truck: { src: truck, Component: TruckComponent },
  camera: { src: camera, Component: CameraComponent },
  demolition: { src: demolition, Component: DemolitionComponent },
  finishing: { src: finishing, Component: FinishingComponent },
  layers: { src: layers, Component: LayersComponent },
  pneumaticHammer: { src: pneumaticHammer, Component: PneumaticHammerComponent },
  tiles: { src: tiles, Component: TilesComponent },
  wall: { src: wall, Component: WallComponent },
  crane: { src: crane, Component: CraneComponent },
  electric: { src: electric, Component: ElectricComponent },
  floorCement: { src: floorCement, Component: FloorCementComponent },
  paintWall: { src: paintWall, Component: PaintWallComponent },
  plumbing: { src: plumbing, Component: PlumbingComponent },
  trashBin: { src: trashBin, Component: TrashBinComponent },
  ventilation: { src: ventilation, Component: VentilationComponent },
} as const;


export const getIcons = (): CardIcons => iconsMap
