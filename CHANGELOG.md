# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.12](https://gitlab.com/m.zhuravlev61/svrd-icons/compare/v1.0.11...v1.0.12) (2020-04-13)

### [1.0.11](https://gitlab.com/m.zhuravlev61/svrd-icons/compare/v1.0.10...v1.0.11) (2020-04-13)

### [1.0.10](https://gitlab.com/m.zhuravlev61/svrd-icons/compare/v1.0.9...v1.0.10) (2020-04-13)

### [1.0.9](https://gitlab.com/m.zhuravlev61/svrd-icons/compare/v1.0.8...v1.0.9) (2020-04-08)

### [1.0.8](https://gitlab.com/m.zhuravlev61/svrd-icons/compare/v1.0.7...v1.0.8) (2020-04-08)

### 1.0.7 (2020-04-08)
