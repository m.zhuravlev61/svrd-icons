import React from 'react';

var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var check = function (it) {
  return it && it.Math == Math && it;
};

// https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
var global_1 =
  // eslint-disable-next-line no-undef
  check(typeof globalThis == 'object' && globalThis) ||
  check(typeof window == 'object' && window) ||
  check(typeof self == 'object' && self) ||
  check(typeof commonjsGlobal == 'object' && commonjsGlobal) ||
  // eslint-disable-next-line no-new-func
  Function('return this')();

var fails = function (exec) {
  try {
    return !!exec();
  } catch (error) {
    return true;
  }
};

// Thank's IE8 for his funny defineProperty
var descriptors = !fails(function () {
  return Object.defineProperty({}, 1, { get: function () { return 7; } })[1] != 7;
});

var nativePropertyIsEnumerable = {}.propertyIsEnumerable;
var getOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

// Nashorn ~ JDK8 bug
var NASHORN_BUG = getOwnPropertyDescriptor && !nativePropertyIsEnumerable.call({ 1: 2 }, 1);

// `Object.prototype.propertyIsEnumerable` method implementation
// https://tc39.github.io/ecma262/#sec-object.prototype.propertyisenumerable
var f = NASHORN_BUG ? function propertyIsEnumerable(V) {
  var descriptor = getOwnPropertyDescriptor(this, V);
  return !!descriptor && descriptor.enumerable;
} : nativePropertyIsEnumerable;

var objectPropertyIsEnumerable = {
	f: f
};

var createPropertyDescriptor = function (bitmap, value) {
  return {
    enumerable: !(bitmap & 1),
    configurable: !(bitmap & 2),
    writable: !(bitmap & 4),
    value: value
  };
};

var toString = {}.toString;

var classofRaw = function (it) {
  return toString.call(it).slice(8, -1);
};

var split = ''.split;

// fallback for non-array-like ES3 and non-enumerable old V8 strings
var indexedObject = fails(function () {
  // throws an error in rhino, see https://github.com/mozilla/rhino/issues/346
  // eslint-disable-next-line no-prototype-builtins
  return !Object('z').propertyIsEnumerable(0);
}) ? function (it) {
  return classofRaw(it) == 'String' ? split.call(it, '') : Object(it);
} : Object;

// `RequireObjectCoercible` abstract operation
// https://tc39.github.io/ecma262/#sec-requireobjectcoercible
var requireObjectCoercible = function (it) {
  if (it == undefined) throw TypeError("Can't call method on " + it);
  return it;
};

// toObject with fallback for non-array-like ES3 strings



var toIndexedObject = function (it) {
  return indexedObject(requireObjectCoercible(it));
};

var isObject = function (it) {
  return typeof it === 'object' ? it !== null : typeof it === 'function';
};

// `ToPrimitive` abstract operation
// https://tc39.github.io/ecma262/#sec-toprimitive
// instead of the ES6 spec version, we didn't implement @@toPrimitive case
// and the second argument - flag - preferred type is a string
var toPrimitive = function (input, PREFERRED_STRING) {
  if (!isObject(input)) return input;
  var fn, val;
  if (PREFERRED_STRING && typeof (fn = input.toString) == 'function' && !isObject(val = fn.call(input))) return val;
  if (typeof (fn = input.valueOf) == 'function' && !isObject(val = fn.call(input))) return val;
  if (!PREFERRED_STRING && typeof (fn = input.toString) == 'function' && !isObject(val = fn.call(input))) return val;
  throw TypeError("Can't convert object to primitive value");
};

var hasOwnProperty = {}.hasOwnProperty;

var has = function (it, key) {
  return hasOwnProperty.call(it, key);
};

var document$1 = global_1.document;
// typeof document.createElement is 'object' in old IE
var EXISTS = isObject(document$1) && isObject(document$1.createElement);

var documentCreateElement = function (it) {
  return EXISTS ? document$1.createElement(it) : {};
};

// Thank's IE8 for his funny defineProperty
var ie8DomDefine = !descriptors && !fails(function () {
  return Object.defineProperty(documentCreateElement('div'), 'a', {
    get: function () { return 7; }
  }).a != 7;
});

var nativeGetOwnPropertyDescriptor = Object.getOwnPropertyDescriptor;

// `Object.getOwnPropertyDescriptor` method
// https://tc39.github.io/ecma262/#sec-object.getownpropertydescriptor
var f$1 = descriptors ? nativeGetOwnPropertyDescriptor : function getOwnPropertyDescriptor(O, P) {
  O = toIndexedObject(O);
  P = toPrimitive(P, true);
  if (ie8DomDefine) try {
    return nativeGetOwnPropertyDescriptor(O, P);
  } catch (error) { /* empty */ }
  if (has(O, P)) return createPropertyDescriptor(!objectPropertyIsEnumerable.f.call(O, P), O[P]);
};

var objectGetOwnPropertyDescriptor = {
	f: f$1
};

var replacement = /#|\.prototype\./;

var isForced = function (feature, detection) {
  var value = data[normalize(feature)];
  return value == POLYFILL ? true
    : value == NATIVE ? false
    : typeof detection == 'function' ? fails(detection)
    : !!detection;
};

var normalize = isForced.normalize = function (string) {
  return String(string).replace(replacement, '.').toLowerCase();
};

var data = isForced.data = {};
var NATIVE = isForced.NATIVE = 'N';
var POLYFILL = isForced.POLYFILL = 'P';

var isForced_1 = isForced;

var path = {};

var aFunction = function (it) {
  if (typeof it != 'function') {
    throw TypeError(String(it) + ' is not a function');
  } return it;
};

// optional / simple context binding
var functionBindContext = function (fn, that, length) {
  aFunction(fn);
  if (that === undefined) return fn;
  switch (length) {
    case 0: return function () {
      return fn.call(that);
    };
    case 1: return function (a) {
      return fn.call(that, a);
    };
    case 2: return function (a, b) {
      return fn.call(that, a, b);
    };
    case 3: return function (a, b, c) {
      return fn.call(that, a, b, c);
    };
  }
  return function (/* ...args */) {
    return fn.apply(that, arguments);
  };
};

var anObject = function (it) {
  if (!isObject(it)) {
    throw TypeError(String(it) + ' is not an object');
  } return it;
};

var nativeDefineProperty = Object.defineProperty;

// `Object.defineProperty` method
// https://tc39.github.io/ecma262/#sec-object.defineproperty
var f$2 = descriptors ? nativeDefineProperty : function defineProperty(O, P, Attributes) {
  anObject(O);
  P = toPrimitive(P, true);
  anObject(Attributes);
  if (ie8DomDefine) try {
    return nativeDefineProperty(O, P, Attributes);
  } catch (error) { /* empty */ }
  if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported');
  if ('value' in Attributes) O[P] = Attributes.value;
  return O;
};

var objectDefineProperty = {
	f: f$2
};

var createNonEnumerableProperty = descriptors ? function (object, key, value) {
  return objectDefineProperty.f(object, key, createPropertyDescriptor(1, value));
} : function (object, key, value) {
  object[key] = value;
  return object;
};

var getOwnPropertyDescriptor$1 = objectGetOwnPropertyDescriptor.f;






var wrapConstructor = function (NativeConstructor) {
  var Wrapper = function (a, b, c) {
    if (this instanceof NativeConstructor) {
      switch (arguments.length) {
        case 0: return new NativeConstructor();
        case 1: return new NativeConstructor(a);
        case 2: return new NativeConstructor(a, b);
      } return new NativeConstructor(a, b, c);
    } return NativeConstructor.apply(this, arguments);
  };
  Wrapper.prototype = NativeConstructor.prototype;
  return Wrapper;
};

/*
  options.target      - name of the target object
  options.global      - target is the global object
  options.stat        - export as static methods of target
  options.proto       - export as prototype methods of target
  options.real        - real prototype method for the `pure` version
  options.forced      - export even if the native feature is available
  options.bind        - bind methods to the target, required for the `pure` version
  options.wrap        - wrap constructors to preventing global pollution, required for the `pure` version
  options.unsafe      - use the simple assignment of property instead of delete + defineProperty
  options.sham        - add a flag to not completely full polyfills
  options.enumerable  - export as enumerable property
  options.noTargetGet - prevent calling a getter on target
*/
var _export = function (options, source) {
  var TARGET = options.target;
  var GLOBAL = options.global;
  var STATIC = options.stat;
  var PROTO = options.proto;

  var nativeSource = GLOBAL ? global_1 : STATIC ? global_1[TARGET] : (global_1[TARGET] || {}).prototype;

  var target = GLOBAL ? path : path[TARGET] || (path[TARGET] = {});
  var targetPrototype = target.prototype;

  var FORCED, USE_NATIVE, VIRTUAL_PROTOTYPE;
  var key, sourceProperty, targetProperty, nativeProperty, resultProperty, descriptor;

  for (key in source) {
    FORCED = isForced_1(GLOBAL ? key : TARGET + (STATIC ? '.' : '#') + key, options.forced);
    // contains in native
    USE_NATIVE = !FORCED && nativeSource && has(nativeSource, key);

    targetProperty = target[key];

    if (USE_NATIVE) if (options.noTargetGet) {
      descriptor = getOwnPropertyDescriptor$1(nativeSource, key);
      nativeProperty = descriptor && descriptor.value;
    } else nativeProperty = nativeSource[key];

    // export native or implementation
    sourceProperty = (USE_NATIVE && nativeProperty) ? nativeProperty : source[key];

    if (USE_NATIVE && typeof targetProperty === typeof sourceProperty) continue;

    // bind timers to global for call from export context
    if (options.bind && USE_NATIVE) resultProperty = functionBindContext(sourceProperty, global_1);
    // wrap global constructors for prevent changs in this version
    else if (options.wrap && USE_NATIVE) resultProperty = wrapConstructor(sourceProperty);
    // make static versions for prototype methods
    else if (PROTO && typeof sourceProperty == 'function') resultProperty = functionBindContext(Function.call, sourceProperty);
    // default case
    else resultProperty = sourceProperty;

    // add a flag to not completely full polyfills
    if (options.sham || (sourceProperty && sourceProperty.sham) || (targetProperty && targetProperty.sham)) {
      createNonEnumerableProperty(resultProperty, 'sham', true);
    }

    target[key] = resultProperty;

    if (PROTO) {
      VIRTUAL_PROTOTYPE = TARGET + 'Prototype';
      if (!has(path, VIRTUAL_PROTOTYPE)) {
        createNonEnumerableProperty(path, VIRTUAL_PROTOTYPE, {});
      }
      // export virtual prototype methods
      path[VIRTUAL_PROTOTYPE][key] = sourceProperty;
      // export real prototype methods
      if (options.real && targetPrototype && !targetPrototype[key]) {
        createNonEnumerableProperty(targetPrototype, key, sourceProperty);
      }
    }
  }
};

// `Object.defineProperty` method
// https://tc39.github.io/ecma262/#sec-object.defineproperty
_export({ target: 'Object', stat: true, forced: !descriptors, sham: !descriptors }, {
  defineProperty: objectDefineProperty.f
});

var defineProperty_1 = createCommonjsModule(function (module) {
var Object = path.Object;

var defineProperty = module.exports = function defineProperty(it, key, desc) {
  return Object.defineProperty(it, key, desc);
};

if (Object.defineProperty.sham) defineProperty.sham = true;
});

var defineProperty = defineProperty_1;

var defineProperty$1 = defineProperty;

var ceil = Math.ceil;
var floor = Math.floor;

// `ToInteger` abstract operation
// https://tc39.github.io/ecma262/#sec-tointeger
var toInteger = function (argument) {
  return isNaN(argument = +argument) ? 0 : (argument > 0 ? floor : ceil)(argument);
};

var min = Math.min;

// `ToLength` abstract operation
// https://tc39.github.io/ecma262/#sec-tolength
var toLength = function (argument) {
  return argument > 0 ? min(toInteger(argument), 0x1FFFFFFFFFFFFF) : 0; // 2 ** 53 - 1 == 9007199254740991
};

var max = Math.max;
var min$1 = Math.min;

// Helper for a popular repeating case of the spec:
// Let integer be ? ToInteger(index).
// If integer < 0, let result be max((length + integer), 0); else let result be min(integer, length).
var toAbsoluteIndex = function (index, length) {
  var integer = toInteger(index);
  return integer < 0 ? max(integer + length, 0) : min$1(integer, length);
};

// `Array.prototype.{ indexOf, includes }` methods implementation
var createMethod = function (IS_INCLUDES) {
  return function ($this, el, fromIndex) {
    var O = toIndexedObject($this);
    var length = toLength(O.length);
    var index = toAbsoluteIndex(fromIndex, length);
    var value;
    // Array#includes uses SameValueZero equality algorithm
    // eslint-disable-next-line no-self-compare
    if (IS_INCLUDES && el != el) while (length > index) {
      value = O[index++];
      // eslint-disable-next-line no-self-compare
      if (value != value) return true;
    // Array#indexOf ignores holes, Array#includes - not
    } else for (;length > index; index++) {
      if ((IS_INCLUDES || index in O) && O[index] === el) return IS_INCLUDES || index || 0;
    } return !IS_INCLUDES && -1;
  };
};

var arrayIncludes = {
  // `Array.prototype.includes` method
  // https://tc39.github.io/ecma262/#sec-array.prototype.includes
  includes: createMethod(true),
  // `Array.prototype.indexOf` method
  // https://tc39.github.io/ecma262/#sec-array.prototype.indexof
  indexOf: createMethod(false)
};

var hiddenKeys = {};

var indexOf = arrayIncludes.indexOf;


var objectKeysInternal = function (object, names) {
  var O = toIndexedObject(object);
  var i = 0;
  var result = [];
  var key;
  for (key in O) !has(hiddenKeys, key) && has(O, key) && result.push(key);
  // Don't enum bug & hidden keys
  while (names.length > i) if (has(O, key = names[i++])) {
    ~indexOf(result, key) || result.push(key);
  }
  return result;
};

// IE8- don't enum bug keys
var enumBugKeys = [
  'constructor',
  'hasOwnProperty',
  'isPrototypeOf',
  'propertyIsEnumerable',
  'toLocaleString',
  'toString',
  'valueOf'
];

// `Object.keys` method
// https://tc39.github.io/ecma262/#sec-object.keys
var objectKeys = Object.keys || function keys(O) {
  return objectKeysInternal(O, enumBugKeys);
};

// `Object.defineProperties` method
// https://tc39.github.io/ecma262/#sec-object.defineproperties
var objectDefineProperties = descriptors ? Object.defineProperties : function defineProperties(O, Properties) {
  anObject(O);
  var keys = objectKeys(Properties);
  var length = keys.length;
  var index = 0;
  var key;
  while (length > index) objectDefineProperty.f(O, key = keys[index++], Properties[key]);
  return O;
};

// `Object.defineProperties` method
// https://tc39.github.io/ecma262/#sec-object.defineproperties
_export({ target: 'Object', stat: true, forced: !descriptors, sham: !descriptors }, {
  defineProperties: objectDefineProperties
});

var defineProperties_1 = createCommonjsModule(function (module) {
var Object = path.Object;

var defineProperties = module.exports = function defineProperties(T, D) {
  return Object.defineProperties(T, D);
};

if (Object.defineProperties.sham) defineProperties.sham = true;
});

var defineProperties = defineProperties_1;

var defineProperties$1 = defineProperties;

var aFunction$1 = function (variable) {
  return typeof variable == 'function' ? variable : undefined;
};

var getBuiltIn = function (namespace, method) {
  return arguments.length < 2 ? aFunction$1(path[namespace]) || aFunction$1(global_1[namespace])
    : path[namespace] && path[namespace][method] || global_1[namespace] && global_1[namespace][method];
};

var hiddenKeys$1 = enumBugKeys.concat('length', 'prototype');

// `Object.getOwnPropertyNames` method
// https://tc39.github.io/ecma262/#sec-object.getownpropertynames
var f$3 = Object.getOwnPropertyNames || function getOwnPropertyNames(O) {
  return objectKeysInternal(O, hiddenKeys$1);
};

var objectGetOwnPropertyNames = {
	f: f$3
};

var f$4 = Object.getOwnPropertySymbols;

var objectGetOwnPropertySymbols = {
	f: f$4
};

// all object keys, includes non-enumerable and symbols
var ownKeys = getBuiltIn('Reflect', 'ownKeys') || function ownKeys(it) {
  var keys = objectGetOwnPropertyNames.f(anObject(it));
  var getOwnPropertySymbols = objectGetOwnPropertySymbols.f;
  return getOwnPropertySymbols ? keys.concat(getOwnPropertySymbols(it)) : keys;
};

var createProperty = function (object, key, value) {
  var propertyKey = toPrimitive(key);
  if (propertyKey in object) objectDefineProperty.f(object, propertyKey, createPropertyDescriptor(0, value));
  else object[propertyKey] = value;
};

// `Object.getOwnPropertyDescriptors` method
// https://tc39.github.io/ecma262/#sec-object.getownpropertydescriptors
_export({ target: 'Object', stat: true, sham: !descriptors }, {
  getOwnPropertyDescriptors: function getOwnPropertyDescriptors(object) {
    var O = toIndexedObject(object);
    var getOwnPropertyDescriptor = objectGetOwnPropertyDescriptor.f;
    var keys = ownKeys(O);
    var result = {};
    var index = 0;
    var key, descriptor;
    while (keys.length > index) {
      descriptor = getOwnPropertyDescriptor(O, key = keys[index++]);
      if (descriptor !== undefined) createProperty(result, key, descriptor);
    }
    return result;
  }
});

var getOwnPropertyDescriptors = path.Object.getOwnPropertyDescriptors;

var getOwnPropertyDescriptors$1 = getOwnPropertyDescriptors;

var getOwnPropertyDescriptors$2 = getOwnPropertyDescriptors$1;

var setGlobal = function (key, value) {
  try {
    createNonEnumerableProperty(global_1, key, value);
  } catch (error) {
    global_1[key] = value;
  } return value;
};

var SHARED = '__core-js_shared__';
var store = global_1[SHARED] || setGlobal(SHARED, {});

var sharedStore = store;

var functionToString = Function.toString;

// this helper broken in `3.4.1-3.4.4`, so we can't use `shared` helper
if (typeof sharedStore.inspectSource != 'function') {
  sharedStore.inspectSource = function (it) {
    return functionToString.call(it);
  };
}

var inspectSource = sharedStore.inspectSource;

var WeakMap = global_1.WeakMap;

var nativeWeakMap = typeof WeakMap === 'function' && /native code/.test(inspectSource(WeakMap));

var shared = createCommonjsModule(function (module) {
(module.exports = function (key, value) {
  return sharedStore[key] || (sharedStore[key] = value !== undefined ? value : {});
})('versions', []).push({
  version: '3.6.4',
  mode:  'pure' ,
  copyright: '© 2020 Denis Pushkarev (zloirock.ru)'
});
});

var id = 0;
var postfix = Math.random();

var uid = function (key) {
  return 'Symbol(' + String(key === undefined ? '' : key) + ')_' + (++id + postfix).toString(36);
};

var keys = shared('keys');

var sharedKey = function (key) {
  return keys[key] || (keys[key] = uid(key));
};

var WeakMap$1 = global_1.WeakMap;
var set, get, has$1;

var enforce = function (it) {
  return has$1(it) ? get(it) : set(it, {});
};

var getterFor = function (TYPE) {
  return function (it) {
    var state;
    if (!isObject(it) || (state = get(it)).type !== TYPE) {
      throw TypeError('Incompatible receiver, ' + TYPE + ' required');
    } return state;
  };
};

if (nativeWeakMap) {
  var store$1 = new WeakMap$1();
  var wmget = store$1.get;
  var wmhas = store$1.has;
  var wmset = store$1.set;
  set = function (it, metadata) {
    wmset.call(store$1, it, metadata);
    return metadata;
  };
  get = function (it) {
    return wmget.call(store$1, it) || {};
  };
  has$1 = function (it) {
    return wmhas.call(store$1, it);
  };
} else {
  var STATE = sharedKey('state');
  hiddenKeys[STATE] = true;
  set = function (it, metadata) {
    createNonEnumerableProperty(it, STATE, metadata);
    return metadata;
  };
  get = function (it) {
    return has(it, STATE) ? it[STATE] : {};
  };
  has$1 = function (it) {
    return has(it, STATE);
  };
}

var internalState = {
  set: set,
  get: get,
  has: has$1,
  enforce: enforce,
  getterFor: getterFor
};

// `ToObject` abstract operation
// https://tc39.github.io/ecma262/#sec-toobject
var toObject = function (argument) {
  return Object(requireObjectCoercible(argument));
};

var correctPrototypeGetter = !fails(function () {
  function F() { /* empty */ }
  F.prototype.constructor = null;
  return Object.getPrototypeOf(new F()) !== F.prototype;
});

var IE_PROTO = sharedKey('IE_PROTO');
var ObjectPrototype = Object.prototype;

// `Object.getPrototypeOf` method
// https://tc39.github.io/ecma262/#sec-object.getprototypeof
var objectGetPrototypeOf = correctPrototypeGetter ? Object.getPrototypeOf : function (O) {
  O = toObject(O);
  if (has(O, IE_PROTO)) return O[IE_PROTO];
  if (typeof O.constructor == 'function' && O instanceof O.constructor) {
    return O.constructor.prototype;
  } return O instanceof Object ? ObjectPrototype : null;
};

var nativeSymbol = !!Object.getOwnPropertySymbols && !fails(function () {
  // Chrome 38 Symbol has incorrect toString conversion
  // eslint-disable-next-line no-undef
  return !String(Symbol());
});

var useSymbolAsUid = nativeSymbol
  // eslint-disable-next-line no-undef
  && !Symbol.sham
  // eslint-disable-next-line no-undef
  && typeof Symbol.iterator == 'symbol';

var WellKnownSymbolsStore = shared('wks');
var Symbol$1 = global_1.Symbol;
var createWellKnownSymbol = useSymbolAsUid ? Symbol$1 : Symbol$1 && Symbol$1.withoutSetter || uid;

var wellKnownSymbol = function (name) {
  if (!has(WellKnownSymbolsStore, name)) {
    if (nativeSymbol && has(Symbol$1, name)) WellKnownSymbolsStore[name] = Symbol$1[name];
    else WellKnownSymbolsStore[name] = createWellKnownSymbol('Symbol.' + name);
  } return WellKnownSymbolsStore[name];
};

var ITERATOR = wellKnownSymbol('iterator');
var BUGGY_SAFARI_ITERATORS = false;

// `%IteratorPrototype%` object
// https://tc39.github.io/ecma262/#sec-%iteratorprototype%-object
var IteratorPrototype, PrototypeOfArrayIteratorPrototype, arrayIterator;

if ([].keys) {
  arrayIterator = [].keys();
  // Safari 8 has buggy iterators w/o `next`
  if (!('next' in arrayIterator)) BUGGY_SAFARI_ITERATORS = true;
  else {
    PrototypeOfArrayIteratorPrototype = objectGetPrototypeOf(objectGetPrototypeOf(arrayIterator));
    if (PrototypeOfArrayIteratorPrototype !== Object.prototype) IteratorPrototype = PrototypeOfArrayIteratorPrototype;
  }
}

if (IteratorPrototype == undefined) IteratorPrototype = {};

var iteratorsCore = {
  IteratorPrototype: IteratorPrototype,
  BUGGY_SAFARI_ITERATORS: BUGGY_SAFARI_ITERATORS
};

var html = getBuiltIn('document', 'documentElement');

var GT = '>';
var LT = '<';
var PROTOTYPE = 'prototype';
var SCRIPT = 'script';
var IE_PROTO$1 = sharedKey('IE_PROTO');

var EmptyConstructor = function () { /* empty */ };

var scriptTag = function (content) {
  return LT + SCRIPT + GT + content + LT + '/' + SCRIPT + GT;
};

// Create object with fake `null` prototype: use ActiveX Object with cleared prototype
var NullProtoObjectViaActiveX = function (activeXDocument) {
  activeXDocument.write(scriptTag(''));
  activeXDocument.close();
  var temp = activeXDocument.parentWindow.Object;
  activeXDocument = null; // avoid memory leak
  return temp;
};

// Create object with fake `null` prototype: use iframe Object with cleared prototype
var NullProtoObjectViaIFrame = function () {
  // Thrash, waste and sodomy: IE GC bug
  var iframe = documentCreateElement('iframe');
  var JS = 'java' + SCRIPT + ':';
  var iframeDocument;
  iframe.style.display = 'none';
  html.appendChild(iframe);
  // https://github.com/zloirock/core-js/issues/475
  iframe.src = String(JS);
  iframeDocument = iframe.contentWindow.document;
  iframeDocument.open();
  iframeDocument.write(scriptTag('document.F=Object'));
  iframeDocument.close();
  return iframeDocument.F;
};

// Check for document.domain and active x support
// No need to use active x approach when document.domain is not set
// see https://github.com/es-shims/es5-shim/issues/150
// variation of https://github.com/kitcambridge/es5-shim/commit/4f738ac066346
// avoid IE GC bug
var activeXDocument;
var NullProtoObject = function () {
  try {
    /* global ActiveXObject */
    activeXDocument = document.domain && new ActiveXObject('htmlfile');
  } catch (error) { /* ignore */ }
  NullProtoObject = activeXDocument ? NullProtoObjectViaActiveX(activeXDocument) : NullProtoObjectViaIFrame();
  var length = enumBugKeys.length;
  while (length--) delete NullProtoObject[PROTOTYPE][enumBugKeys[length]];
  return NullProtoObject();
};

hiddenKeys[IE_PROTO$1] = true;

// `Object.create` method
// https://tc39.github.io/ecma262/#sec-object.create
var objectCreate = Object.create || function create(O, Properties) {
  var result;
  if (O !== null) {
    EmptyConstructor[PROTOTYPE] = anObject(O);
    result = new EmptyConstructor();
    EmptyConstructor[PROTOTYPE] = null;
    // add "__proto__" for Object.getPrototypeOf polyfill
    result[IE_PROTO$1] = O;
  } else result = NullProtoObject();
  return Properties === undefined ? result : objectDefineProperties(result, Properties);
};

var TO_STRING_TAG = wellKnownSymbol('toStringTag');
var test = {};

test[TO_STRING_TAG] = 'z';

var toStringTagSupport = String(test) === '[object z]';

var TO_STRING_TAG$1 = wellKnownSymbol('toStringTag');
// ES3 wrong here
var CORRECT_ARGUMENTS = classofRaw(function () { return arguments; }()) == 'Arguments';

// fallback for IE11 Script Access Denied error
var tryGet = function (it, key) {
  try {
    return it[key];
  } catch (error) { /* empty */ }
};

// getting tag from ES6+ `Object.prototype.toString`
var classof = toStringTagSupport ? classofRaw : function (it) {
  var O, tag, result;
  return it === undefined ? 'Undefined' : it === null ? 'Null'
    // @@toStringTag case
    : typeof (tag = tryGet(O = Object(it), TO_STRING_TAG$1)) == 'string' ? tag
    // builtinTag case
    : CORRECT_ARGUMENTS ? classofRaw(O)
    // ES3 arguments fallback
    : (result = classofRaw(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : result;
};

// `Object.prototype.toString` method implementation
// https://tc39.github.io/ecma262/#sec-object.prototype.tostring
var objectToString = toStringTagSupport ? {}.toString : function toString() {
  return '[object ' + classof(this) + ']';
};

var defineProperty$2 = objectDefineProperty.f;





var TO_STRING_TAG$2 = wellKnownSymbol('toStringTag');

var setToStringTag = function (it, TAG, STATIC, SET_METHOD) {
  if (it) {
    var target = STATIC ? it : it.prototype;
    if (!has(target, TO_STRING_TAG$2)) {
      defineProperty$2(target, TO_STRING_TAG$2, { configurable: true, value: TAG });
    }
    if (SET_METHOD && !toStringTagSupport) {
      createNonEnumerableProperty(target, 'toString', objectToString);
    }
  }
};

var IteratorPrototype$1 = iteratorsCore.IteratorPrototype;

var createIteratorConstructor = function (IteratorConstructor, NAME, next) {
  var TO_STRING_TAG = NAME + ' Iterator';
  IteratorConstructor.prototype = objectCreate(IteratorPrototype$1, { next: createPropertyDescriptor(1, next) });
  setToStringTag(IteratorConstructor, TO_STRING_TAG, false, true);
  return IteratorConstructor;
};

var aPossiblePrototype = function (it) {
  if (!isObject(it) && it !== null) {
    throw TypeError("Can't set " + String(it) + ' as a prototype');
  } return it;
};

// `Object.setPrototypeOf` method
// https://tc39.github.io/ecma262/#sec-object.setprototypeof
// Works with __proto__ only. Old v8 can't work with null proto objects.
/* eslint-disable no-proto */
var objectSetPrototypeOf = Object.setPrototypeOf || ('__proto__' in {} ? function () {
  var CORRECT_SETTER = false;
  var test = {};
  var setter;
  try {
    setter = Object.getOwnPropertyDescriptor(Object.prototype, '__proto__').set;
    setter.call(test, []);
    CORRECT_SETTER = test instanceof Array;
  } catch (error) { /* empty */ }
  return function setPrototypeOf(O, proto) {
    anObject(O);
    aPossiblePrototype(proto);
    if (CORRECT_SETTER) setter.call(O, proto);
    else O.__proto__ = proto;
    return O;
  };
}() : undefined);

var redefine = function (target, key, value, options) {
  if (options && options.enumerable) target[key] = value;
  else createNonEnumerableProperty(target, key, value);
};

var IteratorPrototype$2 = iteratorsCore.IteratorPrototype;
var BUGGY_SAFARI_ITERATORS$1 = iteratorsCore.BUGGY_SAFARI_ITERATORS;
var ITERATOR$1 = wellKnownSymbol('iterator');
var KEYS = 'keys';
var VALUES = 'values';
var ENTRIES = 'entries';

var defineIterator = function (Iterable, NAME, IteratorConstructor, next, DEFAULT, IS_SET, FORCED) {
  createIteratorConstructor(IteratorConstructor, NAME, next);

  var getIterationMethod = function (KIND) {
    if (KIND === DEFAULT && defaultIterator) return defaultIterator;
    if (!BUGGY_SAFARI_ITERATORS$1 && KIND in IterablePrototype) return IterablePrototype[KIND];
    switch (KIND) {
      case KEYS: return function keys() { return new IteratorConstructor(this, KIND); };
      case VALUES: return function values() { return new IteratorConstructor(this, KIND); };
      case ENTRIES: return function entries() { return new IteratorConstructor(this, KIND); };
    } return function () { return new IteratorConstructor(this); };
  };

  var TO_STRING_TAG = NAME + ' Iterator';
  var INCORRECT_VALUES_NAME = false;
  var IterablePrototype = Iterable.prototype;
  var nativeIterator = IterablePrototype[ITERATOR$1]
    || IterablePrototype['@@iterator']
    || DEFAULT && IterablePrototype[DEFAULT];
  var defaultIterator = !BUGGY_SAFARI_ITERATORS$1 && nativeIterator || getIterationMethod(DEFAULT);
  var anyNativeIterator = NAME == 'Array' ? IterablePrototype.entries || nativeIterator : nativeIterator;
  var CurrentIteratorPrototype, methods, KEY;

  // fix native
  if (anyNativeIterator) {
    CurrentIteratorPrototype = objectGetPrototypeOf(anyNativeIterator.call(new Iterable()));
    if (IteratorPrototype$2 !== Object.prototype && CurrentIteratorPrototype.next) {
      // Set @@toStringTag to native iterators
      setToStringTag(CurrentIteratorPrototype, TO_STRING_TAG, true, true);
    }
  }

  // fix Array#{values, @@iterator}.name in V8 / FF
  if (DEFAULT == VALUES && nativeIterator && nativeIterator.name !== VALUES) {
    INCORRECT_VALUES_NAME = true;
    defaultIterator = function values() { return nativeIterator.call(this); };
  }

  // define iterator
  if (( FORCED) && IterablePrototype[ITERATOR$1] !== defaultIterator) {
    createNonEnumerableProperty(IterablePrototype, ITERATOR$1, defaultIterator);
  }

  // export additional methods
  if (DEFAULT) {
    methods = {
      values: getIterationMethod(VALUES),
      keys: IS_SET ? defaultIterator : getIterationMethod(KEYS),
      entries: getIterationMethod(ENTRIES)
    };
    if (FORCED) for (KEY in methods) {
      if (BUGGY_SAFARI_ITERATORS$1 || INCORRECT_VALUES_NAME || !(KEY in IterablePrototype)) {
        redefine(IterablePrototype, KEY, methods[KEY]);
      }
    } else _export({ target: NAME, proto: true, forced: BUGGY_SAFARI_ITERATORS$1 || INCORRECT_VALUES_NAME }, methods);
  }

  return methods;
};

var ARRAY_ITERATOR = 'Array Iterator';
var setInternalState = internalState.set;
var getInternalState = internalState.getterFor(ARRAY_ITERATOR);

// `Array.prototype.entries` method
// https://tc39.github.io/ecma262/#sec-array.prototype.entries
// `Array.prototype.keys` method
// https://tc39.github.io/ecma262/#sec-array.prototype.keys
// `Array.prototype.values` method
// https://tc39.github.io/ecma262/#sec-array.prototype.values
// `Array.prototype[@@iterator]` method
// https://tc39.github.io/ecma262/#sec-array.prototype-@@iterator
// `CreateArrayIterator` internal method
// https://tc39.github.io/ecma262/#sec-createarrayiterator
var es_array_iterator = defineIterator(Array, 'Array', function (iterated, kind) {
  setInternalState(this, {
    type: ARRAY_ITERATOR,
    target: toIndexedObject(iterated), // target
    index: 0,                          // next index
    kind: kind                         // kind
  });
// `%ArrayIteratorPrototype%.next` method
// https://tc39.github.io/ecma262/#sec-%arrayiteratorprototype%.next
}, function () {
  var state = getInternalState(this);
  var target = state.target;
  var kind = state.kind;
  var index = state.index++;
  if (!target || index >= target.length) {
    state.target = undefined;
    return { value: undefined, done: true };
  }
  if (kind == 'keys') return { value: index, done: false };
  if (kind == 'values') return { value: target[index], done: false };
  return { value: [index, target[index]], done: false };
}, 'values');

// iterable DOM collections
// flag - `iterable` interface - 'entries', 'keys', 'values', 'forEach' methods
var domIterables = {
  CSSRuleList: 0,
  CSSStyleDeclaration: 0,
  CSSValueList: 0,
  ClientRectList: 0,
  DOMRectList: 0,
  DOMStringList: 0,
  DOMTokenList: 1,
  DataTransferItemList: 0,
  FileList: 0,
  HTMLAllCollection: 0,
  HTMLCollection: 0,
  HTMLFormElement: 0,
  HTMLSelectElement: 0,
  MediaList: 0,
  MimeTypeArray: 0,
  NamedNodeMap: 0,
  NodeList: 1,
  PaintRequestList: 0,
  Plugin: 0,
  PluginArray: 0,
  SVGLengthList: 0,
  SVGNumberList: 0,
  SVGPathSegList: 0,
  SVGPointList: 0,
  SVGStringList: 0,
  SVGTransformList: 0,
  SourceBufferList: 0,
  StyleSheetList: 0,
  TextTrackCueList: 0,
  TextTrackList: 0,
  TouchList: 0
};

var TO_STRING_TAG$3 = wellKnownSymbol('toStringTag');

for (var COLLECTION_NAME in domIterables) {
  var Collection = global_1[COLLECTION_NAME];
  var CollectionPrototype = Collection && Collection.prototype;
  if (CollectionPrototype && classof(CollectionPrototype) !== TO_STRING_TAG$3) {
    createNonEnumerableProperty(CollectionPrototype, TO_STRING_TAG$3, COLLECTION_NAME);
  }
}

// `IsArray` abstract operation
// https://tc39.github.io/ecma262/#sec-isarray
var isArray = Array.isArray || function isArray(arg) {
  return classofRaw(arg) == 'Array';
};

var SPECIES = wellKnownSymbol('species');

// `ArraySpeciesCreate` abstract operation
// https://tc39.github.io/ecma262/#sec-arrayspeciescreate
var arraySpeciesCreate = function (originalArray, length) {
  var C;
  if (isArray(originalArray)) {
    C = originalArray.constructor;
    // cross-realm fallback
    if (typeof C == 'function' && (C === Array || isArray(C.prototype))) C = undefined;
    else if (isObject(C)) {
      C = C[SPECIES];
      if (C === null) C = undefined;
    }
  } return new (C === undefined ? Array : C)(length === 0 ? 0 : length);
};

var push = [].push;

// `Array.prototype.{ forEach, map, filter, some, every, find, findIndex }` methods implementation
var createMethod$1 = function (TYPE) {
  var IS_MAP = TYPE == 1;
  var IS_FILTER = TYPE == 2;
  var IS_SOME = TYPE == 3;
  var IS_EVERY = TYPE == 4;
  var IS_FIND_INDEX = TYPE == 6;
  var NO_HOLES = TYPE == 5 || IS_FIND_INDEX;
  return function ($this, callbackfn, that, specificCreate) {
    var O = toObject($this);
    var self = indexedObject(O);
    var boundFunction = functionBindContext(callbackfn, that, 3);
    var length = toLength(self.length);
    var index = 0;
    var create = specificCreate || arraySpeciesCreate;
    var target = IS_MAP ? create($this, length) : IS_FILTER ? create($this, 0) : undefined;
    var value, result;
    for (;length > index; index++) if (NO_HOLES || index in self) {
      value = self[index];
      result = boundFunction(value, index, O);
      if (TYPE) {
        if (IS_MAP) target[index] = result; // map
        else if (result) switch (TYPE) {
          case 3: return true;              // some
          case 5: return value;             // find
          case 6: return index;             // findIndex
          case 2: push.call(target, value); // filter
        } else if (IS_EVERY) return false;  // every
      }
    }
    return IS_FIND_INDEX ? -1 : IS_SOME || IS_EVERY ? IS_EVERY : target;
  };
};

var arrayIteration = {
  // `Array.prototype.forEach` method
  // https://tc39.github.io/ecma262/#sec-array.prototype.foreach
  forEach: createMethod$1(0),
  // `Array.prototype.map` method
  // https://tc39.github.io/ecma262/#sec-array.prototype.map
  map: createMethod$1(1),
  // `Array.prototype.filter` method
  // https://tc39.github.io/ecma262/#sec-array.prototype.filter
  filter: createMethod$1(2),
  // `Array.prototype.some` method
  // https://tc39.github.io/ecma262/#sec-array.prototype.some
  some: createMethod$1(3),
  // `Array.prototype.every` method
  // https://tc39.github.io/ecma262/#sec-array.prototype.every
  every: createMethod$1(4),
  // `Array.prototype.find` method
  // https://tc39.github.io/ecma262/#sec-array.prototype.find
  find: createMethod$1(5),
  // `Array.prototype.findIndex` method
  // https://tc39.github.io/ecma262/#sec-array.prototype.findIndex
  findIndex: createMethod$1(6)
};

var arrayMethodIsStrict = function (METHOD_NAME, argument) {
  var method = [][METHOD_NAME];
  return !!method && fails(function () {
    // eslint-disable-next-line no-useless-call,no-throw-literal
    method.call(null, argument || function () { throw 1; }, 1);
  });
};

var defineProperty$3 = Object.defineProperty;
var cache = {};

var thrower = function (it) { throw it; };

var arrayMethodUsesToLength = function (METHOD_NAME, options) {
  if (has(cache, METHOD_NAME)) return cache[METHOD_NAME];
  if (!options) options = {};
  var method = [][METHOD_NAME];
  var ACCESSORS = has(options, 'ACCESSORS') ? options.ACCESSORS : false;
  var argument0 = has(options, 0) ? options[0] : thrower;
  var argument1 = has(options, 1) ? options[1] : undefined;

  return cache[METHOD_NAME] = !!method && !fails(function () {
    if (ACCESSORS && !descriptors) return true;
    var O = { length: -1 };

    if (ACCESSORS) defineProperty$3(O, 1, { enumerable: true, get: thrower });
    else O[1] = 1;

    method.call(O, argument0, argument1);
  });
};

var $forEach = arrayIteration.forEach;



var STRICT_METHOD = arrayMethodIsStrict('forEach');
var USES_TO_LENGTH = arrayMethodUsesToLength('forEach');

// `Array.prototype.forEach` method implementation
// https://tc39.github.io/ecma262/#sec-array.prototype.foreach
var arrayForEach = (!STRICT_METHOD || !USES_TO_LENGTH) ? function forEach(callbackfn /* , thisArg */) {
  return $forEach(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
} : [].forEach;

// `Array.prototype.forEach` method
// https://tc39.github.io/ecma262/#sec-array.prototype.foreach
_export({ target: 'Array', proto: true, forced: [].forEach != arrayForEach }, {
  forEach: arrayForEach
});

var entryVirtual = function (CONSTRUCTOR) {
  return path[CONSTRUCTOR + 'Prototype'];
};

var forEach = entryVirtual('Array').forEach;

var forEach$1 = forEach;

var ArrayPrototype = Array.prototype;

var DOMIterables = {
  DOMTokenList: true,
  NodeList: true
};

var forEach_1 = function (it) {
  var own = it.forEach;
  return it === ArrayPrototype || (it instanceof Array && own === ArrayPrototype.forEach)
    // eslint-disable-next-line no-prototype-builtins
    || DOMIterables.hasOwnProperty(classof(it)) ? forEach$1 : own;
};

var forEach$2 = forEach_1;

var nativeGetOwnPropertyDescriptor$1 = objectGetOwnPropertyDescriptor.f;


var FAILS_ON_PRIMITIVES = fails(function () { nativeGetOwnPropertyDescriptor$1(1); });
var FORCED = !descriptors || FAILS_ON_PRIMITIVES;

// `Object.getOwnPropertyDescriptor` method
// https://tc39.github.io/ecma262/#sec-object.getownpropertydescriptor
_export({ target: 'Object', stat: true, forced: FORCED, sham: !descriptors }, {
  getOwnPropertyDescriptor: function getOwnPropertyDescriptor(it, key) {
    return nativeGetOwnPropertyDescriptor$1(toIndexedObject(it), key);
  }
});

var getOwnPropertyDescriptor_1 = createCommonjsModule(function (module) {
var Object = path.Object;

var getOwnPropertyDescriptor = module.exports = function getOwnPropertyDescriptor(it, key) {
  return Object.getOwnPropertyDescriptor(it, key);
};

if (Object.getOwnPropertyDescriptor.sham) getOwnPropertyDescriptor.sham = true;
});

var getOwnPropertyDescriptor$2 = getOwnPropertyDescriptor_1;

var getOwnPropertyDescriptor$3 = getOwnPropertyDescriptor$2;

var engineUserAgent = getBuiltIn('navigator', 'userAgent') || '';

var process = global_1.process;
var versions = process && process.versions;
var v8 = versions && versions.v8;
var match, version;

if (v8) {
  match = v8.split('.');
  version = match[0] + match[1];
} else if (engineUserAgent) {
  match = engineUserAgent.match(/Edge\/(\d+)/);
  if (!match || match[1] >= 74) {
    match = engineUserAgent.match(/Chrome\/(\d+)/);
    if (match) version = match[1];
  }
}

var engineV8Version = version && +version;

var SPECIES$1 = wellKnownSymbol('species');

var arrayMethodHasSpeciesSupport = function (METHOD_NAME) {
  // We can't use this feature detection in V8 since it causes
  // deoptimization and serious performance degradation
  // https://github.com/zloirock/core-js/issues/677
  return engineV8Version >= 51 || !fails(function () {
    var array = [];
    var constructor = array.constructor = {};
    constructor[SPECIES$1] = function () {
      return { foo: 1 };
    };
    return array[METHOD_NAME](Boolean).foo !== 1;
  });
};

var $filter = arrayIteration.filter;



var HAS_SPECIES_SUPPORT = arrayMethodHasSpeciesSupport('filter');
// Edge 14- issue
var USES_TO_LENGTH$1 = arrayMethodUsesToLength('filter');

// `Array.prototype.filter` method
// https://tc39.github.io/ecma262/#sec-array.prototype.filter
// with adding support of @@species
_export({ target: 'Array', proto: true, forced: !HAS_SPECIES_SUPPORT || !USES_TO_LENGTH$1 }, {
  filter: function filter(callbackfn /* , thisArg */) {
    return $filter(this, callbackfn, arguments.length > 1 ? arguments[1] : undefined);
  }
});

var filter = entryVirtual('Array').filter;

var ArrayPrototype$1 = Array.prototype;

var filter_1 = function (it) {
  var own = it.filter;
  return it === ArrayPrototype$1 || (it instanceof Array && own === ArrayPrototype$1.filter) ? filter : own;
};

var filter$1 = filter_1;

var filter$2 = filter$1;

var nativeGetOwnPropertyNames = objectGetOwnPropertyNames.f;

var toString$1 = {}.toString;

var windowNames = typeof window == 'object' && window && Object.getOwnPropertyNames
  ? Object.getOwnPropertyNames(window) : [];

var getWindowNames = function (it) {
  try {
    return nativeGetOwnPropertyNames(it);
  } catch (error) {
    return windowNames.slice();
  }
};

// fallback for IE11 buggy Object.getOwnPropertyNames with iframe and window
var f$5 = function getOwnPropertyNames(it) {
  return windowNames && toString$1.call(it) == '[object Window]'
    ? getWindowNames(it)
    : nativeGetOwnPropertyNames(toIndexedObject(it));
};

var objectGetOwnPropertyNamesExternal = {
	f: f$5
};

var f$6 = wellKnownSymbol;

var wellKnownSymbolWrapped = {
	f: f$6
};

var defineProperty$4 = objectDefineProperty.f;

var defineWellKnownSymbol = function (NAME) {
  var Symbol = path.Symbol || (path.Symbol = {});
  if (!has(Symbol, NAME)) defineProperty$4(Symbol, NAME, {
    value: wellKnownSymbolWrapped.f(NAME)
  });
};

var $forEach$1 = arrayIteration.forEach;

var HIDDEN = sharedKey('hidden');
var SYMBOL = 'Symbol';
var PROTOTYPE$1 = 'prototype';
var TO_PRIMITIVE = wellKnownSymbol('toPrimitive');
var setInternalState$1 = internalState.set;
var getInternalState$1 = internalState.getterFor(SYMBOL);
var ObjectPrototype$1 = Object[PROTOTYPE$1];
var $Symbol = global_1.Symbol;
var $stringify = getBuiltIn('JSON', 'stringify');
var nativeGetOwnPropertyDescriptor$2 = objectGetOwnPropertyDescriptor.f;
var nativeDefineProperty$1 = objectDefineProperty.f;
var nativeGetOwnPropertyNames$1 = objectGetOwnPropertyNamesExternal.f;
var nativePropertyIsEnumerable$1 = objectPropertyIsEnumerable.f;
var AllSymbols = shared('symbols');
var ObjectPrototypeSymbols = shared('op-symbols');
var StringToSymbolRegistry = shared('string-to-symbol-registry');
var SymbolToStringRegistry = shared('symbol-to-string-registry');
var WellKnownSymbolsStore$1 = shared('wks');
var QObject = global_1.QObject;
// Don't use setters in Qt Script, https://github.com/zloirock/core-js/issues/173
var USE_SETTER = !QObject || !QObject[PROTOTYPE$1] || !QObject[PROTOTYPE$1].findChild;

// fallback for old Android, https://code.google.com/p/v8/issues/detail?id=687
var setSymbolDescriptor = descriptors && fails(function () {
  return objectCreate(nativeDefineProperty$1({}, 'a', {
    get: function () { return nativeDefineProperty$1(this, 'a', { value: 7 }).a; }
  })).a != 7;
}) ? function (O, P, Attributes) {
  var ObjectPrototypeDescriptor = nativeGetOwnPropertyDescriptor$2(ObjectPrototype$1, P);
  if (ObjectPrototypeDescriptor) delete ObjectPrototype$1[P];
  nativeDefineProperty$1(O, P, Attributes);
  if (ObjectPrototypeDescriptor && O !== ObjectPrototype$1) {
    nativeDefineProperty$1(ObjectPrototype$1, P, ObjectPrototypeDescriptor);
  }
} : nativeDefineProperty$1;

var wrap = function (tag, description) {
  var symbol = AllSymbols[tag] = objectCreate($Symbol[PROTOTYPE$1]);
  setInternalState$1(symbol, {
    type: SYMBOL,
    tag: tag,
    description: description
  });
  if (!descriptors) symbol.description = description;
  return symbol;
};

var isSymbol = useSymbolAsUid ? function (it) {
  return typeof it == 'symbol';
} : function (it) {
  return Object(it) instanceof $Symbol;
};

var $defineProperty = function defineProperty(O, P, Attributes) {
  if (O === ObjectPrototype$1) $defineProperty(ObjectPrototypeSymbols, P, Attributes);
  anObject(O);
  var key = toPrimitive(P, true);
  anObject(Attributes);
  if (has(AllSymbols, key)) {
    if (!Attributes.enumerable) {
      if (!has(O, HIDDEN)) nativeDefineProperty$1(O, HIDDEN, createPropertyDescriptor(1, {}));
      O[HIDDEN][key] = true;
    } else {
      if (has(O, HIDDEN) && O[HIDDEN][key]) O[HIDDEN][key] = false;
      Attributes = objectCreate(Attributes, { enumerable: createPropertyDescriptor(0, false) });
    } return setSymbolDescriptor(O, key, Attributes);
  } return nativeDefineProperty$1(O, key, Attributes);
};

var $defineProperties = function defineProperties(O, Properties) {
  anObject(O);
  var properties = toIndexedObject(Properties);
  var keys = objectKeys(properties).concat($getOwnPropertySymbols(properties));
  $forEach$1(keys, function (key) {
    if (!descriptors || $propertyIsEnumerable.call(properties, key)) $defineProperty(O, key, properties[key]);
  });
  return O;
};

var $create = function create(O, Properties) {
  return Properties === undefined ? objectCreate(O) : $defineProperties(objectCreate(O), Properties);
};

var $propertyIsEnumerable = function propertyIsEnumerable(V) {
  var P = toPrimitive(V, true);
  var enumerable = nativePropertyIsEnumerable$1.call(this, P);
  if (this === ObjectPrototype$1 && has(AllSymbols, P) && !has(ObjectPrototypeSymbols, P)) return false;
  return enumerable || !has(this, P) || !has(AllSymbols, P) || has(this, HIDDEN) && this[HIDDEN][P] ? enumerable : true;
};

var $getOwnPropertyDescriptor = function getOwnPropertyDescriptor(O, P) {
  var it = toIndexedObject(O);
  var key = toPrimitive(P, true);
  if (it === ObjectPrototype$1 && has(AllSymbols, key) && !has(ObjectPrototypeSymbols, key)) return;
  var descriptor = nativeGetOwnPropertyDescriptor$2(it, key);
  if (descriptor && has(AllSymbols, key) && !(has(it, HIDDEN) && it[HIDDEN][key])) {
    descriptor.enumerable = true;
  }
  return descriptor;
};

var $getOwnPropertyNames = function getOwnPropertyNames(O) {
  var names = nativeGetOwnPropertyNames$1(toIndexedObject(O));
  var result = [];
  $forEach$1(names, function (key) {
    if (!has(AllSymbols, key) && !has(hiddenKeys, key)) result.push(key);
  });
  return result;
};

var $getOwnPropertySymbols = function getOwnPropertySymbols(O) {
  var IS_OBJECT_PROTOTYPE = O === ObjectPrototype$1;
  var names = nativeGetOwnPropertyNames$1(IS_OBJECT_PROTOTYPE ? ObjectPrototypeSymbols : toIndexedObject(O));
  var result = [];
  $forEach$1(names, function (key) {
    if (has(AllSymbols, key) && (!IS_OBJECT_PROTOTYPE || has(ObjectPrototype$1, key))) {
      result.push(AllSymbols[key]);
    }
  });
  return result;
};

// `Symbol` constructor
// https://tc39.github.io/ecma262/#sec-symbol-constructor
if (!nativeSymbol) {
  $Symbol = function Symbol() {
    if (this instanceof $Symbol) throw TypeError('Symbol is not a constructor');
    var description = !arguments.length || arguments[0] === undefined ? undefined : String(arguments[0]);
    var tag = uid(description);
    var setter = function (value) {
      if (this === ObjectPrototype$1) setter.call(ObjectPrototypeSymbols, value);
      if (has(this, HIDDEN) && has(this[HIDDEN], tag)) this[HIDDEN][tag] = false;
      setSymbolDescriptor(this, tag, createPropertyDescriptor(1, value));
    };
    if (descriptors && USE_SETTER) setSymbolDescriptor(ObjectPrototype$1, tag, { configurable: true, set: setter });
    return wrap(tag, description);
  };

  redefine($Symbol[PROTOTYPE$1], 'toString', function toString() {
    return getInternalState$1(this).tag;
  });

  redefine($Symbol, 'withoutSetter', function (description) {
    return wrap(uid(description), description);
  });

  objectPropertyIsEnumerable.f = $propertyIsEnumerable;
  objectDefineProperty.f = $defineProperty;
  objectGetOwnPropertyDescriptor.f = $getOwnPropertyDescriptor;
  objectGetOwnPropertyNames.f = objectGetOwnPropertyNamesExternal.f = $getOwnPropertyNames;
  objectGetOwnPropertySymbols.f = $getOwnPropertySymbols;

  wellKnownSymbolWrapped.f = function (name) {
    return wrap(wellKnownSymbol(name), name);
  };

  if (descriptors) {
    // https://github.com/tc39/proposal-Symbol-description
    nativeDefineProperty$1($Symbol[PROTOTYPE$1], 'description', {
      configurable: true,
      get: function description() {
        return getInternalState$1(this).description;
      }
    });
  }
}

_export({ global: true, wrap: true, forced: !nativeSymbol, sham: !nativeSymbol }, {
  Symbol: $Symbol
});

$forEach$1(objectKeys(WellKnownSymbolsStore$1), function (name) {
  defineWellKnownSymbol(name);
});

_export({ target: SYMBOL, stat: true, forced: !nativeSymbol }, {
  // `Symbol.for` method
  // https://tc39.github.io/ecma262/#sec-symbol.for
  'for': function (key) {
    var string = String(key);
    if (has(StringToSymbolRegistry, string)) return StringToSymbolRegistry[string];
    var symbol = $Symbol(string);
    StringToSymbolRegistry[string] = symbol;
    SymbolToStringRegistry[symbol] = string;
    return symbol;
  },
  // `Symbol.keyFor` method
  // https://tc39.github.io/ecma262/#sec-symbol.keyfor
  keyFor: function keyFor(sym) {
    if (!isSymbol(sym)) throw TypeError(sym + ' is not a symbol');
    if (has(SymbolToStringRegistry, sym)) return SymbolToStringRegistry[sym];
  },
  useSetter: function () { USE_SETTER = true; },
  useSimple: function () { USE_SETTER = false; }
});

_export({ target: 'Object', stat: true, forced: !nativeSymbol, sham: !descriptors }, {
  // `Object.create` method
  // https://tc39.github.io/ecma262/#sec-object.create
  create: $create,
  // `Object.defineProperty` method
  // https://tc39.github.io/ecma262/#sec-object.defineproperty
  defineProperty: $defineProperty,
  // `Object.defineProperties` method
  // https://tc39.github.io/ecma262/#sec-object.defineproperties
  defineProperties: $defineProperties,
  // `Object.getOwnPropertyDescriptor` method
  // https://tc39.github.io/ecma262/#sec-object.getownpropertydescriptors
  getOwnPropertyDescriptor: $getOwnPropertyDescriptor
});

_export({ target: 'Object', stat: true, forced: !nativeSymbol }, {
  // `Object.getOwnPropertyNames` method
  // https://tc39.github.io/ecma262/#sec-object.getownpropertynames
  getOwnPropertyNames: $getOwnPropertyNames,
  // `Object.getOwnPropertySymbols` method
  // https://tc39.github.io/ecma262/#sec-object.getownpropertysymbols
  getOwnPropertySymbols: $getOwnPropertySymbols
});

// Chrome 38 and 39 `Object.getOwnPropertySymbols` fails on primitives
// https://bugs.chromium.org/p/v8/issues/detail?id=3443
_export({ target: 'Object', stat: true, forced: fails(function () { objectGetOwnPropertySymbols.f(1); }) }, {
  getOwnPropertySymbols: function getOwnPropertySymbols(it) {
    return objectGetOwnPropertySymbols.f(toObject(it));
  }
});

// `JSON.stringify` method behavior with symbols
// https://tc39.github.io/ecma262/#sec-json.stringify
if ($stringify) {
  var FORCED_JSON_STRINGIFY = !nativeSymbol || fails(function () {
    var symbol = $Symbol();
    // MS Edge converts symbol values to JSON as {}
    return $stringify([symbol]) != '[null]'
      // WebKit converts symbol values to JSON as null
      || $stringify({ a: symbol }) != '{}'
      // V8 throws on boxed symbols
      || $stringify(Object(symbol)) != '{}';
  });

  _export({ target: 'JSON', stat: true, forced: FORCED_JSON_STRINGIFY }, {
    // eslint-disable-next-line no-unused-vars
    stringify: function stringify(it, replacer, space) {
      var args = [it];
      var index = 1;
      var $replacer;
      while (arguments.length > index) args.push(arguments[index++]);
      $replacer = replacer;
      if (!isObject(replacer) && it === undefined || isSymbol(it)) return; // IE8 returns string on undefined
      if (!isArray(replacer)) replacer = function (key, value) {
        if (typeof $replacer == 'function') value = $replacer.call(this, key, value);
        if (!isSymbol(value)) return value;
      };
      args[1] = replacer;
      return $stringify.apply(null, args);
    }
  });
}

// `Symbol.prototype[@@toPrimitive]` method
// https://tc39.github.io/ecma262/#sec-symbol.prototype-@@toprimitive
if (!$Symbol[PROTOTYPE$1][TO_PRIMITIVE]) {
  createNonEnumerableProperty($Symbol[PROTOTYPE$1], TO_PRIMITIVE, $Symbol[PROTOTYPE$1].valueOf);
}
// `Symbol.prototype[@@toStringTag]` property
// https://tc39.github.io/ecma262/#sec-symbol.prototype-@@tostringtag
setToStringTag($Symbol, SYMBOL);

hiddenKeys[HIDDEN] = true;

var getOwnPropertySymbols = path.Object.getOwnPropertySymbols;

var getOwnPropertySymbols$1 = getOwnPropertySymbols;

var getOwnPropertySymbols$2 = getOwnPropertySymbols$1;

var FAILS_ON_PRIMITIVES$1 = fails(function () { objectKeys(1); });

// `Object.keys` method
// https://tc39.github.io/ecma262/#sec-object.keys
_export({ target: 'Object', stat: true, forced: FAILS_ON_PRIMITIVES$1 }, {
  keys: function keys(it) {
    return objectKeys(toObject(it));
  }
});

var keys$1 = path.Object.keys;

var keys$2 = keys$1;

var keys$3 = keys$2;

var nativeAssign = Object.assign;
var defineProperty$5 = Object.defineProperty;

// `Object.assign` method
// https://tc39.github.io/ecma262/#sec-object.assign
var objectAssign = !nativeAssign || fails(function () {
  // should have correct order of operations (Edge bug)
  if (descriptors && nativeAssign({ b: 1 }, nativeAssign(defineProperty$5({}, 'a', {
    enumerable: true,
    get: function () {
      defineProperty$5(this, 'b', {
        value: 3,
        enumerable: false
      });
    }
  }), { b: 2 })).b !== 1) return true;
  // should work with symbols and should have deterministic property order (V8 bug)
  var A = {};
  var B = {};
  // eslint-disable-next-line no-undef
  var symbol = Symbol();
  var alphabet = 'abcdefghijklmnopqrst';
  A[symbol] = 7;
  alphabet.split('').forEach(function (chr) { B[chr] = chr; });
  return nativeAssign({}, A)[symbol] != 7 || objectKeys(nativeAssign({}, B)).join('') != alphabet;
}) ? function assign(target, source) { // eslint-disable-line no-unused-vars
  var T = toObject(target);
  var argumentsLength = arguments.length;
  var index = 1;
  var getOwnPropertySymbols = objectGetOwnPropertySymbols.f;
  var propertyIsEnumerable = objectPropertyIsEnumerable.f;
  while (argumentsLength > index) {
    var S = indexedObject(arguments[index++]);
    var keys = getOwnPropertySymbols ? objectKeys(S).concat(getOwnPropertySymbols(S)) : objectKeys(S);
    var length = keys.length;
    var j = 0;
    var key;
    while (length > j) {
      key = keys[j++];
      if (!descriptors || propertyIsEnumerable.call(S, key)) T[key] = S[key];
    }
  } return T;
} : nativeAssign;

// `Object.assign` method
// https://tc39.github.io/ecma262/#sec-object.assign
_export({ target: 'Object', stat: true, forced: Object.assign !== objectAssign }, {
  assign: objectAssign
});

var assign = path.Object.assign;

var assign$1 = assign;

var assign$2 = assign$1;

var _extends_1 = createCommonjsModule(function (module) {
function _extends() {
  module.exports = _extends = assign$2 || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

module.exports = _extends;
});

var defineProperty$6 = defineProperty_1;

var defineProperty$7 = defineProperty$6;

function _defineProperty(obj, key, value) {
  if (key in obj) {
    defineProperty$7(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
  } else {
    obj[key] = value;
  }

  return obj;
}

var defineProperty$8 = _defineProperty;

var $indexOf = arrayIncludes.indexOf;



var nativeIndexOf = [].indexOf;

var NEGATIVE_ZERO = !!nativeIndexOf && 1 / [1].indexOf(1, -0) < 0;
var STRICT_METHOD$1 = arrayMethodIsStrict('indexOf');
var USES_TO_LENGTH$2 = arrayMethodUsesToLength('indexOf', { ACCESSORS: true, 1: 0 });

// `Array.prototype.indexOf` method
// https://tc39.github.io/ecma262/#sec-array.prototype.indexof
_export({ target: 'Array', proto: true, forced: NEGATIVE_ZERO || !STRICT_METHOD$1 || !USES_TO_LENGTH$2 }, {
  indexOf: function indexOf(searchElement /* , fromIndex = 0 */) {
    return NEGATIVE_ZERO
      // convert -0 to +0
      ? nativeIndexOf.apply(this, arguments) || 0
      : $indexOf(this, searchElement, arguments.length > 1 ? arguments[1] : undefined);
  }
});

var indexOf$1 = entryVirtual('Array').indexOf;

var ArrayPrototype$2 = Array.prototype;

var indexOf_1 = function (it) {
  var own = it.indexOf;
  return it === ArrayPrototype$2 || (it instanceof Array && own === ArrayPrototype$2.indexOf) ? indexOf$1 : own;
};

var indexOf$2 = indexOf_1;

var indexOf$3 = indexOf$2;

var getOwnPropertySymbols$3 = getOwnPropertySymbols;

var getOwnPropertySymbols$4 = getOwnPropertySymbols$3;

var keys$4 = keys$1;

var keys$5 = keys$4;

function _objectWithoutPropertiesLoose(source, excluded) {
  if (source == null) return {};
  var target = {};

  var sourceKeys = keys$5(source);

  var key, i;

  for (i = 0; i < sourceKeys.length; i++) {
    key = sourceKeys[i];
    if (indexOf$3(excluded).call(excluded, key) >= 0) continue;
    target[key] = source[key];
  }

  return target;
}

var objectWithoutPropertiesLoose = _objectWithoutPropertiesLoose;

function _objectWithoutProperties(source, excluded) {
  if (source == null) return {};
  var target = objectWithoutPropertiesLoose(source, excluded);
  var key, i;

  if (getOwnPropertySymbols$4) {
    var sourceSymbolKeys = getOwnPropertySymbols$4(source);

    for (i = 0; i < sourceSymbolKeys.length; i++) {
      key = sourceSymbolKeys[i];
      if (indexOf$3(excluded).call(excluded, key) >= 0) continue;
      if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue;
      target[key] = source[key];
    }
  }

  return target;
}

var objectWithoutProperties = _objectWithoutProperties;

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var _ref = /*#__PURE__*/React.createElement("defs", null);

var _ref2 = /*#__PURE__*/React.createElement("path", {
  d: "M4044 4869V2426c0-131-108-239-240-239H1362c-131 0-239 108-239 239v2443c0 131 108 239 239 239h2442c132 0 240-108 240-239zM1717 2681h1732c56 0 101 45 101 100v1733c0 55-45 100-101 100H1717c-55 0-100-45-100-100V2781c0-55 45-100 100-100zm2327 5733V5971c0-131-108-239-240-239H1362c-131 0-239 108-239 239v2443c0 131 108 239 239 239h2442c132 0 240-108 240-239zM1717 6226h1732c56 0 101 45 101 100v1733c0 55-45 100-101 100H1717c-55 0-100-45-100-100V6326c0-55 45-100 100-100zm5866 2188V5971c0-131-108-239-239-239H4901c-131 0-239 108-239 239v2443c0 131 108 239 239 239h2443c131 0 239-108 239-239zM5256 6226h1733c55 0 100 45 100 100v1733c0 55-45 100-100 100H5256c-55 0-100-45-100-100V6326c0-55 45-100 100-100zm3133-2762L7311 1272c-58-118-202-167-320-109L4799 2241c-118 59-167 203-109 321l1078 2191c58 118 203 168 321 109l2191-1078c118-58 167-202 109-320zm-3053-936l1554-765c50-24 110-4 135 46l764 1554c25 50 4 111-45 135l-1555 765c-49 24-110 4-135-46l-764-1554c-25-50-4-111 46-135z",
  id: "\u0421\u043B\u043E\u0439_x0020_1"
});

var SvgComponent = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref, _ref2);
};

var blocks_light = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cpath%20d%3D%22M4044%204869l0%20-2443c0%2C-131%20-108%2C-239%20-240%2C-239l-2442%200c-131%2C0%20-239%2C108%20-239%2C239l0%202443c0%2C131%20108%2C239%20239%2C239l2442%200c132%2C0%20240%2C-108%20240%2C-239zm-2327%20-2188l1732%200c56%2C0%20101%2C45%20101%2C100l0%201733c0%2C55%20-45%2C100%20-101%2C100l-1732%200c-55%2C0%20-100%2C-45%20-100%2C-100l0%20-1733c0%2C-55%2045%2C-100%20100%2C-100zm2327%205733l0%20-2443c0%2C-131%20-108%2C-239%20-240%2C-239l-2442%200c-131%2C0%20-239%2C108%20-239%2C239l0%202443c0%2C131%20108%2C239%20239%2C239l2442%200c132%2C0%20240%2C-108%20240%2C-239zm-2327%20-2188l1732%200c56%2C0%20101%2C45%20101%2C100l0%201733c0%2C55%20-45%2C100%20-101%2C100l-1732%200c-55%2C0%20-100%2C-45%20-100%2C-100l0%20-1733c0%2C-55%2045%2C-100%20100%2C-100zm5866%202188l0%20-2443c0%2C-131%20-108%2C-239%20-239%2C-239l-2443%200c-131%2C0%20-239%2C108%20-239%2C239l0%202443c0%2C131%20108%2C239%20239%2C239l2443%200c131%2C0%20239%2C-108%20239%2C-239zm-2327%20-2188l1733%200c55%2C0%20100%2C45%20100%2C100l0%201733c0%2C55%20-45%2C100%20-100%2C100l-1733%200c-55%2C0%20-100%2C-45%20-100%2C-100l0%20-1733c0%2C-55%2045%2C-100%20100%2C-100zm3133%20-2762l-1078%20-2192c-58%2C-118%20-202%2C-167%20-320%2C-109l-2192%201078c-118%2C59%20-167%2C203%20-109%2C321l1078%202191c58%2C118%20203%2C168%20321%2C109l2191%20-1078c118%2C-58%20167%2C-202%20109%2C-320zm-3053%20-936l1554%20-765c50%2C-24%20110%2C-4%20135%2C46l764%201554c25%2C50%204%2C111%20-45%2C135l-1555%20765c-49%2C24%20-110%2C4%20-135%2C-46l-764%20-1554c-25%2C-50%20-4%2C-111%2046%2C-135z%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$1() { _extends$1 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$1.apply(this, arguments); }

var _ref$1 = /*#__PURE__*/React.createElement("defs", {
  id: "defs4"
});

var _ref2$1 = /*#__PURE__*/React.createElement("g", {
  id: "\u0421\u043B\u043E\u0439_x0020_1",
  transform: "translate(-1157.878 -1346.24) scale(1.3075)"
}, /*#__PURE__*/React.createElement("g", {
  id: "_2538414054368"
}, /*#__PURE__*/React.createElement("path", {
  className: "fil0",
  d: "M4034 4938V2420c0-111-90-202-201-202H1315c-111 0-202 91-202 202v2518c0 111 91 201 202 201h2518c111 0 201-90 201-201zM1836 2855h1476c47 0 86 38 86 86v1476c0 47-39 86-86 86H1836c-48 0-86-39-86-86V2941c0-48 38-86 86-86z",
  id: "path7"
}), /*#__PURE__*/React.createElement("path", {
  className: "fil0",
  d: "M4034 8483V5965c0-111-90-202-201-202H1315c-111 0-202 91-202 202v2518c0 111 91 201 202 201h2518c111 0 201-90 201-201zM1836 6400h1476c47 0 86 38 86 85v1477c0 47-39 86-86 86H1836c-48 0-86-39-86-86V6485c0-47 38-85 86-85z",
  id: "path9"
}), /*#__PURE__*/React.createElement("path", {
  className: "fil0",
  d: "M7574 8483V5965c0-111-91-202-202-202H4854c-111 0-201 91-201 202v2518c0 111 90 201 201 201h2518c111 0 202-90 202-201zM5375 6400h1476c47 0 86 38 86 85v1477c0 47-39 86-86 86H5375c-47 0-86-39-86-86V6485c0-47 39-85 86-85z",
  id: "path11"
}), /*#__PURE__*/React.createElement("path", {
  className: "fil0",
  d: "M8394 3488L7302 1219c-48-100-169-142-269-94L4764 2217c-100 48-142 169-94 269l1092 2269c48 100 169 142 269 94l2269-1092c99-48 142-169 94-269zm-2885-924l1331-640c42-20 94-2 114 40l640 1331c21 42 3 94-40 114l-1330 640c-43 21-94 3-115-40l-640-1330c-20-42-2-94 40-115z",
  id: "path13"
})));

var SvgComponent$1 = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$1({
    xmlSpace: "preserve",
    width: "100mm",
    height: "100mm",
    version: 1.1,
    viewBox: "0 0 10000 10000",
    id: "svg17",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$1, _ref2$1);
};

var blocks_regular = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3Asodipodi%3D%22http%3A%2F%2Fsodipodi.sourceforge.net%2FDTD%2Fsodipodi-0.dtd%22%20%20%20xmlns%3Ainkscape%3D%22http%3A%2F%2Fwww.inkscape.org%2Fnamespaces%2Finkscape%22%20%20%20xml%3Aspace%3D%22preserve%22%20%20%20width%3D%22100mm%22%20%20%20height%3D%22100mm%22%20%20%20version%3D%221.1%22%20%20%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22%20%20%20viewBox%3D%220%200%2010000%2010000%22%20%20%20id%3D%22svg17%22%20%20%20sodipodi%3Adocname%3D%22blocks_regular.svg%22%20%20%20inkscape%3Aversion%3D%220.92.3%20%282405546%2C%202018-03-11%29%22%3E%3Cmetadata%20%20%20id%3D%22metadata21%22%3E%3Crdf%3ARDF%3E%3Ccc%3AWork%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%3Cdc%3Atype%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%3C%2Fcc%3AWork%3E%3C%2Frdf%3ARDF%3E%3C%2Fmetadata%3E%3Csodipodi%3Anamedview%20%20%20pagecolor%3D%22%23ffffff%22%20%20%20bordercolor%3D%22%23666666%22%20%20%20borderopacity%3D%221%22%20%20%20objecttolerance%3D%2210%22%20%20%20gridtolerance%3D%2210%22%20%20%20guidetolerance%3D%2210%22%20%20%20inkscape%3Apageopacity%3D%220%22%20%20%20inkscape%3Apageshadow%3D%222%22%20%20%20inkscape%3Awindow-width%3D%22640%22%20%20%20inkscape%3Awindow-height%3D%22480%22%20%20%20id%3D%22namedview19%22%20%20%20showgrid%3D%22false%22%20%20%20inkscape%3Azoom%3D%220.62441667%22%20%20%20inkscape%3Acx%3D%22188.97638%22%20%20%20inkscape%3Acy%3D%22188.97638%22%20%20%20inkscape%3Awindow-x%3D%220%22%20%20%20inkscape%3Awindow-y%3D%220%22%20%20%20inkscape%3Awindow-maximized%3D%220%22%20%20%20inkscape%3Acurrent-layer%3D%22svg17%22%20%2F%3E%20%3Cdefs%20%20%20id%3D%22defs4%22%3E%20%20%3Cstyle%20%20%20type%3D%22text%2Fcss%22%20%20%20id%3D%22style2%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20%20%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%20%20%20transform%3D%22matrix%281.3075014%2C0%2C0%2C1.3075014%2C-1157.8777%2C-1346.2392%29%22%3E%20%20%3Cmetadata%20%20%20id%3D%22CorelCorpID_0Corel-Layer%22%20%2F%3E%20%20%3Cg%20%20%20id%3D%22_2538414054368%22%3E%20%20%20%3Cpath%20%20%20class%3D%22fil0%22%20%20%20d%3D%22M%204034%2C4938%20V%202420%20c%200%2C-111%20-90%2C-202%20-201%2C-202%20H%201315%20c%20-111%2C0%20-202%2C91%20-202%2C202%20v%202518%20c%200%2C111%2091%2C201%20202%2C201%20h%202518%20c%20111%2C0%20201%2C-90%20201%2C-201%20z%20M%201836%2C2855%20h%201476%20c%2047%2C0%2086%2C38%2086%2C86%20v%201476%20c%200%2C47%20-39%2C86%20-86%2C86%20H%201836%20c%20-48%2C0%20-86%2C-39%20-86%2C-86%20V%202941%20c%200%2C-48%2038%2C-86%2086%2C-86%20z%22%20%20%20id%3D%22path7%22%20%20%20inkscape%3Aconnector-curvature%3D%220%22%20%20%20%2F%3E%20%20%20%3Cpath%20%20%20class%3D%22fil0%22%20%20%20d%3D%22M%204034%2C8483%20V%205965%20c%200%2C-111%20-90%2C-202%20-201%2C-202%20H%201315%20c%20-111%2C0%20-202%2C91%20-202%2C202%20v%202518%20c%200%2C111%2091%2C201%20202%2C201%20h%202518%20c%20111%2C0%20201%2C-90%20201%2C-201%20z%20M%201836%2C6400%20h%201476%20c%2047%2C0%2086%2C38%2086%2C85%20v%201477%20c%200%2C47%20-39%2C86%20-86%2C86%20H%201836%20c%20-48%2C0%20-86%2C-39%20-86%2C-86%20V%206485%20c%200%2C-47%2038%2C-85%2086%2C-85%20z%22%20%20%20id%3D%22path9%22%20%20%20inkscape%3Aconnector-curvature%3D%220%22%20%20%2F%3E%20%20%20%3Cpath%20%20%20class%3D%22fil0%22%20%20%20d%3D%22M%207574%2C8483%20V%205965%20c%200%2C-111%20-91%2C-202%20-202%2C-202%20H%204854%20c%20-111%2C0%20-201%2C91%20-201%2C202%20v%202518%20c%200%2C111%2090%2C201%20201%2C201%20h%202518%20c%20111%2C0%20202%2C-90%20202%2C-201%20z%20M%205375%2C6400%20h%201476%20c%2047%2C0%2086%2C38%2086%2C85%20v%201477%20c%200%2C47%20-39%2C86%20-86%2C86%20H%205375%20c%20-47%2C0%20-86%2C-39%20-86%2C-86%20V%206485%20c%200%2C-47%2039%2C-85%2086%2C-85%20z%22%20%20%20id%3D%22path11%22%20%20%20inkscape%3Aconnector-curvature%3D%220%22%20%20%2F%3E%20%20%20%3Cpath%20%20%20class%3D%22fil0%22%20%20%20d%3D%22M%208394%2C3488%207302%2C1219%20c%20-48%2C-100%20-169%2C-142%20-269%2C-94%20L%204764%2C2217%20c%20-100%2C48%20-142%2C169%20-94%2C269%20l%201092%2C2269%20c%2048%2C100%20169%2C142%20269%2C94%20L%208300%2C3757%20c%2099%2C-48%20142%2C-169%2094%2C-269%20z%20M%205509%2C2564%206840%2C1924%20c%2042%2C-20%2094%2C-2%20114%2C40%20l%20640%2C1331%20c%2021%2C42%203%2C94%20-40%2C114%20l%20-1330%2C640%20c%20-43%2C21%20-94%2C3%20-115%2C-40%20L%205469%2C2679%20c%20-20%2C-42%20-2%2C-94%2040%2C-115%20z%22%20%20%20id%3D%22path13%22%20%20%20inkscape%3Aconnector-curvature%3D%220%22%20%20%2F%3E%20%20%3C%2Fg%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$2() { _extends$2 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$2.apply(this, arguments); }

var _ref$2 = /*#__PURE__*/React.createElement("defs", null);

var _ref2$2 = /*#__PURE__*/React.createElement("g", {
  id: "\u0421\u043B\u043E\u0439_x0020_1"
}, /*#__PURE__*/React.createElement("g", {
  id: "_2538416377120"
}, /*#__PURE__*/React.createElement("path", {
  className: "fil0",
  d: "M4044 4869V2426c0-131-108-239-240-239H1362c-131 0-239 108-239 239v2443c0 131 108 239 239 239h2442c132 0 240-108 240-239zM4044 8414V5971c0-131-108-239-240-239H1362c-131 0-239 108-239 239v2443c0 131 108 239 239 239h2442c132 0 240-108 240-239zM7583 8414V5971c0-131-108-239-239-239H4901c-131 0-239 108-239 239v2443c0 131 108 239 239 239h2443c131 0 239-108 239-239zM8389 3464L7311 1272c-58-118-202-167-320-109L4799 2241c-118 59-167 203-109 321l1078 2191c58 118 203 168 321 109l2191-1078c118-58 167-202 109-320z"
})));

var SvgComponent$2 = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$2({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$2, _ref2$2);
};

var blocks_solid = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cg%20id%3D%22_2538416377120%22%3E%20%20%20%3Cpath%20class%3D%22fil0%22%20d%3D%22M4044%204869l0%20-2443c0%2C-131%20-108%2C-239%20-240%2C-239l-2442%200c-131%2C0%20-239%2C108%20-239%2C239l0%202443c0%2C131%20108%2C239%20239%2C239l2442%200c132%2C0%20240%2C-108%20240%2C-239z%22%2F%3E%20%20%20%3Cpath%20class%3D%22fil0%22%20d%3D%22M4044%208414l0%20-2443c0%2C-131%20-108%2C-239%20-240%2C-239l-2442%200c-131%2C0%20-239%2C108%20-239%2C239l0%202443c0%2C131%20108%2C239%20239%2C239l2442%200c132%2C0%20240%2C-108%20240%2C-239z%22%2F%3E%20%20%20%3Cpath%20class%3D%22fil0%22%20d%3D%22M7583%208414l0%20-2443c0%2C-131%20-108%2C-239%20-239%2C-239l-2443%200c-131%2C0%20-239%2C108%20-239%2C239l0%202443c0%2C131%20108%2C239%20239%2C239l2443%200c131%2C0%20239%2C-108%20239%2C-239z%22%2F%3E%20%20%20%3Cpath%20class%3D%22fil0%22%20d%3D%22M8389%203464l-1078%20-2192c-58%2C-118%20-202%2C-167%20-320%2C-109l-2192%201078c-118%2C59%20-167%2C203%20-109%2C321l1078%202191c58%2C118%20203%2C168%20321%2C109l2191%20-1078c118%2C-58%20167%2C-202%20109%2C-320z%22%2F%3E%20%20%3C%2Fg%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$3() { _extends$3 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$3.apply(this, arguments); }

var _ref$3 = /*#__PURE__*/React.createElement("path", {
  d: "M48 32C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48H48zm0 32h106c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H38c-3.3 0-6-2.7-6-6V80c0-8.8 7.2-16 16-16zm426 96H38c-3.3 0-6-2.7-6-6v-36c0-3.3 2.7-6 6-6h138l30.2-45.3c1.1-1.7 3-2.7 5-2.7H464c8.8 0 16 7.2 16 16v74c0 3.3-2.7 6-6 6zM256 424c-66.2 0-120-53.8-120-120s53.8-120 120-120 120 53.8 120 120-53.8 120-120 120zm0-208c-48.5 0-88 39.5-88 88s39.5 88 88 88 88-39.5 88-88-39.5-88-88-88zm-48 104c-8.8 0-16-7.2-16-16 0-35.3 28.7-64 64-64 8.8 0 16 7.2 16 16s-7.2 16-16 16c-17.6 0-32 14.4-32 32 0 8.8-7.2 16-16 16z"
});

var SvgComponent$3 = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$3({
    "aria-hidden": "true",
    "data-prefix": "fas",
    "data-icon": "camera-retro",
    className: "svg-inline--fa fa-camera-retro fa-w-16",
    viewBox: "0 0 512 512"
  }, props), _ref$3);
};

var camera_light = "data:image/svg+xml,%3Csvg%20aria-hidden%3D%22true%22%20focusable%3D%22false%22%20data-prefix%3D%22fas%22%20data-icon%3D%22camera-retro%22%20class%3D%22svg-inline--fa%20fa-camera-retro%20fa-w-16%22%20role%3D%22img%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20512%20512%22%3E%3Cpath%20d%3D%22M48%2032C21.5%2032%200%2053.5%200%2080v352c0%2026.5%2021.5%2048%2048%2048h416c26.5%200%2048-21.5%2048-48V80c0-26.5-21.5-48-48-48H48zm0%2032h106c3.3%200%206%202.7%206%206v20c0%203.3-2.7%206-6%206H38c-3.3%200-6-2.7-6-6V80c0-8.8%207.2-16%2016-16zm426%2096H38c-3.3%200-6-2.7-6-6v-36c0-3.3%202.7-6%206-6h138l30.2-45.3c1.1-1.7%203-2.7%205-2.7H464c8.8%200%2016%207.2%2016%2016v74c0%203.3-2.7%206-6%206zM256%20424c-66.2%200-120-53.8-120-120s53.8-120%20120-120%20120%2053.8%20120%20120-53.8%20120-120%20120zm0-208c-48.5%200-88%2039.5-88%2088s39.5%2088%2088%2088%2088-39.5%2088-88-39.5-88-88-88zm-48%20104c-8.8%200-16-7.2-16-16%200-35.3%2028.7-64%2064-64%208.8%200%2016%207.2%2016%2016s-7.2%2016-16%2016c-17.6%200-32%2014.4-32%2032%200%208.8-7.2%2016-16%2016z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E";

function _extends$4() { _extends$4 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$4.apply(this, arguments); }

var _ref$4 = /*#__PURE__*/React.createElement("path", {
  d: "M48 32C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48H48zm0 32h106c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H38c-3.3 0-6-2.7-6-6V80c0-8.8 7.2-16 16-16zm426 96H38c-3.3 0-6-2.7-6-6v-36c0-3.3 2.7-6 6-6h138l30.2-45.3c1.1-1.7 3-2.7 5-2.7H464c8.8 0 16 7.2 16 16v74c0 3.3-2.7 6-6 6zM256 424c-66.2 0-120-53.8-120-120s53.8-120 120-120 120 53.8 120 120-53.8 120-120 120zm0-208c-48.5 0-88 39.5-88 88s39.5 88 88 88 88-39.5 88-88-39.5-88-88-88zm-48 104c-8.8 0-16-7.2-16-16 0-35.3 28.7-64 64-64 8.8 0 16 7.2 16 16s-7.2 16-16 16c-17.6 0-32 14.4-32 32 0 8.8-7.2 16-16 16z"
});

var SvgComponent$4 = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$4({
    "aria-hidden": "true",
    "data-prefix": "fas",
    "data-icon": "camera-retro",
    className: "svg-inline--fa fa-camera-retro fa-w-16",
    viewBox: "0 0 512 512"
  }, props), _ref$4);
};

var camera_regular = "data:image/svg+xml,%3Csvg%20aria-hidden%3D%22true%22%20focusable%3D%22false%22%20data-prefix%3D%22fas%22%20data-icon%3D%22camera-retro%22%20class%3D%22svg-inline--fa%20fa-camera-retro%20fa-w-16%22%20role%3D%22img%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20512%20512%22%3E%3Cpath%20d%3D%22M48%2032C21.5%2032%200%2053.5%200%2080v352c0%2026.5%2021.5%2048%2048%2048h416c26.5%200%2048-21.5%2048-48V80c0-26.5-21.5-48-48-48H48zm0%2032h106c3.3%200%206%202.7%206%206v20c0%203.3-2.7%206-6%206H38c-3.3%200-6-2.7-6-6V80c0-8.8%207.2-16%2016-16zm426%2096H38c-3.3%200-6-2.7-6-6v-36c0-3.3%202.7-6%206-6h138l30.2-45.3c1.1-1.7%203-2.7%205-2.7H464c8.8%200%2016%207.2%2016%2016v74c0%203.3-2.7%206-6%206zM256%20424c-66.2%200-120-53.8-120-120s53.8-120%20120-120%20120%2053.8%20120%20120-53.8%20120-120%20120zm0-208c-48.5%200-88%2039.5-88%2088s39.5%2088%2088%2088%2088-39.5%2088-88-39.5-88-88-88zm-48%20104c-8.8%200-16-7.2-16-16%200-35.3%2028.7-64%2064-64%208.8%200%2016%207.2%2016%2016s-7.2%2016-16%2016c-17.6%200-32%2014.4-32%2032%200%208.8-7.2%2016-16%2016z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E";

function _extends$5() { _extends$5 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$5.apply(this, arguments); }

var _ref$5 = /*#__PURE__*/React.createElement("path", {
  d: "M48 32C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48H48zm0 32h106c3.3 0 6 2.7 6 6v20c0 3.3-2.7 6-6 6H38c-3.3 0-6-2.7-6-6V80c0-8.8 7.2-16 16-16zm426 96H38c-3.3 0-6-2.7-6-6v-36c0-3.3 2.7-6 6-6h138l30.2-45.3c1.1-1.7 3-2.7 5-2.7H464c8.8 0 16 7.2 16 16v74c0 3.3-2.7 6-6 6zM256 424c-66.2 0-120-53.8-120-120s53.8-120 120-120 120 53.8 120 120-53.8 120-120 120zm0-208c-48.5 0-88 39.5-88 88s39.5 88 88 88 88-39.5 88-88-39.5-88-88-88zm-48 104c-8.8 0-16-7.2-16-16 0-35.3 28.7-64 64-64 8.8 0 16 7.2 16 16s-7.2 16-16 16c-17.6 0-32 14.4-32 32 0 8.8-7.2 16-16 16z"
});

var SvgComponent$5 = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$5({
    "aria-hidden": "true",
    "data-prefix": "fas",
    "data-icon": "camera-retro",
    className: "svg-inline--fa fa-camera-retro fa-w-16",
    viewBox: "0 0 512 512"
  }, props), _ref$5);
};

var camera_solid = "data:image/svg+xml,%3Csvg%20aria-hidden%3D%22true%22%20focusable%3D%22false%22%20data-prefix%3D%22fas%22%20data-icon%3D%22camera-retro%22%20class%3D%22svg-inline--fa%20fa-camera-retro%20fa-w-16%22%20role%3D%22img%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20512%20512%22%3E%3Cpath%20d%3D%22M48%2032C21.5%2032%200%2053.5%200%2080v352c0%2026.5%2021.5%2048%2048%2048h416c26.5%200%2048-21.5%2048-48V80c0-26.5-21.5-48-48-48H48zm0%2032h106c3.3%200%206%202.7%206%206v20c0%203.3-2.7%206-6%206H38c-3.3%200-6-2.7-6-6V80c0-8.8%207.2-16%2016-16zm426%2096H38c-3.3%200-6-2.7-6-6v-36c0-3.3%202.7-6%206-6h138l30.2-45.3c1.1-1.7%203-2.7%205-2.7H464c8.8%200%2016%207.2%2016%2016v74c0%203.3-2.7%206-6%206zM256%20424c-66.2%200-120-53.8-120-120s53.8-120%20120-120%20120%2053.8%20120%20120-53.8%20120-120%20120zm0-208c-48.5%200-88%2039.5-88%2088s39.5%2088%2088%2088%2088-39.5%2088-88-39.5-88-88-88zm-48%20104c-8.8%200-16-7.2-16-16%200-35.3%2028.7-64%2064-64%208.8%200%2016%207.2%2016%2016s-7.2%2016-16%2016c-17.6%200-32%2014.4-32%2032%200%208.8-7.2%2016-16%2016z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E";

function _extends$6() { _extends$6 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$6.apply(this, arguments); }

var _ref$6 = /*#__PURE__*/React.createElement("defs", null);

var _ref2$3 = /*#__PURE__*/React.createElement("g", {
  id: "\u0421\u043B\u043E\u0439_x0020_1"
}, /*#__PURE__*/React.createElement("g", {
  id: "_2538414027232"
}, /*#__PURE__*/React.createElement("path", {
  className: "fil0",
  d: "M4789 2757h-305c-55 0-101 45-101 100v2452c0 34 17 63 42 82l1250 1084c41 37 105 32 142-10l199-230c37-42 32-105-10-142l-1055-916c-42-40-60-89-62-143V2857c0-55-45-100-100-100z"
}), /*#__PURE__*/React.createElement("path", {
  className: "fil0",
  d: "M8414 4907c0-2086-1692-3777-3778-3777S859 2821 859 4907s1691 3777 3777 3777 3778-1691 3778-3777zM4636 1633c1808 0 3274 1466 3274 3274S6444 8181 4636 8181 1363 6715 1363 4907s1465-3274 3273-3274z"
})), /*#__PURE__*/React.createElement("path", {
  className: "fil1",
  d: "M4383 3220h506v1748h-506z"
}), /*#__PURE__*/React.createElement("path", {
  className: "fil1",
  d: "M4383 3220h506v1748h-506z"
}));

var SvgComponent$6 = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$6({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$6, _ref2$3);
};

var clock_light = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil1%20%7Bfill%3A%232B2A29%7D%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cg%20id%3D%22_2538414027232%22%3E%20%20%20%3Cpath%20class%3D%22fil0%22%20d%3D%22M4789%202757l-305%200c-55%2C0%20-101%2C45%20-101%2C100l0%202452c0%2C34%2017%2C63%2042%2C82l0%200%201250%201084c41%2C37%20105%2C32%20142%2C-10l199%20-230c37%2C-42%2032%2C-105%20-10%2C-142l-1055%20-916c-42%2C-40%20-60%2C-89%20-62%2C-143l0%20-2177c0%2C-55%20-45%2C-100%20-100%2C-100z%22%2F%3E%20%20%20%3Cpath%20class%3D%22fil0%22%20d%3D%22M8414%204907c0%2C-2086%20-1692%2C-3777%20-3778%2C-3777%20-2086%2C0%20-3777%2C1691%20-3777%2C3777%200%2C2086%201691%2C3777%203777%2C3777%202086%2C0%203778%2C-1691%203778%2C-3777zm-3778%20-3274c1808%2C0%203274%2C1466%203274%2C3274%200%2C1808%20-1466%2C3274%20-3274%2C3274%20-1808%2C0%20-3273%2C-1466%20-3273%2C-3274%200%2C-1808%201465%2C-3274%203273%2C-3274z%22%2F%3E%20%20%3C%2Fg%3E%20%20%3Crect%20class%3D%22fil1%22%20x%3D%224383%22%20y%3D%223220%22%20width%3D%22506%22%20height%3D%221748%22%2F%3E%20%20%3Crect%20class%3D%22fil1%22%20x%3D%224383%22%20y%3D%223220%22%20width%3D%22506%22%20height%3D%221748%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$7() { _extends$7 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$7.apply(this, arguments); }

var _ref$7 = /*#__PURE__*/React.createElement("defs", null);

var _ref2$4 = /*#__PURE__*/React.createElement("g", {
  id: "\u0421\u043B\u043E\u0439_x0020_1"
}, /*#__PURE__*/React.createElement("g", {
  id: "_2538416352320"
}, /*#__PURE__*/React.createElement("path", {
  d: "M4863 2763h-453c-83 0-151 68-151 151v2338c0 46 21 88 53 116l1120 1000c62 55 158 49 214-13l302-338c55-62 49-158-13-213l-803-718c-74-70-111-157-118-256V2914c0-83-68-151-151-151z"
}), /*#__PURE__*/React.createElement("path", {
  d: "M4866 2763h-463c-85 0-154 69-154 154v2391c0 47 21 89 54 117l1 1 1144 1022c64 56 162 51 218-13l309-345c57-64 51-162-12-218l-822-734c-76-72-114-160-120-262V2917c0-85-70-154-155-154z"
}), /*#__PURE__*/React.createElement("path", {
  d: "M4636 1139c-2086 0-3777 1691-3777 3777s1691 3777 3777 3777 3778-1691 3778-3777-1692-3777-3778-3777zm0 755c1669 0 3022 1353 3022 3022S6305 7938 4636 7938 1615 6585 1615 4916s1352-3022 3021-3022z"
})), /*#__PURE__*/React.createElement("path", {
  d: "M4249 3213h772v1663h-772z"
}));

var SvgComponent$7 = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$7({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$7, _ref2$4);
};

var clock_regular = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil1%20%7Bfill%3A%232B2A29%7D%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cg%20id%3D%22_2538416352320%22%3E%20%20%20%3Cpath%20d%3D%22M4863%202763l-453%200c-83%2C0%20-151%2C68%20-151%2C151l0%202338c0%2C46%2021%2C88%2053%2C116l0%200%201120%201000c62%2C55%20158%2C49%20214%2C-13l302%20-338c55%2C-62%2049%2C-158%20-13%2C-213l-803%20-718c-74%2C-70%20-111%2C-157%20-118%2C-256l0%20-1916c0%2C-83%20-68%2C-151%20-151%2C-151z%22%2F%3E%20%20%20%3Cpath%20d%3D%22M4866%202763l-463%200c-85%2C0%20-154%2C69%20-154%2C154l0%202391c0%2C47%2021%2C89%2054%2C117l1%201%201144%201022c64%2C56%20162%2C51%20218%2C-13l309%20-345c57%2C-64%2051%2C-162%20-12%2C-218l-822%20-734c-76%2C-72%20-114%2C-160%20-120%2C-262l0%20-1959c0%2C-85%20-70%2C-154%20-155%2C-154z%22%2F%3E%20%20%20%3Cpath%20d%3D%22M4636%201139c-2086%2C0%20-3777%2C1691%20-3777%2C3777%200%2C2086%201691%2C3777%203777%2C3777%202086%2C0%203778%2C-1691%203778%2C-3777%200%2C-2086%20-1692%2C-3777%20-3778%2C-3777zm0%20755c1669%2C0%203022%2C1353%203022%2C3022%200%2C1669%20-1353%2C3022%20-3022%2C3022%20-1669%2C0%20-3021%2C-1353%20-3021%2C-3022%200%2C-1669%201352%2C-3022%203021%2C-3022z%22%2F%3E%20%20%3C%2Fg%3E%20%20%3Crect%20x%3D%224249%22%20y%3D%223213%22%20width%3D%22772%22%20height%3D%221663%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$8() { _extends$8 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$8.apply(this, arguments); }

var _ref$8 = /*#__PURE__*/React.createElement("defs", null);

var _ref2$5 = /*#__PURE__*/React.createElement("g", {
  id: "\u0421\u043B\u043E\u0439_x0020_1"
}, /*#__PURE__*/React.createElement("g", {
  id: "_2538414010272"
}, /*#__PURE__*/React.createElement("path", {
  d: "M8414 4907c0-2086-1692-3777-3778-3777S859 2821 859 4907s1691 3777 3777 3777 3778-1691 3778-3777z"
}), /*#__PURE__*/React.createElement("path", {
  d: "M4863 2754h-453c-83 0-151 68-151 151v2338c0 46 21 88 53 115v1l1120 999c62 56 158 50 214-12l302-338c55-62 49-158-13-213l-803-718c-74-70-111-157-118-256V2905c0-83-68-151-151-151z",
  fill: "#fefefe"
})));

var SvgComponent$8 = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$8({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$8, _ref2$5);
};

var clock_solid = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil1%20%7Bfill%3A%23FEFEFE%7D%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cg%20id%3D%22_2538414010272%22%3E%20%20%20%3Cpath%20d%3D%22M8414%204907c0%2C-2086%20-1692%2C-3777%20-3778%2C-3777%20-2086%2C0%20-3777%2C1691%20-3777%2C3777%200%2C2086%201691%2C3777%203777%2C3777%202086%2C0%203778%2C-1691%203778%2C-3777z%22%2F%3E%20%20%20%3Cpath%20class%3D%22fil1%22%20d%3D%22M4863%202754l-453%200c-83%2C0%20-151%2C68%20-151%2C151l0%202338c0%2C46%2021%2C88%2053%2C115l0%201%201120%20999c62%2C56%20158%2C50%20214%2C-12l302%20-338c55%2C-62%2049%2C-158%20-13%2C-213l-803%20-718c-74%2C-70%20-111%2C-157%20-118%2C-256l0%20-1916c0%2C-83%20-68%2C-151%20-151%2C-151z%22%2F%3E%20%20%3C%2Fg%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$9() { _extends$9 = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$9.apply(this, arguments); }

var _ref$9 = /*#__PURE__*/React.createElement("path", {
  d: "M537.6 226.6c4.1-10.7 6.4-22.4 6.4-34.6 0-53-43-96-96-96-19.7 0-38.1 6-53.3 16.2C367 64.2 315.3 32 256 32c-88.4 0-160 71.6-160 160 0 2.7.1 5.4.2 8.1C40.2 219.8 0 273.2 0 336c0 79.5 64.5 144 144 144h368c70.7 0 128-57.3 128-128 0-61.9-44-113.6-102.4-125.4z"
});

var SvgComponent$9 = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$9({
    "aria-hidden": "true",
    "data-prefix": "fas",
    "data-icon": "cloud",
    className: "svg-inline--fa fa-cloud fa-w-20",
    viewBox: "0 0 640 512"
  }, props), _ref$9);
};

var cloud_solid = "data:image/svg+xml,%3Csvg%20aria-hidden%3D%22true%22%20focusable%3D%22false%22%20data-prefix%3D%22fas%22%20data-icon%3D%22cloud%22%20class%3D%22svg-inline--fa%20fa-cloud%20fa-w-20%22%20role%3D%22img%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20640%20512%22%3E%3Cpath%20d%3D%22M537.6%20226.6c4.1-10.7%206.4-22.4%206.4-34.6%200-53-43-96-96-96-19.7%200-38.1%206-53.3%2016.2C367%2064.2%20315.3%2032%20256%2032c-88.4%200-160%2071.6-160%20160%200%202.7.1%205.4.2%208.1C40.2%20219.8%200%20273.2%200%20336c0%2079.5%2064.5%20144%20144%20144h368c70.7%200%20128-57.3%20128-128%200-61.9-44-113.6-102.4-125.4z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E";

function _extends$a() { _extends$a = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$a.apply(this, arguments); }

var _ref$a = /*#__PURE__*/React.createElement("path", {
  d: "M239.1 6.3l-208 78c-18.7 7-31.1 25-31.1 45v225.1c0 18.2 10.3 34.8 26.5 42.9l208 104c13.5 6.8 29.4 6.8 42.9 0l208-104c16.3-8.1 26.5-24.8 26.5-42.9V129.3c0-20-12.4-37.9-31.1-44.9l-208-78C262 2.2 250 2.2 239.1 6.3zM256 68.4l192 72v1.1l-192 78-192-78v-1.1l192-72zm32 356V275.5l160-65v133.9l-160 80z"
});

var SvgComponent$a = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$a({
    "aria-hidden": "true",
    "data-prefix": "fas",
    "data-icon": "cube",
    className: "svg-inline--fa fa-cube fa-w-16",
    viewBox: "0 0 512 512"
  }, props), _ref$a);
};

var cube_solid = "data:image/svg+xml,%3Csvg%20aria-hidden%3D%22true%22%20focusable%3D%22false%22%20data-prefix%3D%22fas%22%20data-icon%3D%22cube%22%20class%3D%22svg-inline--fa%20fa-cube%20fa-w-16%22%20role%3D%22img%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20512%20512%22%3E%3Cpath%20d%3D%22M239.1%206.3l-208%2078c-18.7%207-31.1%2025-31.1%2045v225.1c0%2018.2%2010.3%2034.8%2026.5%2042.9l208%20104c13.5%206.8%2029.4%206.8%2042.9%200l208-104c16.3-8.1%2026.5-24.8%2026.5-42.9V129.3c0-20-12.4-37.9-31.1-44.9l-208-78C262%202.2%20250%202.2%20239.1%206.3zM256%2068.4l192%2072v1.1l-192%2078-192-78v-1.1l192-72zm32%20356V275.5l160-65v133.9l-160%2080z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E";

function _extends$b() { _extends$b = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$b.apply(this, arguments); }

var _ref$b = /*#__PURE__*/React.createElement("path", {
  d: "M12 20V10M18 20V4M6 20v-4"
});

var SvgComponent$b = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$b({
    viewBox: "0 0 24 24",
    fill: "none",
    stroke: "currentColor",
    strokeWidth: 2,
    strokeLinecap: "round",
    strokeLinejoin: "round",
    className: "feather feather-bar-chart"
  }, props), _ref$b);
};

var diogramm_regular = "data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20viewBox%3D%220%200%2024%2024%22%20fill%3D%22none%22%20stroke%3D%22currentColor%22%20stroke-width%3D%222%22%20stroke-linecap%3D%22round%22%20stroke-linejoin%3D%22round%22%20class%3D%22feather%20feather-bar-chart%22%3E%3Cline%20x1%3D%2212%22%20y1%3D%2220%22%20x2%3D%2212%22%20y2%3D%2210%22%3E%3C%2Fline%3E%3Cline%20x1%3D%2218%22%20y1%3D%2220%22%20x2%3D%2218%22%20y2%3D%224%22%3E%3C%2Fline%3E%3Cline%20x1%3D%226%22%20y1%3D%2220%22%20x2%3D%226%22%20y2%3D%2216%22%3E%3C%2Fline%3E%3C%2Fsvg%3E";

function _extends$c() { _extends$c = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$c.apply(this, arguments); }

var _ref$c = /*#__PURE__*/React.createElement("defs", null);

var _ref2$6 = /*#__PURE__*/React.createElement("g", {
  id: "\u0421\u043B\u043E\u0439_x0020_1"
}, /*#__PURE__*/React.createElement("g", {
  id: "_2538414056192"
}, /*#__PURE__*/React.createElement("path", {
  className: "fil0",
  d: "M7108 1139h505v1901l1887 1521c35 27 35 71 10 100l-228 283c-26 35-71 35-100 10L5236 1772c-128-104-308-97-429 0L861 4954c-29 25-73 25-100-10l-228-283c-25-29-25-74 10-100l4052-3266c241-194 598-207 854 1l1659 1336V1139z"
}), /*#__PURE__*/React.createElement("path", {
  className: "fil0",
  d: "M1650 5485v3072c0 69 51 116 116 116h2080c65 0 116-47 116-116V6623c0-68 50-115 115-115h1909c66 0 116 47 116 115v1934c0 69 50 116 116 116h2080c65 0 115-47 115-116l1-3070c0-37-16-65-41-85L5139 2791c-66-51-148-51-212 0L1691 5400c-25 21-41 48-41 85zm504 2608V5673l2825-2277c5-5 11-9 17-12 28-18 56-16 84 7l2829 2285v2417c0 45-31 76-75 76H6680c-10 0-20-2-28-6-28-11-47-38-47-70l-2-1768c0-193-123-318-292-318H3774c-160 0-315 141-315 318v1773c0 44-33 71-76 71H2230c-46 0-76-29-76-76z"
})));

var SvgComponent$c = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$c({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$c, _ref2$6);
};

var home_light = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil0%20%7Bfill%3A%232B2A29%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cg%20id%3D%22_2538414056192%22%3E%20%20%20%3Cpath%20class%3D%22fil0%22%20d%3D%22M7108%201139l505%200%200%201901%201887%201521c35%2C27%2035%2C71%2010%2C100l-228%20283c-26%2C35%20-71%2C35%20-100%2C10l-3946%20-3182c-128%2C-104%20-308%2C-97%20-429%2C0l-3946%203182c-29%2C25%20-73%2C25%20-100%2C-10l-228%20-283c-25%2C-29%20-25%2C-74%2010%2C-100l4052%20-3266c241%2C-194%20598%2C-207%20854%2C1l1659%201336%200%20-1493z%22%2F%3E%20%20%20%3Cpath%20class%3D%22fil0%22%20d%3D%22M1650%205485l0%203072c0%2C69%2051%2C116%20116%2C116l2080%200c65%2C0%20116%2C-47%20116%2C-116l0%20-1934c0%2C-68%2050%2C-115%20115%2C-115l1909%200c66%2C0%20116%2C47%20116%2C115l0%201934c0%2C69%2050%2C116%20116%2C116l2080%200c65%2C0%20115%2C-47%20115%2C-116l1%20-3070c0%2C-37%20-16%2C-65%20-41%2C-85l-3234%20-2611c-66%2C-51%20-148%2C-51%20-212%2C0l-3236%202609c-25%2C21%20-41%2C48%20-41%2C85zm504%202608l0%20-2420%202825%20-2277c5%2C-5%2011%2C-9%2017%2C-12%2028%2C-18%2056%2C-16%2084%2C7l2829%202285%200%202417c0%2C45%20-31%2C76%20-75%2C76l-1154%200c-10%2C0%20-20%2C-2%20-28%2C-6%20-28%2C-11%20-47%2C-38%20-47%2C-70l-2%20-1768c0%2C-193%20-123%2C-318%20-292%2C-318l-2537%200c-160%2C0%20-315%2C141%20-315%2C318l0%201768%200%205c0%2C44%20-33%2C71%20-76%2C71l-1153%200c-46%2C0%20-76%2C-29%20-76%2C-76z%22%2F%3E%20%20%3C%2Fg%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$d() { _extends$d = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$d.apply(this, arguments); }

var _ref$d = /*#__PURE__*/React.createElement("defs", null);

var _ref2$7 = /*#__PURE__*/React.createElement("g", {
  id: "\u0421\u043B\u043E\u0439_x0020_1"
}, /*#__PURE__*/React.createElement("g", {
  id: "_2538416360416"
}, /*#__PURE__*/React.createElement("path", {
  className: "fil0",
  d: "M3308 5747h3483"
}), /*#__PURE__*/React.createElement("path", {
  className: "fil1",
  d: "M6506 7826V5833c0-51-38-86-86-86H3679c-48 0-86 35-86 86v1993c0 51-37 86-86 86h-980c-49 0-87-35-87-86V5844c0-28 12-48 31-64l2550-2052c33-25 60-27 94 1l2513 2051c19 16 31 36 31 64v1982c0 51-38 86-87 86h-980c-49 0-86-35-86-86zM5157 2803c-66-51-148-52-213 0L1709 5411c-26 21-41 48-41 86v3071c0 69 50 116 116 116h2079c66 0 116-47 116-116V6635c0-69 50-116 116-116h1909c65 0 116 47 116 116v1933c0 69 50 116 115 116h2080c66 0 116-47 116-116V5498c0-37-15-64-41-85L5157 2803zm1735 5008V5908c0-76-54-161-149-161h-323c48 0 86 35 86 86v1993c0 51 37 86 86 86h400c-57-1-100-42-100-101zm-3299 15V5833c0-51 38-86 86-86h-311c-94 0-161 75-161 167v1897c0 60-44 101-101 101h401c49 0 86-35 86-86z"
}), /*#__PURE__*/React.createElement("path", {
  className: "fil1",
  d: "M770 4531l330 409c46 61 139 58 190 14l3600-2915c86-69 250-74 331 2l3601 2902c51 44 151 47 197-15l321-398 6-7v-1c38-51 35-123-23-168L7827 3150l-2-2025h-772l3 1403-1510-1217c-295-238-705-224-982 0L787 4355c-61 46-62 124-17 176z"
}), /*#__PURE__*/React.createElement("path", {
  className: "fil0",
  d: "M5762 6518H4336M6892 7811c0 60 44 101 101 101M6892 5847c0-59-44-100-101-100M6892 5847v1964"
})));

var SvgComponent$d = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$d({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$d, _ref2$7);
};

var home_regular = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil0%20%7Bfill%3Anone%7D%20%20%20%20.fil1%20%7Bfill%3A%232B2A29%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cg%20id%3D%22_2538416360416%22%3E%20%20%20%3Cline%20class%3D%22fil0%22%20x1%3D%223308%22%20y1%3D%225747%22%20x2%3D%226791%22%20y2%3D%20%225747%22%20%2F%3E%20%20%20%3Cpath%20class%3D%22fil1%22%20d%3D%22M6506%207826l0%20-1993c0%2C-51%20-38%2C-86%20-86%2C-86l-2741%200%200%200c-48%2C0%20-86%2C35%20-86%2C86l0%201993c0%2C51%20-37%2C86%20-86%2C86l-401%200c0%2C0%200%2C0%200%2C0l-579%200c-49%2C0%20-87%2C-35%20-87%2C-86l0%20-1982c0%2C-28%2012%2C-48%2031%2C-64l2550%20-2052c33%2C-25%2060%2C-27%2094%2C1l2513%202051c19%2C16%2031%2C36%2031%2C64l0%201982c0%2C51%20-38%2C86%20-87%2C86l-579%200c-1%2C0%20-1%2C0%20-1%2C0l-400%200c-49%2C0%20-86%2C-35%20-86%2C-86zm-1349%20-5023c-66%2C-51%20-148%2C-52%20-213%2C0l-3235%202608c-26%2C21%20-41%2C48%20-41%2C86l0%203071c0%2C69%2050%2C116%20116%2C116l2079%200c66%2C0%20116%2C-47%20116%2C-116l0%20-1933c0%2C-69%2050%2C-116%20116%2C-116l1909%200c65%2C0%20116%2C47%20116%2C116l0%201933c0%2C69%2050%2C116%20115%2C116l2080%200c66%2C0%20116%2C-47%20116%2C-116l0%20-3070c0%2C-37%20-15%2C-64%20-41%2C-85l-3233%20-2610zm1735%205008l0%20-1903c0%2C-76%20-54%2C-161%20-149%2C-161l-323%200c48%2C0%2086%2C35%2086%2C86l0%201993c0%2C51%2037%2C86%2086%2C86l400%200c-57%2C-1%20-100%2C-42%20-100%2C-101zm-3299%2015l0%20-1993c0%2C-51%2038%2C-86%2086%2C-86l-311%200c-94%2C0%20-161%2C75%20-161%2C167l0%201897c0%2C60%20-44%2C101%20-101%2C101l401%200c49%2C0%2086%2C-35%2086%2C-86z%22%2F%3E%20%20%20%3Cpath%20class%3D%22fil1%22%20d%3D%22M770%204531l330%20409c46%2C61%20139%2C58%20190%2C14l3600%20-2915c86%2C-69%20250%2C-74%20331%2C2l3601%202902c51%2C44%20151%2C47%20197%2C-15l321%20-398%206%20-7c0%2C0%200%2C0%200%2C-1%2038%2C-51%2035%2C-123%20-23%2C-168l-1496%20-1204%20-2%20-2025%20-772%200%203%201403%20-1510%20-1217c-295%2C-238%20-705%2C-224%20-982%2C0l-3777%203044c-61%2C46%20-62%2C124%20-17%2C176z%22%2F%3E%20%20%20%3Cline%20class%3D%22fil0%22%20x1%3D%225762%22%20y1%3D%226518%22%20x2%3D%224336%22%20y2%3D%20%226518%22%20%2F%3E%20%20%20%3Cpath%20class%3D%22fil0%22%20d%3D%22M6892%207811c0%2C60%2044%2C101%20101%2C101%22%2F%3E%20%20%20%3Cpath%20class%3D%22fil0%22%20d%3D%22M6892%205847c0%2C-59%20-44%2C-100%20-101%2C-100%22%2F%3E%20%20%20%3Cline%20class%3D%22fil0%22%20x1%3D%226892%22%20y1%3D%225847%22%20x2%3D%226892%22%20y2%3D%20%227811%22%20%2F%3E%20%20%3C%2Fg%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$e() { _extends$e = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$e.apply(this, arguments); }

var _ref$e = /*#__PURE__*/React.createElement("defs", null);

var _ref2$8 = /*#__PURE__*/React.createElement("g", {
  id: "\u0421\u043B\u043E\u0439_x0020_1"
}, /*#__PURE__*/React.createElement("g", {
  id: "_2538414068640"
}, /*#__PURE__*/React.createElement("path", {
  className: "fil0",
  d: "M752 4531l400 496c47 62 125 62 176 18l3593-2897c60-49 157-52 221 0l3600 2902c52 44 129 43 176-18l405-502 6-7v-1c37-51 34-123-24-168L7871 3199V1150h-887v1334L5528 1311c-294-238-704-224-981 0L770 4355c-62 46-62 124-18 176z"
}), /*#__PURE__*/React.createElement("path", {
  className: "fil0",
  d: "M8372 5413L5139 2803c-66-51-148-52-212 0L1691 5411c-25 21-41 48-41 86v3071c0 69 50 116 116 116h2080c65 0 115-47 115-116V6635c0-69 51-116 116-116h1909c66 0 116 47 116 116v1933c0 69 50 116 116 116h2080c65 0 115-47 115-116l1-3070c0-37-16-64-42-85z"
})));

var SvgComponent$e = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$e({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$e, _ref2$8);
};

var home_solid = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil0%20%7Bfill%3A%232B2A29%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cg%20id%3D%22_2538414068640%22%3E%20%20%20%3Cpath%20class%3D%22fil0%22%20d%3D%22M752%204531l400%20496c47%2C62%20125%2C62%20176%2C18l3593%20-2897c60%2C-49%20157%2C-52%20221%2C0l3600%202902c52%2C44%20129%2C43%20176%2C-18l405%20-502%206%20-7c0%2C0%200%2C0%200%2C-1%2037%2C-51%2034%2C-123%20-24%2C-168l-1434%20-1155%200%20-2049%20-887%200%200%201334%20-1456%20-1173c-294%2C-238%20-704%2C-224%20-981%2C0l-3777%203044c-62%2C46%20-62%2C124%20-18%2C176z%22%2F%3E%20%20%20%3Cpath%20class%3D%22fil0%22%20d%3D%22M8372%205413l-3233%20-2610c-66%2C-51%20-148%2C-52%20-212%2C0l-3236%202608c-25%2C21%20-41%2C48%20-41%2C86l0%203071c0%2C69%2050%2C116%20116%2C116l2080%200c65%2C0%20115%2C-47%20115%2C-116l0%20-1933c0%2C-69%2051%2C-116%20116%2C-116l1909%200c66%2C0%20116%2C47%20116%2C116l0%201933c0%2C69%2050%2C116%20116%2C116l2080%200c65%2C0%20115%2C-47%20115%2C-116l1%20-3070c0%2C-37%20-16%2C-64%20-42%2C-85z%22%2F%3E%20%20%3C%2Fg%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$f() { _extends$f = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$f.apply(this, arguments); }

var _ref$f = /*#__PURE__*/React.createElement("path", {
  fill: "currentColor",
  d: "M480 416v16c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V176c0-26.51 21.49-48 48-48h16v48H54a6 6 0 00-6 6v244a6 6 0 006 6h372a6 6 0 006-6v-10h48zm42-336H150a6 6 0 00-6 6v244a6 6 0 006 6h372a6 6 0 006-6V86a6 6 0 00-6-6zm6-48c26.51 0 48 21.49 48 48v256c0 26.51-21.49 48-48 48H144c-26.51 0-48-21.49-48-48V80c0-26.51 21.49-48 48-48h384zM264 144c0 22.091-17.909 40-40 40s-40-17.909-40-40 17.909-40 40-40 40 17.909 40 40zm-72 96l39.515-39.515c4.686-4.686 12.284-4.686 16.971 0L288 240l103.515-103.515c4.686-4.686 12.284-4.686 16.971 0L480 208v80H192v-48z"
});

var SvgComponent$f = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$f({
    "aria-hidden": "true",
    "data-prefix": "far",
    "data-icon": "images",
    className: "svg-inline--fa fa-images fa-w-18",
    viewBox: "0 0 576 512"
  }, props), _ref$f);
};

var images_regular = "data:image/svg+xml,%3Csvg%20aria-hidden%3D%22true%22%20focusable%3D%22false%22%20data-prefix%3D%22far%22%20data-icon%3D%22images%22%20class%3D%22svg-inline--fa%20fa-images%20fa-w-18%22%20role%3D%22img%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20576%20512%22%3E%3Cpath%20fill%3D%22currentColor%22%20d%3D%22M480%20416v16c0%2026.51-21.49%2048-48%2048H48c-26.51%200-48-21.49-48-48V176c0-26.51%2021.49-48%2048-48h16v48H54a6%206%200%200%200-6%206v244a6%206%200%200%200%206%206h372a6%206%200%200%200%206-6v-10h48zm42-336H150a6%206%200%200%200-6%206v244a6%206%200%200%200%206%206h372a6%206%200%200%200%206-6V86a6%206%200%200%200-6-6zm6-48c26.51%200%2048%2021.49%2048%2048v256c0%2026.51-21.49%2048-48%2048H144c-26.51%200-48-21.49-48-48V80c0-26.51%2021.49-48%2048-48h384zM264%20144c0%2022.091-17.909%2040-40%2040s-40-17.909-40-40%2017.909-40%2040-40%2040%2017.909%2040%2040zm-72%2096l39.515-39.515c4.686-4.686%2012.284-4.686%2016.971%200L288%20240l103.515-103.515c4.686-4.686%2012.284-4.686%2016.971%200L480%20208v80H192v-48z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E";

function _extends$g() { _extends$g = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$g.apply(this, arguments); }

var _ref$g = /*#__PURE__*/React.createElement("defs", null);

var _ref2$9 = /*#__PURE__*/React.createElement("path", {
  d: "M5879 1431c-262-459-980-479-1254-4L1136 7473c-311 537-93 1209 710 1209h6680c789 47 1192-639 798-1211L5879 1431zm-822 264c82-141 286-156 367-15l3515 6130c95 152-29 370-227 370H1836c-207 8-385-241-277-426l3498-6059zm-34 1713h452c44 0 80 35 80 79v2341c0 44-36 80-80 80h-452c-44 0-80-36-80-80V3487c0-44 36-79 80-79zm0 3203h452c44 0 80 36 80 80v452c0 43-36 79-80 79h-452c-44 0-80-36-80-79v-452c0-44 36-80 80-80z",
  id: "\u0421\u043B\u043E\u0439_x0020_1"
});

var SvgComponent$g = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$g({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$g, _ref2$9);
};

var issue_light = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cpath%20d%3D%22M5879%201431c-262%2C-459%20-980%2C-479%20-1254%2C-4l-3489%206046c-311%2C537%20-93%2C1209%20710%2C1209l6680%200c789%2C47%201192%2C-639%20798%2C-1211l-3445%20-6040zm-822%20264c82%2C-141%20286%2C-156%20367%2C-15l3515%206130c95%2C152%20-29%2C370%20-227%2C370l-6876%200c-207%2C8%20-385%2C-241%20-277%2C-426l3498%20-6059zm-34%201713l452%200c44%2C0%2080%2C35%2080%2C79l0%202341c0%2C44%20-36%2C80%20-80%2C80l-452%200c-44%2C0%20-80%2C-36%20-80%2C-80l0%20-2341c0%2C-44%2036%2C-79%2080%2C-79zm0%203203l452%200c44%2C0%2080%2C36%2080%2C80l0%20452c0%2C43%20-36%2C79%20-80%2C79l-452%200c-44%2C0%20-80%2C-36%20-80%2C-79l0%20-452c0%2C-44%2036%2C-80%2080%2C-80z%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$h() { _extends$h = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$h.apply(this, arguments); }

var _ref$h = /*#__PURE__*/React.createElement("defs", null);

var _ref2$a = /*#__PURE__*/React.createElement("path", {
  d: "M4983 3405h575c56 0 102 46 102 102v2297c0 56-46 102-102 102h-575c-55 0-101-46-101-102V3507c0-56 46-102 101-102zm0 3038h575c56 0 102 45 102 101v575c0 56-46 102-102 102h-575c-55 0-101-46-101-102v-575c0-56 46-101 101-101zM1130 7473c-311 537-93 1209 710 1209h6680c789 47 1192-639 798-1211L5873 1429c-262-459-980-479-1254-4L1130 7473zm797 168l3221-5591c63-108 144-121 208-8l3173 5618c68 119-9 244-168 244H2092c-135 2-246-131-165-263z",
  id: "\u0421\u043B\u043E\u0439_x0020_1"
});

var SvgComponent$h = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$h({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$h, _ref2$a);
};

var issue_regular = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil0%20%7Bfill%3A%232B2A29%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cpath%20d%3D%22M4983%203405l575%200c56%2C0%20102%2C46%20102%2C102l0%202297c0%2C56%20-46%2C102%20-102%2C102l-575%200c-55%2C0%20-101%2C-46%20-101%2C-102l0%20-2297c0%2C-56%2046%2C-102%20101%2C-102zm0%203038l575%200c56%2C0%20102%2C45%20102%2C101l0%20575c0%2C56%20-46%2C102%20-102%2C102l-575%200c-55%2C0%20-101%2C-46%20-101%2C-102l0%20-575c0%2C-56%2046%2C-101%20101%2C-101zm-3853%201030c-311%2C537%20-93%2C1209%20710%2C1209l6680%200c789%2C47%201192%2C-639%20798%2C-1211l-3445%20-6042c-262%2C-459%20-980%2C-479%20-1254%2C-4l-3489%206048zm797%20168l3221%20-5591c63%2C-108%20144%2C-121%20208%2C-8l3173%205618c68%2C119%20-9%2C244%20-168%2C244l-6269%200c-135%2C2%20-246%2C-131%20-165%2C-263z%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$i() { _extends$i = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$i.apply(this, arguments); }

var _ref$i = /*#__PURE__*/React.createElement("defs", null);

var _ref2$b = /*#__PURE__*/React.createElement("path", {
  d: "M1840 8744h6680c789 47 1192-639 798-1211L5873 1491c-262-459-980-479-1254-4L1130 7535c-311 537-93 1209 710 1209zm3143-5274h575c56 0 102 45 102 101v2300c0 56-46 101-102 101h-575c-55 0-101-45-101-101V3571c0-56 46-101 101-101zm-19 3039h575c56 0 102 46 102 102v575c0 55-46 101-102 101h-575c-55 0-101-46-101-101v-575c0-56 46-102 101-102z",
  id: "\u0421\u043B\u043E\u0439_x0020_1"
});

var SvgComponent$i = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$i({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$i, _ref2$b);
};

var issue_solid = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cpath%20d%3D%22M1840%208744l6680%200c789%2C47%201192%2C-639%20798%2C-1211l-3445%20-6042c-262%2C-459%20-980%2C-479%20-1254%2C-4l-3489%206048c-311%2C537%20-93%2C1209%20710%2C1209zm3143%20-5274l575%200c56%2C0%20102%2C45%20102%2C101l0%202300c0%2C56%20-46%2C101%20-102%2C101l-575%200c-55%2C0%20-101%2C-45%20-101%2C-101l0%20-2300c0%2C-56%2046%2C-101%20101%2C-101zm-19%203039l575%200c56%2C0%20102%2C46%20102%2C102l0%20575c0%2C55%20-46%2C101%20-102%2C101l-575%200c-55%2C0%20-101%2C-46%20-101%2C-101l0%20-575c0%2C-56%2046%2C-102%20101%2C-102z%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$j() { _extends$j = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$j.apply(this, arguments); }

var _ref$j = /*#__PURE__*/React.createElement("defs", null);

var _ref2$c = /*#__PURE__*/React.createElement("g", {
  id: "\u0421\u043B\u043E\u0439_x0020_1"
}, /*#__PURE__*/React.createElement("path", {
  d: "M8850 4518c375 300 621 761 621 1278 0 936-780 1681-1713 1638-104-6-246 5-294 189-192 615-765 1061-1442 1061-394 0-752-151-1021-398-122-131-221-61-297-17-296 226-666 360-1067 360-1010 0-1811-846-1761-1854-3-126-45-190-139-209-629-70-1119-605-1119-1253 0-397 183-751 469-982 83-72 93-111 74-242-21-117-32-238-32-361 0-1114 902-2018 2014-2018 233 0 456 40 664 112 143 43 237-8 304-60 317-260 722-417 1165-417 427 0 823 149 1136 395 35 38 101 111 60 209-72 105-89 121-158 186-68 47-154 23-193-9-55-41-126-84-173-115-178-118-385-205-735-178-358 27-743 270-981 456-139 133-304 113-447 46-701-273-1342-112-1744 339-467 524-459 793-339 1497 28 153-46 326-146 398-186 137-478 530-420 954 63 466 500 581 839 594 213 16 401 159 386 457-3 77 2 151 4 228 10 487 215 867 556 1116 640 467 1340 74 1685-159 151-118 394-114 548 15 287 233 636 559 1117 388 523-185 706-616 848-950 56-163 291-280 453-273 652 45 1110-229 1340-773 118-279 93-806-359-1255-11-10-82-72-121-123-38-64-25-119 12-158 42-44 82-86 135-132 128-98 242-4 271 20z"
}), /*#__PURE__*/React.createElement("path", {
  d: "M4793 5437l177-849c9-50 55-67 79-45l682 641c28 26 0 71-39 82l-836 230c-45 16-77-10-63-59z"
}), /*#__PURE__*/React.createElement("rect", {
  transform: "matrix(.25757 .24178 -.3471 .36977 7302.85 2083.93)",
  width: 2871,
  height: 5800,
  rx: 200,
  ry: 200
}), /*#__PURE__*/React.createElement("rect", {
  transform: "matrix(.25757 .24178 -.3471 .36977 8158.62 1179.22)",
  width: 2871,
  height: 1533,
  rx: 200,
  ry: 200
}));

var SvgComponent$j = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$j({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$j, _ref2$c);
};

var prescription_light = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil1%20%7Bfill%3A%232B2A29%7D%20%20%20%20.fil0%20%7Bfill%3A%232B2A29%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cpath%20d%3D%22M8850%204518c375%2C300%20621%2C761%20621%2C1278%200%2C936%20-780%2C1681%20-1713%2C1638%20-104%2C-6%20-246%2C5%20-294%2C189%20-192%2C615%20-765%2C1061%20-1442%2C1061%20-394%2C0%20-752%2C-151%20-1021%2C-398%20-122%2C-131%20-221%2C-61%20-297%2C-17%20-296%2C226%20-666%2C360%20-1067%2C360%20-1010%2C0%20-1811%2C-846%20-1761%2C-1854%20-3%2C-126%20-45%2C-190%20-139%2C-209%20-629%2C-70%20-1119%2C-605%20-1119%2C-1253%200%2C-397%20183%2C-751%20469%2C-982%2083%2C-72%2093%2C-111%2074%2C-242%20-21%2C-117%20-32%2C-238%20-32%2C-361%200%2C-1114%20902%2C-2018%202014%2C-2018%20233%2C0%20456%2C40%20664%2C112%20143%2C43%20237%2C-8%20304%2C-60%20317%2C-260%20722%2C-417%201165%2C-417%20427%2C0%20823%2C149%201136%2C395%2035%2C38%20101%2C111%2060%2C209%20-72%2C105%20-89%2C121%20-158%2C186%20-68%2C47%20-154%2C23%20-193%2C-9%20-55%2C-41%20-126%2C-84%20-173%2C-115%20-178%2C-118%20-385%2C-205%20-735%2C-178%20-358%2C27%20-743%2C270%20-981%2C456%20-139%2C133%20-304%2C113%20-447%2C46%20-701%2C-273%20-1342%2C-112%20-1744%2C339%20-467%2C524%20-459%2C793%20-339%2C1497%2028%2C153%20-46%2C326%20-146%2C398%20-186%2C137%20-478%2C530%20-420%2C954%2063%2C466%20500%2C581%20839%2C594%20213%2C16%20401%2C159%20386%2C457%20-3%2C77%202%2C151%204%2C228%2010%2C487%20215%2C867%20556%2C1116%20640%2C467%201340%2C74%201685%2C-159%20151%2C-118%20394%2C-114%20548%2C15%20287%2C233%20636%2C559%201117%2C388%20523%2C-185%20706%2C-616%20848%2C-950%2056%2C-163%20291%2C-280%20453%2C-273%20652%2C45%201110%2C-229%201340%2C-773%20118%2C-279%2093%2C-806%20-359%2C-1255%20-11%2C-10%20-82%2C-72%20-121%2C-123%20-38%2C-64%20-25%2C-119%2012%2C-158%2042%2C-44%2082%2C-86%20135%2C-132%20128%2C-98%20242%2C-4%20271%2C20z%22%2F%3E%20%20%3Cpath%20d%3D%22M4793%205437l177%20-849c9%2C-50%2055%2C-67%2079%2C-45l682%20641c28%2C26%200%2C71%20-39%2C82l-836%20230c-45%2C16%20-77%2C-10%20-63%2C-59z%22%2F%3E%20%20%3Crect%20transform%3D%22matrix%280.25757%200.241781%20-0.347099%200.369765%207302.85%202083.93%29%22%20width%3D%222871%22%20height%3D%225800%22%20rx%3D%22200%22%20ry%3D%22200%22%2F%3E%20%20%3Crect%20transform%3D%22matrix%280.25757%200.241781%20-0.347099%200.369765%208158.62%201179.22%29%22%20width%3D%222871%22%20height%3D%221533%22%20rx%3D%22200%22%20ry%3D%22200%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$k() { _extends$k = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$k.apply(this, arguments); }

var _ref$k = /*#__PURE__*/React.createElement("defs", null);

var _ref2$d = /*#__PURE__*/React.createElement("g", {
  id: "\u0421\u043B\u043E\u0439_x0020_1"
}, /*#__PURE__*/React.createElement("path", {
  d: "M8850 4524c375 300 621 761 621 1277 0 904-733 1637-1637 1637-25 0-50-1-76-2-104-6-246 5-294 189-192 614-765 1059-1442 1059-394 0-752-150-1021-397-122-131-221-61-297-17-296 225-666 360-1067 360-974 0-1763-790-1763-1763 0-30 1-60 2-89-3-126-45-189-139-209-629-70-1119-603-1119-1251 0-396 183-749 469-980 83-73 93-111 74-242-21-117-32-238-32-361 0-1112 902-2014 2015-2014 232 0 455 39 663 112 143 43 237-9 304-60 317-260 723-417 1165-417 427 0 823 149 1136 394 122 104 67 219 2 295-75 71-150 144-239 222-78 74-151 73-231 41-196-85-446-206-796-180-358 27-615 211-853 397-139 132-327 144-469 76-375-127-875-273-1292 46-584 447-720 951-599 1654 28 152-36 244-137 316-186 137-453 356-396 779 64 465 512 538 851 551 204-3 314 125 325 300 34 454 23 1082 419 1370 639 466 1271 95 1616-138 151-117 365-124 519 5 286 233 580 571 1060 401 523-185 601-602 743-936 56-163 236-276 398-269 409 0 1052 23 1282-520 118-279 165-618-287-1066-50-50-109-207-36-301 59-60 103-106 244-256 104-102 235-89 314-13z"
}), /*#__PURE__*/React.createElement("path", {
  d: "M4716 5534l94-1054c4-63 61-78 95-46l983 914c41 38 11 91-37 99l-1045 171c-56 14-101-24-90-84z"
}), /*#__PURE__*/React.createElement("rect", {
  transform: "matrix(.37141 .34534 -.34534 .37141 7134.83 1952.41)",
  width: 2871,
  height: 5800,
  rx: 200,
  ry: 200
}), /*#__PURE__*/React.createElement("rect", {
  transform: "matrix(.37141 .34534 -.34534 .37141 7987.97 1034.86)",
  width: 2871,
  height: 1533,
  rx: 200,
  ry: 200
}));

var SvgComponent$k = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$k({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$k, _ref2$d);
};

var prescription_regular = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil1%20%7Bfill%3A%232B2A29%7D%20%20%20%20.fil0%20%7Bfill%3A%232B2A29%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cpath%20d%3D%22M8850%204524c375%2C300%20621%2C761%20621%2C1277%200%2C904%20-733%2C1637%20-1637%2C1637%20-25%2C0%20-50%2C-1%20-76%2C-2%20-104%2C-6%20-246%2C5%20-294%2C189%20-192%2C614%20-765%2C1059%20-1442%2C1059%20-394%2C0%20-752%2C-150%20-1021%2C-397%20-122%2C-131%20-221%2C-61%20-297%2C-17%20-296%2C225%20-666%2C360%20-1067%2C360%20-974%2C0%20-1763%2C-790%20-1763%2C-1763%200%2C-30%201%2C-60%202%2C-89%20-3%2C-126%20-45%2C-189%20-139%2C-209%20-629%2C-70%20-1119%2C-603%20-1119%2C-1251%200%2C-396%20183%2C-749%20469%2C-980%2083%2C-73%2093%2C-111%2074%2C-242%20-21%2C-117%20-32%2C-238%20-32%2C-361%200%2C-1112%20902%2C-2014%202015%2C-2014%20232%2C0%20455%2C39%20663%2C112%20143%2C43%20237%2C-9%20304%2C-60%20317%2C-260%20723%2C-417%201165%2C-417%20427%2C0%20823%2C149%201136%2C394%20122%2C104%2067%2C219%202%2C295%20-75%2C71%20-150%2C144%20-239%2C222%20-78%2C74%20-151%2C73%20-231%2C41%20-196%2C-85%20-446%2C-206%20-796%2C-180%20-358%2C27%20-615%2C211%20-853%2C397%20-139%2C132%20-327%2C144%20-469%2C76%20-375%2C-127%20-875%2C-273%20-1292%2C46%20-584%2C447%20-720%2C951%20-599%2C1654%2028%2C152%20-36%2C244%20-137%2C316%20-186%2C137%20-453%2C356%20-396%2C779%2064%2C465%20512%2C538%20851%2C551%20204%2C-3%20314%2C125%20325%2C300%2034%2C454%2023%2C1082%20419%2C1370%20639%2C466%201271%2C95%201616%2C-138%20151%2C-117%20365%2C-124%20519%2C5%20286%2C233%20580%2C571%201060%2C401%20523%2C-185%20601%2C-602%20743%2C-936%2056%2C-163%20236%2C-276%20398%2C-269%20409%2C0%201052%2C23%201282%2C-520%20118%2C-279%20165%2C-618%20-287%2C-1066%20-50%2C-50%20-109%2C-207%20-36%2C-301%2059%2C-60%20103%2C-106%20244%2C-256%20104%2C-102%20235%2C-89%20314%2C-13z%22%2F%3E%20%20%3Cpath%20d%3D%22M4716%205534l94%20-1054c4%2C-63%2061%2C-78%2095%2C-46l983%20914c41%2C38%2011%2C91%20-37%2C99l-1045%20171c-56%2C14%20-101%2C-24%20-90%2C-84z%22%2F%3E%20%20%3Crect%20transform%3D%22matrix%280.371413%200.345335%20-0.345335%200.371413%207134.83%201952.41%29%22%20width%3D%222871%22%20height%3D%225800%22%20rx%3D%22200%22%20ry%3D%22200%22%2F%3E%20%20%3Crect%20transform%3D%22matrix%280.371413%200.345335%20-0.345335%200.371413%207987.97%201034.86%29%22%20width%3D%222871%22%20height%3D%221533%22%20rx%3D%22200%22%20ry%3D%22200%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$l() { _extends$l = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$l.apply(this, arguments); }

var _ref$l = /*#__PURE__*/React.createElement("defs", null);

var _ref2$e = /*#__PURE__*/React.createElement("g", {
  id: "\u0421\u043B\u043E\u0439_x0020_1"
}, /*#__PURE__*/React.createElement("path", {
  d: "M8850 4524c375 300 621 761 621 1277 0 904-733 1637-1637 1637-25 0-50-1-76-2-104-6-246 5-294 189-192 614-765 1059-1442 1059-394 0-752-150-1021-397-122-131-221-61-297-17-296 225-666 360-1067 360-974 0-1763-790-1763-1763 0-30 1-60 2-89-2-126-45-189-139-209-629-70-1119-603-1119-1251 0-396 183-749 469-980 83-73 93-111 74-242-21-117-32-238-32-361 0-1112 902-2014 2015-2014 232 0 455 39 663 112 143 43 237-9 304-60 317-260 723-417 1165-417 427 0 823 149 1136 394 101 86 85 213 38 286L4528 4103c-74 79-82 111-91 201l-137 1533c-32 156 100 220 193 205l1686-275c68-3 146-52 190-100l1254-1348c99-108 309-170 526-129 245 53 464 156 661 304 14 8 28 18 40 30z"
}), /*#__PURE__*/React.createElement("path", {
  d: "M4716 5534l94-1054c4-63 61-78 95-46l983 914c41 38 11 91-37 99l-1045 171c-56 14-101-24-90-84z"
}), /*#__PURE__*/React.createElement("rect", {
  transform: "matrix(.37141 .34534 -.34534 .37141 7134.83 1952.41)",
  width: 2871,
  height: 5800,
  rx: 200,
  ry: 200
}), /*#__PURE__*/React.createElement("rect", {
  transform: "matrix(.37141 .34534 -.34534 .37141 7987.97 1034.85)",
  width: 2871,
  height: 1533,
  rx: 200,
  ry: 200
}));

var SvgComponent$l = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$l({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$l, _ref2$e);
};

var prescription_solid = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil1%20%7Bfill%3A%232B2A29%7D%20%20%20%20.fil0%20%7Bfill%3A%232B2A29%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cpath%20d%3D%22M8850%204524c375%2C300%20621%2C761%20621%2C1277%200%2C904%20-733%2C1637%20-1637%2C1637%20-25%2C0%20-50%2C-1%20-76%2C-2%20-104%2C-6%20-246%2C5%20-294%2C189%20-192%2C614%20-765%2C1059%20-1442%2C1059%20-394%2C0%20-752%2C-150%20-1021%2C-397%20-122%2C-131%20-221%2C-61%20-297%2C-17%20-296%2C225%20-666%2C360%20-1067%2C360%20-974%2C0%20-1763%2C-790%20-1763%2C-1763%200%2C-30%201%2C-60%202%2C-89%20-2%2C-126%20-45%2C-189%20-139%2C-209%20-629%2C-70%20-1119%2C-603%20-1119%2C-1251%200%2C-396%20183%2C-749%20469%2C-980%2083%2C-73%2093%2C-111%2074%2C-242%20-21%2C-117%20-32%2C-238%20-32%2C-361%200%2C-1112%20902%2C-2014%202015%2C-2014%20232%2C0%20455%2C39%20663%2C112%20143%2C43%20237%2C-9%20304%2C-60%20317%2C-260%20723%2C-417%201165%2C-417%20427%2C0%20823%2C149%201136%2C394%20101%2C86%2085%2C213%2038%2C286l0%200%20-1922%202067c-74%2C79%20-82%2C111%20-91%2C201l-137%201533c-32%2C156%20100%2C220%20193%2C205l1686%20-275c68%2C-3%20146%2C-52%20190%2C-100l1254%20-1348c99%2C-108%20309%2C-170%20526%2C-129%20245%2C53%20464%2C156%20661%2C304%2014%2C8%2028%2C18%2040%2C30z%22%2F%3E%20%20%3Cpath%20d%3D%22M4716%205534l94%20-1054c4%2C-63%2061%2C-78%2095%2C-46l983%20914c41%2C38%2011%2C91%20-37%2C99l-1045%20171c-56%2C14%20-101%2C-24%20-90%2C-84z%22%2F%3E%20%20%3Crect%20transform%3D%22matrix%280.371413%200.345335%20-0.345335%200.371413%207134.83%201952.41%29%22%20width%3D%222871%22%20height%3D%225800%22%20rx%3D%22200%22%20ry%3D%22200%22%2F%3E%20%20%3Crect%20transform%3D%22matrix%280.371413%200.345335%20-0.345335%200.371413%207987.97%201034.85%29%22%20width%3D%222871%22%20height%3D%221533%22%20rx%3D%22200%22%20ry%3D%22200%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$m() { _extends$m = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$m.apply(this, arguments); }

var _ref$m = /*#__PURE__*/React.createElement("defs", null);

var _ref2$f = /*#__PURE__*/React.createElement("path", {
  d: "M8414 4907c0-2086-1692-3777-3778-3777S859 2821 859 4907s1691 3777 3777 3777 3778-1691 3778-3777zM4636 1633c1808 0 3274 1466 3274 3274S6444 8181 4636 8181 1363 6715 1363 4907s1465-3274 3273-3274zm1691 2436c0-624-505-1130-1129-1130H3674c-55 0-101 46-101 101v1649h-515c-55 0-100 46-100 101v308c0 55 45 101 100 101h515v355h-515c-55 0-100 45-100 101v308c0 55 45 100 100 100h515v494c0 55 46 101 101 101h308c55 0 101-46 101-101v-494h1457c55 0 100-45 100-100v-308c0-56-45-101-100-101H4083v-355h1115c624 0 1129-506 1129-1130zm-2118 620c-68 0-126-57-126-126v-989c0-73 66-126 126-126h989v1c342 0 620 277 620 620 0 342-278 620-620 620h-989z",
  id: "\u0421\u043B\u043E\u0439_x0020_1"
});

var SvgComponent$m = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$m({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$m, _ref2$f);
};

var rub_light = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cpath%20d%3D%22M8414%204907c0%2C-2086%20-1692%2C-3777%20-3778%2C-3777%20-2086%2C0%20-3777%2C1691%20-3777%2C3777%200%2C2086%201691%2C3777%203777%2C3777%202086%2C0%203778%2C-1691%203778%2C-3777zm-3778%20-3274c1808%2C0%203274%2C1466%203274%2C3274%200%2C1808%20-1466%2C3274%20-3274%2C3274%20-1808%2C0%20-3273%2C-1466%20-3273%2C-3274%200%2C-1808%201465%2C-3274%203273%2C-3274zm1691%202436c0%2C-624%20-505%2C-1130%20-1129%2C-1130l-1216%200%20-176%200%20-132%200c-55%2C0%20-101%2C46%20-101%2C101l0%201649%20-515%200c-55%2C0%20-100%2C46%20-100%2C101l0%20308c0%2C55%2045%2C101%20100%2C101l515%200%200%20355%20-515%200c-55%2C0%20-100%2C45%20-100%2C101l0%20308c0%2C55%2045%2C100%20100%2C100l515%200%200%20494c0%2C55%2046%2C101%20101%2C101l308%200c55%2C0%20101%2C-46%20101%2C-101l0%20-494%201457%200c55%2C0%20100%2C-45%20100%2C-100l0%20-308c0%2C-56%20-45%2C-101%20-100%2C-101l-1457%200%200%20-355%201115%200%200%200c624%2C0%201129%2C-506%201129%2C-1130zm-2118%20620c-68%2C0%20-126%2C-57%20-126%2C-126l0%20-989c0%2C-73%2066%2C-126%20126%2C-126l989%200%200%201c342%2C0%20620%2C277%20620%2C620%200%2C342%20-278%2C620%20-620%2C620l0%200%20-989%200z%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$n() { _extends$n = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$n.apply(this, arguments); }

var _ref$n = /*#__PURE__*/React.createElement("defs", null);

var _ref2$g = /*#__PURE__*/React.createElement("path", {
  d: "M4636 1130c-2086 0-3777 1691-3777 3777s1691 3777 3777 3777 3778-1691 3778-3777-1692-3777-3778-3777zm0 755c1669 0 3022 1353 3022 3022S6305 7929 4636 7929 1615 6576 1615 4907s1352-3022 3021-3022zm1073 2220c0 272-212 494-479 511H4226V3593h972c282 0 511 230 511 512zM3082 5556h490v-285h-490c-70 0-126-57-126-126v-403c0-69 56-126 126-126h490V3065c0-70 56-126 126-126h1533c608 18 1096 533 1096 1166 0 618-466 1124-1055 1163-9 2-17 3-26 3H4226v285h1287c69 0 126 57 126 126v403c0 69-57 126-126 126H4226v320c0 69-56 126-125 126h-403c-70 0-126-57-126-126v-320h-490c-70 0-126-57-126-126v-403c0-69 56-126 126-126z",
  id: "\u0421\u043B\u043E\u0439_x0020_1"
});

var SvgComponent$n = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$n({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$n, _ref2$g);
};

var rub_regular = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cpath%20d%3D%22M4636%201130c-2086%2C0%20-3777%2C1691%20-3777%2C3777%200%2C2086%201691%2C3777%203777%2C3777%202086%2C0%203778%2C-1691%203778%2C-3777%200%2C-2086%20-1692%2C-3777%20-3778%2C-3777zm0%20755c1669%2C0%203022%2C1353%203022%2C3022%200%2C1669%20-1353%2C3022%20-3022%2C3022%20-1669%2C0%20-3021%2C-1353%20-3021%2C-3022%200%2C-1669%201352%2C-3022%203021%2C-3022zm1073%202220c0%2C272%20-212%2C494%20-479%2C511l-1004%200%200%20-1023%20972%200c282%2C0%20511%2C230%20511%2C512zm-2627%201451l490%200%200%20-75%200%20-210%20-490%200c-70%2C0%20-126%2C-57%20-126%2C-126l0%20-403c0%2C-69%2056%2C-126%20126%2C-126l490%200%200%20-1551c0%2C-70%2056%2C-126%20126%2C-126l168%200%20235%200%201097%200%2021%200c4%2C0%208%2C0%2012%2C0%20608%2C18%201096%2C533%201096%2C1166%200%2C618%20-466%2C1124%20-1055%2C1163%20-9%2C2%20-17%2C3%20-26%2C3l-1020%200%200%20210%200%2075%201287%200c69%2C0%20126%2C57%20126%2C126l0%20403c0%2C69%20-57%2C126%20-126%2C126l-1287%200%200%20320c0%2C69%20-56%2C126%20-125%2C126l-403%200c-70%2C0%20-126%2C-57%20-126%2C-126l0%20-320%20-490%200c-70%2C0%20-126%2C-57%20-126%2C-126l0%20-403c0%2C-69%2056%2C-126%20126%2C-126z%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$o() { _extends$o = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$o.apply(this, arguments); }

var _ref$o = /*#__PURE__*/React.createElement("defs", null);

var _ref2$h = /*#__PURE__*/React.createElement("path", {
  d: "M4636 1130c2087 0 3778 1691 3778 3777S6723 8684 4636 8684c-2086 0-3777-1691-3777-3777s1691-3777 3777-3777zm1073 2975c0 272-212 495-479 511H4226V3593h972c282 0 511 230 511 512zM3082 5556h490v-285h-490c-70 0-126-57-126-126v-403c0-69 56-126 126-126h490V3065c0-70 56-126 126-126h1533c608 18 1096 533 1096 1166 0 618-466 1124-1055 1163-9 2-17 3-26 3H4226v285h1287c69 0 126 57 126 126v403c0 69-57 126-126 126H4226v320c0 69-56 126-125 126h-403c-70 0-126-57-126-126v-320h-490c-70 0-126-57-126-126v-403c0-69 56-126 126-126z",
  id: "\u0421\u043B\u043E\u0439_x0020_1"
});

var SvgComponent$o = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$o({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$o, _ref2$h);
};

var rub_solid = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cpath%20d%3D%22M4636%201130c2087%2C0%203778%2C1691%203778%2C3777%200%2C2086%20-1691%2C3777%20-3778%2C3777%20-2086%2C0%20-3777%2C-1691%20-3777%2C-3777%200%2C-2086%201691%2C-3777%203777%2C-3777zm1073%202975c0%2C272%20-212%2C495%20-479%2C511l-1004%200%200%20-1023%20972%200c282%2C0%20511%2C230%20511%2C512zm-2627%201451l490%200%200%20-75%200%20-210%20-490%200c-70%2C0%20-126%2C-57%20-126%2C-126l0%20-403c0%2C-69%2056%2C-126%20126%2C-126l490%200%200%20-1551c0%2C-70%2056%2C-126%20126%2C-126l168%200%20235%200%201097%200%2021%200c4%2C0%208%2C0%2012%2C0%20608%2C18%201096%2C533%201096%2C1166%200%2C618%20-466%2C1124%20-1055%2C1163%20-9%2C2%20-17%2C3%20-26%2C3l-1020%200%200%20210%200%2075%201287%200c69%2C0%20126%2C57%20126%2C126l0%20403c0%2C69%20-57%2C126%20-126%2C126l-1287%200%200%20320c0%2C69%20-56%2C126%20-125%2C126l-403%200c-70%2C0%20-126%2C-57%20-126%2C-126l0%20-320%20-490%200c-70%2C0%20-126%2C-57%20-126%2C-126l0%20-403c0%2C-69%2056%2C-126%20126%2C-126z%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$p() { _extends$p = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$p.apply(this, arguments); }

var _ref$p = /*#__PURE__*/React.createElement("defs", null);

var _ref2$i = /*#__PURE__*/React.createElement("path", {
  d: "M5115 1139c-1071 0-1939 868-1939 1939s868 1939 1939 1939 1939-868 1939-1939-868-1939-1939-1939zM3679 3078c0-793 643-1435 1436-1435 792 0 1435 642 1435 1435s-643 1435-1435 1435c-793 0-1436-642-1436-1435zM2328 7213c-4-691 647-1355 1569-1297 444 28 498 206 1218 206 721 0 772-196 1228-206 894-20 1568 625 1568 1211v729c0 208-122 317-301 322H2634c-170 0-306-102-306-322v-643zm2787-1591c-536 0-903-226-1438-226-1018 0-1851 832-1851 1850v708c0 401 327 729 728 730h5132c400-1 728-329 728-730v-708c0-1018-833-1850-1851-1850-536 0-913 226-1448 226z",
  id: "\u0421\u043B\u043E\u0439_x0020_1"
});

var SvgComponent$p = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$p({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$p, _ref2$i);
};

var user_light = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cpath%20d%3D%22M5115%201139c-1071%2C0%20-1939%2C868%20-1939%2C1939%200%2C1071%20868%2C1939%201939%2C1939%201071%2C0%201939%2C-868%201939%2C-1939%200%2C-1071%20-868%2C-1939%20-1939%2C-1939zm-1436%201939c0%2C-793%20643%2C-1435%201436%2C-1435%20792%2C0%201435%2C642%201435%2C1435%200%2C793%20-643%2C1435%20-1435%2C1435%20-793%2C0%20-1436%2C-642%20-1436%2C-1435zm-1351%204135c-4%2C-691%20647%2C-1355%201569%2C-1297%20444%2C28%20498%2C206%201218%2C206%20721%2C0%20772%2C-196%201228%2C-206%20894%2C-20%201568%2C625%201568%2C1211l0%20729c0%2C208%20-122%2C317%20-301%2C322l-318%200%20-4658%200c-170%2C0%20-306%2C-102%20-306%2C-322l0%20-643zm2787%20-1591c-536%2C0%20-903%2C-226%20-1438%2C-226%20-1018%2C0%20-1851%2C832%20-1851%2C1850l0%20708c0%2C401%20327%2C729%20728%2C730l5129%200%203%200c400%2C-1%20728%2C-329%20728%2C-730l0%20-708c0%2C-1018%20-833%2C-1850%20-1851%2C-1850%20-536%2C0%20-913%2C226%20-1448%2C226z%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$q() { _extends$q = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$q.apply(this, arguments); }

var _ref$q = /*#__PURE__*/React.createElement("defs", null);

var _ref2$j = /*#__PURE__*/React.createElement("path", {
  d: "M3176 3078c0 1071 868 1939 1939 1939s1939-868 1939-1939-868-1939-1939-1939-1939 868-1939 1939zm1939 1209c-668 0-1209-542-1209-1209 0-668 541-1209 1209-1209 667 0 1208 541 1208 1209 0 667-541 1209-1208 1209zM2592 7189c0-627 586-1132 1420-1002 401 63 588 222 1103 222s826-199 1112-222c786-63 1421 376 1421 935v645c0 111-71 167-166 167l-395 1H2760c-77 0-168-62-168-168v-578zm2523-1553c-536 0-903-226-1438-226-1018 0-1851 832-1851 1850v708c0 401 327 729 728 730h5132c400-1 728-329 728-730v-708c0-1018-833-1850-1851-1850-536 0-913 226-1448 226z",
  id: "\u0421\u043B\u043E\u0439_x0020_1"
});

var SvgComponent$q = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$q({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$q, _ref2$j);
};

var user_regular = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.str0%20%7Bstroke%3A%232B2A29%3Bstroke-width%3A20%7D%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cpath%20d%3D%22M3176%203078c0%2C1071%20868%2C1939%201939%2C1939%201071%2C0%201939%2C-868%201939%2C-1939%200%2C-1071%20-868%2C-1939%20-1939%2C-1939%20-1071%2C0%20-1939%2C868%20-1939%2C1939zm1939%201209c-668%2C0%20-1209%2C-542%20-1209%2C-1209%200%2C-668%20541%2C-1209%201209%2C-1209%20667%2C0%201208%2C541%201208%2C1209%200%2C667%20-541%2C1209%20-1208%2C1209zm-2523%202902c0%2C-627%20586%2C-1132%201420%2C-1002%20401%2C63%20588%2C222%201103%2C222%20515%2C0%20826%2C-199%201112%2C-222%20786%2C-63%201421%2C376%201421%2C935l0%20645c0%2C111%20-71%2C167%20-166%2C167l-395%201%20-4327%200c-77%2C0%20-168%2C-62%20-168%2C-168l0%20-578zm2523%20-1553c-536%2C0%20-903%2C-226%20-1438%2C-226%20-1018%2C0%20-1851%2C832%20-1851%2C1850l0%20708c0%2C401%20327%2C729%20728%2C730l5129%200%203%200c400%2C-1%20728%2C-329%20728%2C-730l0%20-708c0%2C-1018%20-833%2C-1850%20-1851%2C-1850%20-536%2C0%20-913%2C226%20-1448%2C226z%22%2F%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$r() { _extends$r = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$r.apply(this, arguments); }

var _ref$r = /*#__PURE__*/React.createElement("defs", null);

var _ref2$k = /*#__PURE__*/React.createElement("g", {
  id: "\u0421\u043B\u043E\u0439_x0020_1"
}, /*#__PURE__*/React.createElement("g", {
  id: "_2538416372992"
}, /*#__PURE__*/React.createElement("path", {
  d: "M7054 3078c0-1071-868-1939-1939-1939s-1939 868-1939 1939 868 1939 1939 1939 1939-868 1939-1939z"
}), /*#__PURE__*/React.createElement("path", {
  d: "M5115 1139c-1071 0-1939 868-1939 1939s868 1939 1939 1939 1939-868 1939-1939-868-1939-1939-1939zM3679 3078c0-793 643-1435 1436-1435 792 0 1435 642 1435 1435s-643 1435-1435 1435c-793 0-1436-642-1436-1435zM2328 7213c-4-691 647-1355 1569-1297 444 28 498 206 1218 206 721 0 772-196 1228-206 894-20 1568 625 1568 1211v729c0 208-122 317-301 322H2634c-170 0-306-102-306-322v-643zm2787-1591c-536 0-903-226-1438-226-1018 0-1851 832-1851 1850v708c0 401 327 729 728 730h5132c400-1 728-329 728-730v-708c0-1018-833-1850-1851-1850-536 0-913 226-1448 226z"
}), /*#__PURE__*/React.createElement("path", {
  d: "M1826 7246c0-1018 833-1850 1851-1850 535 0 902 226 1438 226 535 0 912-226 1448-226 1018 0 1851 832 1851 1850v708c0 401-328 729-728 730H2554c-401-1-728-329-728-730v-708z"
})));

var SvgComponent$r = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$r({
    width: "100mm",
    height: "100mm",
    viewBox: "0 0 10000 10000",
    shapeRendering: "geometricPrecision",
    textRendering: "geometricPrecision",
    imageRendering: "optimizeQuality",
    fillRule: "evenodd",
    clipRule: "evenodd"
  }, props), _ref$r, _ref2$k);
};

var user_solid = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3C!DOCTYPE%20svg%20PUBLIC%20%22-%2F%2FW3C%2F%2FDTD%20SVG%201.1%2F%2FEN%22%20%22http%3A%2F%2Fwww.w3.org%2FGraphics%2FSVG%2F1.1%2FDTD%2Fsvg11.dtd%22%3E%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xml%3Aspace%3D%22preserve%22%20width%3D%22100mm%22%20height%3D%22100mm%22%20version%3D%221.1%22%20style%3D%22shape-rendering%3AgeometricPrecision%3B%20text-rendering%3AgeometricPrecision%3B%20image-rendering%3AoptimizeQuality%3B%20fill-rule%3Aevenodd%3B%20clip-rule%3Aevenodd%22viewBox%3D%220%200%2010000%2010000%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%3Cdefs%3E%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%20%20%20%3C!%5BCDATA%5B%20%20%20%20.str0%20%7Bstroke%3A%232B2A29%3Bstroke-width%3A20%7D%20%20%20%20.fil0%20%7Bfill%3A%231F1B20%7D%20%20%20%20.fil1%20%7Bfill%3A%231F1B20%7D%20%20%20%5D%5D%3E%20%20%3C%2Fstyle%3E%20%3C%2Fdefs%3E%20%3Cg%20id%3D%22%D0%A1%D0%BB%D0%BE%D0%B9_x0020_1%22%3E%20%20%3Cmetadata%20id%3D%22CorelCorpID_0Corel-Layer%22%2F%3E%20%20%3Cg%20id%3D%22_2538416372992%22%3E%20%20%20%3Cpath%20d%3D%22M7054%203078c0%2C-1071%20-868%2C-1939%20-1939%2C-1939%20-1071%2C0%20-1939%2C868%20-1939%2C1939%200%2C1071%20868%2C1939%201939%2C1939%201071%2C0%201939%2C-868%201939%2C-1939z%22%2F%3E%20%20%20%3Cpath%20d%3D%22M5115%201139c-1071%2C0%20-1939%2C868%20-1939%2C1939%200%2C1071%20868%2C1939%201939%2C1939%201071%2C0%201939%2C-868%201939%2C-1939%200%2C-1071%20-868%2C-1939%20-1939%2C-1939zm-1436%201939c0%2C-793%20643%2C-1435%201436%2C-1435%20792%2C0%201435%2C642%201435%2C1435%200%2C793%20-643%2C1435%20-1435%2C1435%20-793%2C0%20-1436%2C-642%20-1436%2C-1435z%22%2F%3E%20%20%20%3Cpath%20d%3D%22M2328%207213c-4%2C-691%20647%2C-1355%201569%2C-1297%20444%2C28%20498%2C206%201218%2C206%20721%2C0%20772%2C-196%201228%2C-206%20894%2C-20%201568%2C625%201568%2C1211l0%20729c0%2C208%20-122%2C317%20-301%2C322l-318%200%20-4658%200c-170%2C0%20-306%2C-102%20-306%2C-322l0%20-643zm2787%20-1591c-536%2C0%20-903%2C-226%20-1438%2C-226%20-1018%2C0%20-1851%2C832%20-1851%2C1850l0%20708c0%2C401%20327%2C729%20728%2C730l5129%200%203%200c400%2C-1%20728%2C-329%20728%2C-730l0%20-708c0%2C-1018%20-833%2C-1850%20-1851%2C-1850%20-536%2C0%20-913%2C226%20-1448%2C226z%22%2F%3E%20%20%20%3Cpath%20d%3D%22M1826%207246c0%2C-1018%20833%2C-1850%201851%2C-1850%20535%2C0%20902%2C226%201438%2C226%20535%2C0%20912%2C-226%201448%2C-226%201018%2C0%201851%2C832%201851%2C1850l0%20708c0%2C401%20-328%2C729%20-728%2C730l-3%200%20-5129%200c-401%2C-1%20-728%2C-329%20-728%2C-730l0%20-708z%22%2F%3E%20%20%3C%2Fg%3E%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$s() { _extends$s = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$s.apply(this, arguments); }

var _ref$s = /*#__PURE__*/React.createElement("path", {
  d: "M5 20a1 1 0 01-2 0v-6a1 1 0 012 0v6zm5 0a1 1 0 01-2 0v-9a1 1 0 012 0v9zm5 0a1 1 0 01-2 0v-8a1 1 0 012 0v8zm5 0a1 1 0 01-2 0V9a1 1 0 012 0v11zM4.625 10.78a1 1 0 11-1.25-1.56l5-4a1 1 0 01.996-.148l4.512 1.804 5.562-3.708a1 1 0 011.11 1.664l-6 4a1 1 0 01-.926.096l-4.46-1.783-4.544 3.636z"
});

var SvgComponent$s = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$s({
    width: 24,
    height: 24
  }, props), _ref$s);
};

var chart_bars = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%20%3Csvg%20width%3D%2224%22%20height%3D%2224%22%20viewBox%3D%220%200%2024%2024%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%3E%3Ctitle%3Echart%3C%2Ftitle%3E%3Cpath%20d%3D%22M5%2020a1%201%200%200%201-2%200v-6a1%201%200%200%201%202%200v6zm5%200a1%201%200%200%201-2%200v-9a1%201%200%200%201%202%200v9zm5%200a1%201%200%200%201-2%200v-8a1%201%200%200%201%202%200v8zm5%200a1%201%200%200%201-2%200V9a1%201%200%200%201%202%200v11zM4.625%2010.78a1%201%200%201%201-1.25-1.56l5-4a1%201%200%200%201%20.996-.148l4.512%201.804%205.562-3.708a1%201%200%200%201%201.11%201.664l-6%204a1%201%200%200%201-.926.096l-4.46-1.783-4.544%203.636z%22%20fill%3D%22%23000%22%20fill-rule%3D%22nonzero%22%2F%3E%3C%2Fsvg%3E";

var chart_line = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%3F%3E%3Csvg%20width%3D%2220px%22%20height%3D%2214px%22%20viewBox%3D%220%200%2020%2014%22%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%20%20%20%20%20%20%20%3Ctitle%3Eshow_chart%3C%2Ftitle%3E%20%20%20%20%3Cdesc%3ECreated%20with%20Sketch.%3C%2Fdesc%3E%20%20%20%20%3Cg%20id%3D%22Icons%22%20stroke%3D%22none%22%20stroke-width%3D%221%22%20fill%3D%22none%22%20fill-rule%3D%22evenodd%22%3E%20%20%20%20%20%20%20%20%3Cg%20id%3D%22Outlined%22%20transform%3D%22translate%28-510.000000%2C%20-2107.000000%29%22%3E%20%20%20%20%20%20%20%20%20%20%20%20%3Cg%20id%3D%22Editor%22%20transform%3D%22translate%28100.000000%2C%201960.000000%29%22%3E%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%3Cg%20id%3D%22Outlined-%2F-Editor-%2F-show_chart%22%20transform%3D%22translate%28408.000000%2C%20142.000000%29%22%3E%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%3Cg%3E%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%3Cpolygon%20id%3D%22Path%22%20points%3D%220%200%2024%200%2024%2024%200%2024%22%3E%3C%2Fpolygon%3E%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%3Cpolygon%20id%3D%22%F0%9F%94%B9-Icon-Color%22%20fill%3D%22%231D1D1D%22%20points%3D%223.5%2018.49%209.5%2012.48%2013.5%2016.48%2022%206.92%2020.59%205.51%2013.5%2013.48%209.5%209.48%202%2016.99%22%3E%3C%2Fpolygon%3E%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%3C%2Fg%3E%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%3C%2Fg%3E%20%20%20%20%20%20%20%20%20%20%20%20%3C%2Fg%3E%20%20%20%20%20%20%20%20%3C%2Fg%3E%20%20%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$t() { _extends$t = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$t.apply(this, arguments); }

var _ref$t = /*#__PURE__*/React.createElement("path", {
  d: "M247.634 334.025c-46.8-1.9-86.3-35.2-95.9-81.2-1.4-6.5-2.1-13.2-2.2-19.9l-.2-15.5-145.5-35.1-2.4 22.6c-2.8 26.3-1.5 52.8 3.9 78.7 25.9 123.5 140.3 209.2 266 199.3l22.6-1.8-30.7-146.5-15.6-.6zm-207.8-102l21.8 5.3-20.2 20.2c-1-8.5-1.6-17-1.6-25.5zm5.9 49.4l38.7-38.6 22.3 5.4-54.6 54.6c-2.5-7-4.6-14.1-6.4-21.4zm24.3 59.3c-3.5-5.8-6.7-11.8-9.6-18l54.2-54.2c1.9 7.1 4.3 13.9 7.2 20.5l-51.8 51.7zm11.3 16.9l50.2-50.2c3.5 5.6 7.4 11 11.7 16l-49.5 49.4c-4.3-4.8-8.5-9.9-12.4-15.2zm26.4 29.5l49.3-49.3c4.9 4.4 10.1 8.4 15.6 12.1l-49.9 49.9c-5.2-4-10.2-8.3-15-12.7zm31.7 24.2l51.2-51.2c6.4 3.1 13.1 5.6 20 7.7l-53.4 53.4c-6.2-3-12.1-6.3-17.8-9.9zm37.2 18.6l54.3-54.3 4.8 22.9-38.2 38.2c-7-1.9-14-4.2-20.9-6.8zm44.4 11.6l19.7-19.7 4.6 21.9c-8.2-.3-16.3-1.1-24.3-2.2zM478.734 184.225c-24.2-115.4-130.4-192.5-244.9-183.5l22.9 109c54.6 2.2 102.9 41.1 114.6 97s-16.8 110.9-66 134.9l22.9 109.1c108.5-37.8 174.7-151.1 150.5-266.5zM198.134 122.025l-22.9-109c-65.8 22.9-116 73.7-139.9 135.8l108.3 26.1c11.9-22.6 30.8-41.3 54.5-52.9z"
});

var SvgComponent$t = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$t({
    viewBox: "0 0 483.715 483.715"
  }, props), _ref$t);
};

var doughnut_chart = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22iso-8859-1%22%3F%3E%3Csvg%20version%3D%221.1%22%20id%3D%22Capa_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%20%20viewBox%3D%220%200%20483.715%20483.715%22%20style%3D%22enable-background%3Anew%200%200%20483.715%20483.715%3B%22%20xml%3Aspace%3D%22preserve%22%3E%3Cg%3E%20%3Cg%3E%20%20%3Cpath%20d%3D%22M247.634%2C334.025c-46.8-1.9-86.3-35.2-95.9-81.2c-1.4-6.5-2.1-13.2-2.2-19.9l-0.2-15.5l-145.5-35.1l-2.4%2C22.6%20%20%20c-2.8%2C26.3-1.5%2C52.8%2C3.9%2C78.7c25.9%2C123.5%2C140.3%2C209.2%2C266%2C199.3l22.6-1.8l-30.7-146.5L247.634%2C334.025z%20M39.834%2C232.025l21.8%2C5.3%20%20%20l-20.2%2C20.2C40.434%2C249.025%2C39.834%2C240.525%2C39.834%2C232.025z%20M45.734%2C281.425l38.7-38.6l22.3%2C5.4l-54.6%2C54.6%20%20%20C49.634%2C295.825%2C47.534%2C288.725%2C45.734%2C281.425z%20M70.034%2C340.725c-3.5-5.8-6.7-11.8-9.6-18l54.2-54.2c1.9%2C7.1%2C4.3%2C13.9%2C7.2%2C20.5%20%20%20L70.034%2C340.725z%20M81.334%2C357.625l50.2-50.2c3.5%2C5.6%2C7.4%2C11%2C11.7%2C16l-49.5%2C49.4C89.434%2C368.025%2C85.234%2C362.925%2C81.334%2C357.625z%20%20%20%20M107.734%2C387.125l49.3-49.3c4.9%2C4.4%2C10.1%2C8.4%2C15.6%2C12.1l-49.9%2C49.9C117.534%2C395.825%2C112.534%2C391.525%2C107.734%2C387.125z%20%20%20%20M139.434%2C411.325l51.2-51.2c6.4%2C3.1%2C13.1%2C5.6%2C20%2C7.7l-53.4%2C53.4C151.034%2C418.225%2C145.134%2C414.925%2C139.434%2C411.325z%20%20%20%20M176.634%2C429.925l54.3-54.3l4.8%2C22.9l-38.2%2C38.2C190.534%2C434.825%2C183.534%2C432.525%2C176.634%2C429.925z%20M221.034%2C441.525l19.7-19.7%20%20%20l4.6%2C21.9C237.134%2C443.425%2C229.034%2C442.625%2C221.034%2C441.525z%22%2F%3E%20%20%3Cpath%20d%3D%22M478.734%2C184.225c-24.2-115.4-130.4-192.5-244.9-183.5l22.9%2C109c54.6%2C2.2%2C102.9%2C41.1%2C114.6%2C97s-16.8%2C110.9-66%2C134.9%20%20%20l22.9%2C109.1C436.734%2C412.925%2C502.934%2C299.625%2C478.734%2C184.225z%22%2F%3E%20%20%3Cpath%20d%3D%22M198.134%2C122.025l-22.9-109c-65.8%2C22.9-116%2C73.7-139.9%2C135.8l108.3%2C26.1C155.534%2C152.325%2C174.434%2C133.625%2C198.134%2C122.025%20%20%20z%22%2F%3E%20%3C%2Fg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$u() { _extends$u = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$u.apply(this, arguments); }

var _ref$u = /*#__PURE__*/React.createElement("defs", null);

var _ref2$l = /*#__PURE__*/React.createElement("path", {
  className: "a",
  d: "M13.5 9.5h1v2l2.5-2h2a4.5 4.5 0 000-9h-6A4.5 4.5 0 008.5 5M.5 23.5a8.719 8.719 0 01.7-3.322c.49-.981 2.539-1.661 5.111-2.613.695-.258.581-2.074.273-2.413a5.127 5.127 0 01-1.336-3.978A3.354 3.354 0 018.5 7.5a3.354 3.354 0 013.256 3.674 5.127 5.127 0 01-1.336 3.978c-.308.339-.422 2.155.273 2.413 2.572.952 4.621 1.632 5.111 2.613a8.719 8.719 0 01.7 3.322z"
});

var _ref3 = /*#__PURE__*/React.createElement("path", {
  d: "M19 4.25a.75.75 0 10.75.75.75.75 0 00-.75-.75zM16 4.25a.75.75 0 10.75.75.75.75 0 00-.75-.75zM13 4.25a.75.75 0 10.75.75.75.75 0 00-.75-.75z"
});

var SvgComponent$u = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$u({
    viewBox: "0 0 24 24"
  }, props), _ref$u, _ref2$l, _ref3);
};

var person_message = "data:image/svg+xml,%3Csvg%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%2024%2024%22%3E%3Cdefs%3E%3Cstyle%3E.a%7Bfill%3Anone%3Bstroke%3A%23000%3Bstroke-linecap%3Around%3Bstroke-linejoin%3Around%3B%7D%3C%2Fstyle%3E%3C%2Fdefs%3E%3Ctitle%3Emessages-people-person-bubble-oval%3C%2Ftitle%3E%3Cpath%20class%3D%22a%22%20d%3D%22M13.5%2C9.5h1v2l2.5-2h2a4.5%2C4.5%2C0%2C0%2C0%2C0-9H13A4.5%2C4.5%2C0%2C0%2C0%2C8.5%2C5%22%2F%3E%3Cpath%20class%3D%22a%22%20d%3D%22M.5%2C23.5a8.719%2C8.719%2C0%2C0%2C1%2C.7-3.322c.49-.981%2C2.539-1.661%2C5.111-2.613.695-.258.581-2.074.273-2.413a5.127%2C5.127%2C0%2C0%2C1-1.336-3.978A3.354%2C3.354%2C0%2C0%2C1%2C8.5%2C7.5a3.354%2C3.354%2C0%2C0%2C1%2C3.256%2C3.674%2C5.127%2C5.127%2C0%2C0%2C1-1.336%2C3.978c-.308.339-.422%2C2.155.273%2C2.413%2C2.572.952%2C4.621%2C1.632%2C5.111%2C2.613a8.719%2C8.719%2C0%2C0%2C1%2C.7%2C3.322Z%22%2F%3E%3Cpath%20d%3D%22M19%2C4.25a.75.75%2C0%2C1%2C0%2C.75.75A.75.75%2C0%2C0%2C0%2C19%2C4.25Z%22%2F%3E%3Cpath%20d%3D%22M16%2C4.25a.75.75%2C0%2C1%2C0%2C.75.75A.75.75%2C0%2C0%2C0%2C16%2C4.25Z%22%2F%3E%3Cpath%20d%3D%22M13%2C4.25a.75.75%2C0%2C1%2C0%2C.75.75A.75.75%2C0%2C0%2C0%2C13%2C4.25Z%22%2F%3E%3C%2Fsvg%3E";

function _extends$v() { _extends$v = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$v.apply(this, arguments); }

var _ref$v = /*#__PURE__*/React.createElement("path", {
  d: "M11.55 5.007v7c0 .552-.47 1-1.05 1-.58 0-1.05-.448-1.05-1v-7c0-.552.47-1 1.05-1 .58 0 1.05.448 1.05 1zm0 10c0 .552-.47 1-1.05 1-.58 0-1.05-.448-1.05-1s.47-1 1.05-1c.58 0 1.05.448 1.05 1zM18.9 17c0 .552-.47 1-1.05 1H3.15c-.58 0-1.05-.448-1.05-1V3c0-.552.47-1 1.05-1h14.7c.58 0 1.05.448 1.05 1v14zm0-17H2.1C.94 0 0 .899 0 2.003v16.004C0 19.112.94 20 2.1 20h16.8c1.16 0 2.1-.892 2.1-1.997V2.007C21 .902 19.95 0 18.9 0z",
  fillRule: "evenodd"
});

var SvgComponent$v = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$v({
    width: 21,
    height: 20
  }, props), _ref$v);
};

var piechart = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20width%3D%2221px%22%20height%3D%2220px%22%20viewBox%3D%220%200%2021%2020%22%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%20%20%20%20%20%20%20%3Ctitle%3Eimportant_message%20%5B%231448%5D%3C%2Ftitle%3E%20%20%20%20%3Cdesc%3ECreated%20with%20Sketch.%3C%2Fdesc%3E%20%20%20%20%3Cdefs%3E%3C%2Fdefs%3E%20%20%20%20%3Cg%20id%3D%22Page-1%22%20stroke%3D%22none%22%20stroke-width%3D%221%22%20fill-rule%3D%22evenodd%22%3E%20%20%20%20%20%20%20%20%3Cg%20id%3D%22Dribbble-Light-Preview%22%20transform%3D%22translate%28-139.000000%2C%20-520.000000%29%22%20fill%3D%22%23000000%22%3E%20%20%20%20%20%20%20%20%20%20%20%20%3Cg%20id%3D%22icons%22%20transform%3D%22translate%2856.000000%2C%20160.000000%29%22%3E%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%3Cpath%20d%3D%22M94.55%2C365.007%20L94.55%2C372.007%20C94.55%2C372.559%2094.0796%2C373.007%2093.5%2C373.007%20C92.9204%2C373.007%2092.45%2C372.559%2092.45%2C372.007%20L92.45%2C365.007%20C92.45%2C364.455%2092.9204%2C364.007%2093.5%2C364.007%20C94.0796%2C364.007%2094.55%2C364.455%2094.55%2C365.007%20L94.55%2C365.007%20Z%20M94.55%2C375.007%20C94.55%2C375.559%2094.0796%2C376.007%2093.5%2C376.007%20C92.9204%2C376.007%2092.45%2C375.559%2092.45%2C375.007%20C92.45%2C374.455%2092.9204%2C374.007%2093.5%2C374.007%20C94.0796%2C374.007%2094.55%2C374.455%2094.55%2C375.007%20L94.55%2C375.007%20Z%20M101.9%2C377%20C101.9%2C377.552%20101.4296%2C378%20100.85%2C378%20L86.15%2C378%20C85.5704%2C378%2085.1%2C377.552%2085.1%2C377%20L85.1%2C363%20C85.1%2C362.448%2085.5704%2C362%2086.15%2C362%20L100.85%2C362%20C101.4296%2C362%20101.9%2C362.448%20101.9%2C363%20L101.9%2C377%20Z%20M101.9%2C360%20L85.1%2C360%20C83.93975%2C360%2083%2C360.899%2083%2C362.003%20L83%2C362.007%20L83%2C378.007%20C83%2C379.112%2083.93975%2C380%2085.1%2C380%20L101.9%2C380%20C103.06025%2C380%20104%2C379.108%20104%2C378.003%20L104%2C362.007%20C104%2C360.902%20102.95%2C360%20101.9%2C360%20L101.9%2C360%20Z%22%20id%3D%22important_message-%5B%231448%5D%22%3E%3C%2Fpath%3E%20%20%20%20%20%20%20%20%20%20%20%20%3C%2Fg%3E%20%20%20%20%20%20%20%20%3C%2Fg%3E%20%20%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$w() { _extends$w = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$w.apply(this, arguments); }

var _ref$w = /*#__PURE__*/React.createElement("path", {
  d: "M415 65.86C372.528 23.39 316.061 0 255.998 0c-25.226 0-50.018 4.161-73.689 12.369-3.913 1.356-5.986 5.629-4.629 9.543s5.627 5.987 9.543 4.629C209.31 18.883 232.449 15 255.998 15c51.194 0 98.157 18.435 134.622 49.003l-23.87 23.868c-7.274 7.275-11.28 16.947-11.28 27.234 0 10.288 4.006 19.96 11.28 27.233 7.274 7.275 16.946 11.281 27.234 11.281 10.287 0 19.959-4.006 27.234-11.28l18.613-18.613a208.335 208.335 0 0121.711 58.679c-18.661 25.541-43.972 47.462-73.734 63.88-9.779-63.957-65.162-113.106-131.811-113.106-66.645 0-122.027 49.146-131.809 113.099-29.771-16.423-55.073-38.327-73.742-63.884a209.196 209.196 0 0121.636-58.751l18.696 18.696c7.51 7.509 17.37 11.263 27.234 11.263 9.861-.001 19.727-3.756 27.233-11.263 7.275-7.274 11.281-16.946 11.281-27.234 0-10.287-4.006-19.959-11.281-27.234l-23.964-23.964a211.405 211.405 0 0136.908-24.752 7.501 7.501 0 00-7.002-13.266C77.137 64.974 31.136 141.216 31.136 224.861c0 60.063 23.39 116.531 65.86 159.002a227.458 227.458 0 0031.265 26.113c-23.271 14.982-43.567 33.751-60.399 55.918-6.651 8.76-7.748 20.317-2.861 30.164 4.879 9.831 14.729 15.938 25.707 15.938h42.964c4.143 0 7.5-3.357 7.5-7.5s-3.357-7.5-7.5-7.5H90.707c-5.319 0-9.907-2.844-12.271-7.606-2.372-4.778-1.859-10.171 1.37-14.425 17.169-22.611 38.181-41.449 62.452-56.036 34.123 20.076 73.104 30.795 113.739 30.795 36.257 0 72.287-8.836 104.193-25.553a221.922 221.922 0 009.472-5.287c24.304 14.593 45.34 33.448 62.527 56.082 3.229 4.253 3.742 9.646 1.37 14.424-2.363 4.763-6.95 7.606-12.271 7.606H166.331c-4.143 0-7.5 3.357-7.5 7.5s3.357 7.5 7.5 7.5H421.29c10.978 0 20.828-6.107 25.707-15.938 4.887-9.847 3.79-21.404-2.86-30.163-16.848-22.188-37.164-40.969-60.461-55.957 21.158-14.648 39.859-32.922 54.912-53.827a7.5 7.5 0 10-12.172-8.766c-19.06 26.469-44.366 48.44-73.186 63.539-29.768 15.597-63.391 23.84-97.232 23.84-115.719 0-209.862-94.144-209.862-209.862 0-7.468.396-14.871 1.167-22.188 20.105 24.003 45.835 44.451 75.418 59.831-.04 1.336-.066 2.675-.066 4.02 0 73.525 59.817 133.343 133.343 133.343 73.526 0 133.344-59.817 133.344-133.343 0-1.342-.026-2.678-.066-4.011 29.567-15.372 55.312-35.824 75.414-59.813a211.308 211.308 0 011.17 22.163c0 33.333-7.591 65.21-22.562 94.746a7.499 7.499 0 003.299 10.08 7.498 7.498 0 0010.08-3.298c16.047-31.657 24.183-65.815 24.183-101.527 0-60.066-23.389-116.534-65.86-159.005zm-304.892 8.087l24.532 24.532a23.359 23.359 0 016.888 16.627c0 6.281-2.445 12.187-6.888 16.627-9.168 9.17-24.086 9.17-33.254 0l-21.357-21.356a209.318 209.318 0 0130.079-36.43zm145.89 310.919c-65.255 0-118.343-53.088-118.343-118.343S190.743 148.18 255.998 148.18s118.344 53.088 118.344 118.343-53.089 118.343-118.344 118.343zm154.613-253.134a23.359 23.359 0 01-16.627 6.888c-6.281 0-12.187-2.445-16.628-6.888a23.359 23.359 0 01-6.887-16.627c0-6.28 2.445-12.186 6.888-16.627l24.424-24.424a211.482 211.482 0 0130.082 36.426l-21.252 21.252z"
});

var _ref2$m = /*#__PURE__*/React.createElement("path", {
  d: "M281.528 88.751c0-14.077-11.453-25.529-25.53-25.529s-25.529 11.452-25.529 25.529 11.452 25.53 25.529 25.53 25.53-11.453 25.53-25.53zm-36.059 0c0-5.806 4.724-10.529 10.529-10.529 5.807 0 10.53 4.724 10.53 10.529 0 5.807-4.724 10.53-10.53 10.53-5.806 0-10.529-4.723-10.529-10.53zM255.998 159.486c-59.021 0-107.037 48.017-107.037 107.037S196.978 373.56 255.998 373.56c59.021 0 107.038-48.017 107.038-107.037S315.02 159.486 255.998 159.486zm0 199.075c-50.749 0-92.037-41.288-92.037-92.037s41.288-92.037 92.037-92.037c50.75 0 92.038 41.288 92.038 92.037s-41.288 92.037-92.038 92.037zM370.497 482.887h.064a7.5 7.5 0 00.062-15l-.116-.001a7.5 7.5 0 00-.126 15l.116.001zM348.264 482.887h.064a7.5 7.5 0 00.062-15l-.115-.001h-.064a7.5 7.5 0 00-.062 15l.115.001zM326.031 482.887h.064a7.5 7.5 0 00.062-15l-.116-.001h-.064a7.5 7.5 0 00-.062 15l.116.001z"
});

var SvgComponent$w = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$w({
    viewBox: "0 0 511.996 511.996"
  }, props), _ref$w, _ref2$m);
};

var webcam = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22iso-8859-1%22%3F%3E%3Csvg%20version%3D%221.1%22%20id%3D%22Capa_1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%20x%3D%220px%22%20y%3D%220px%22%20%20viewBox%3D%220%200%20511.996%20511.996%22%20style%3D%22enable-background%3Anew%200%200%20511.996%20511.996%3B%22%20xml%3Aspace%3D%22preserve%22%3E%3Cg%20id%3D%22XMLID_56_%22%3E%20%3Cpath%20id%3D%22XMLID_57_%22%20d%3D%22M415%2C65.86C372.528%2C23.39%2C316.061%2C0%2C255.998%2C0c-25.226%2C0-50.018%2C4.161-73.689%2C12.369%20%20c-3.913%2C1.356-5.986%2C5.629-4.629%2C9.543s5.627%2C5.987%2C9.543%2C4.629C209.31%2C18.883%2C232.449%2C15%2C255.998%2C15%20%20c51.194%2C0%2C98.157%2C18.435%2C134.622%2C49.003l-23.87%2C23.868c-7.274%2C7.275-11.28%2C16.947-11.28%2C27.234c0%2C10.288%2C4.006%2C19.96%2C11.28%2C27.233%20%20c7.274%2C7.275%2C16.946%2C11.281%2C27.234%2C11.281c10.287%2C0%2C19.959-4.006%2C27.234-11.28l18.613-18.613%20%20c10%2C18.104%2C17.411%2C37.829%2C21.711%2C58.679c-18.661%2C25.541-43.972%2C47.462-73.734%2C63.88c-9.779-63.957-65.162-113.106-131.811-113.106%20%20c-66.645%2C0-122.027%2C49.146-131.809%2C113.099c-29.771-16.423-55.073-38.327-73.742-63.884c4.257-20.713%2C11.59-40.494%2C21.636-58.751%20%20l18.696%2C18.696c7.51%2C7.509%2C17.37%2C11.263%2C27.234%2C11.263c9.861-0.001%2C19.727-3.756%2C27.233-11.263%20%20c7.275-7.274%2C11.281-16.946%2C11.281-27.234c0-10.287-4.006-19.959-11.281-27.234l-23.964-23.964%20%20c11.253-9.404%2C23.592-17.724%2C36.908-24.752c3.663-1.934%2C5.065-6.471%2C3.132-10.134c-1.934-3.662-6.469-5.063-10.134-3.132%20%20C77.137%2C64.974%2C31.136%2C141.216%2C31.136%2C224.861c0%2C60.063%2C23.39%2C116.531%2C65.86%2C159.002c9.727%2C9.727%2C20.195%2C18.439%2C31.265%2C26.113%20%20c-23.271%2C14.982-43.567%2C33.751-60.399%2C55.918c-6.651%2C8.76-7.748%2C20.317-2.861%2C30.164c4.879%2C9.831%2C14.729%2C15.938%2C25.707%2C15.938%20%20h42.964c4.143%2C0%2C7.5-3.357%2C7.5-7.5s-3.357-7.5-7.5-7.5H90.707c-5.319%2C0-9.907-2.844-12.271-7.606%20%20c-2.372-4.778-1.859-10.171%2C1.37-14.425c17.169-22.611%2C38.181-41.449%2C62.452-56.036c34.123%2C20.076%2C73.104%2C30.795%2C113.739%2C30.795%20%20c36.257%2C0%2C72.287-8.836%2C104.193-25.553c3.203-1.678%2C6.354-3.454%2C9.472-5.287c24.304%2C14.593%2C45.34%2C33.448%2C62.527%2C56.082%20%20c3.229%2C4.253%2C3.742%2C9.646%2C1.37%2C14.424c-2.363%2C4.763-6.95%2C7.606-12.271%2C7.606H166.331c-4.143%2C0-7.5%2C3.357-7.5%2C7.5s3.357%2C7.5%2C7.5%2C7.5%20%20H421.29c10.978%2C0%2C20.828-6.107%2C25.707-15.938c4.887-9.847%2C3.79-21.404-2.86-30.163c-16.848-22.188-37.164-40.969-60.461-55.957%20%20c21.158-14.648%2C39.859-32.922%2C54.912-53.827c2.421-3.361%2C1.658-8.049-1.703-10.469c-3.361-2.419-8.049-1.657-10.469%2C1.703%20%20c-19.06%2C26.469-44.366%2C48.44-73.186%2C63.539c-29.768%2C15.597-63.391%2C23.84-97.232%2C23.84c-115.719%2C0-209.862-94.144-209.862-209.862%20%20c0-7.468%2C0.396-14.871%2C1.167-22.188c20.105%2C24.003%2C45.835%2C44.451%2C75.418%2C59.831c-0.04%2C1.336-0.066%2C2.675-0.066%2C4.02%20%20c0%2C73.525%2C59.817%2C133.343%2C133.343%2C133.343c73.526%2C0%2C133.344-59.817%2C133.344-133.343c0-1.342-0.026-2.678-0.066-4.011%20%20c29.567-15.372%2C55.312-35.824%2C75.414-59.813c0.768%2C7.285%2C1.17%2C14.677%2C1.17%2C22.163c0%2C33.333-7.591%2C65.21-22.562%2C94.746%20%20c-1.873%2C3.694-0.396%2C8.208%2C3.299%2C10.08c1.087%2C0.551%2C2.244%2C0.813%2C3.385%2C0.813c2.736%2C0%2C5.374-1.503%2C6.695-4.111%20%20c16.047-31.657%2C24.183-65.815%2C24.183-101.527C480.86%2C164.799%2C457.471%2C108.331%2C415%2C65.86z%20M110.108%2C73.947l24.532%2C24.532%20%20c4.442%2C4.441%2C6.888%2C10.347%2C6.888%2C16.627c0%2C6.281-2.445%2C12.187-6.888%2C16.627c-9.168%2C9.17-24.086%2C9.17-33.254%2C0l-21.357-21.356%20%20C88.609%2C97.193%2C98.69%2C84.968%2C110.108%2C73.947z%20M255.998%2C384.866c-65.255%2C0-118.343-53.088-118.343-118.343%20%20s53.088-118.343%2C118.343-118.343s118.344%2C53.088%2C118.344%2C118.343S321.253%2C384.866%2C255.998%2C384.866z%20M410.611%2C131.732%20%20c-4.441%2C4.442-10.347%2C6.888-16.627%2C6.888c-6.281%2C0-12.187-2.445-16.628-6.888c-4.441-4.44-6.887-10.346-6.887-16.627%20%20c0-6.28%2C2.445-12.186%2C6.888-16.627l24.424-24.424c11.331%2C10.957%2C21.433%2C23.175%2C30.082%2C36.426L410.611%2C131.732z%22%2F%3E%20%3Cpath%20id%3D%22XMLID_62_%22%20d%3D%22M281.528%2C88.751c0-14.077-11.453-25.529-25.53-25.529s-25.529%2C11.452-25.529%2C25.529%20%20s11.452%2C25.53%2C25.529%2C25.53S281.528%2C102.828%2C281.528%2C88.751z%20M245.469%2C88.751c0-5.806%2C4.724-10.529%2C10.529-10.529%20%20c5.807%2C0%2C10.53%2C4.724%2C10.53%2C10.529c0%2C5.807-4.724%2C10.53-10.53%2C10.53C250.192%2C99.281%2C245.469%2C94.558%2C245.469%2C88.751z%22%2F%3E%20%3Cpath%20id%3D%22XMLID_65_%22%20d%3D%22M255.998%2C159.486c-59.021%2C0-107.037%2C48.017-107.037%2C107.037s48.017%2C107.037%2C107.037%2C107.037%20%20c59.021%2C0%2C107.038-48.017%2C107.038-107.037S315.02%2C159.486%2C255.998%2C159.486z%20M255.998%2C358.561%20%20c-50.749%2C0-92.037-41.288-92.037-92.037s41.288-92.037%2C92.037-92.037c50.75%2C0%2C92.038%2C41.288%2C92.038%2C92.037%20%20S306.748%2C358.561%2C255.998%2C358.561z%22%2F%3E%20%3Cpath%20id%3D%22XMLID_68_%22%20d%3D%22M370.497%2C482.887c0.021%2C0%2C0.043%2C0%2C0.064%2C0c4.113%2C0%2C7.464-3.316%2C7.499-7.437%20%20c0.034-4.143-3.295-7.528-7.437-7.563l-0.116-0.001c-4.147-0.03-7.528%2C3.295-7.563%2C7.437c-0.034%2C4.143%2C3.295%2C7.528%2C7.437%2C7.563%20%20L370.497%2C482.887z%22%2F%3E%20%3Cpath%20id%3D%22XMLID_69_%22%20d%3D%22M348.264%2C482.887c0.021%2C0%2C0.043%2C0%2C0.064%2C0c4.112%2C0%2C7.464-3.316%2C7.499-7.437%20%20c0.035-4.142-3.295-7.528-7.437-7.563l-0.115-0.001c-0.021%2C0-0.043%2C0-0.064%2C0c-4.112%2C0-7.464%2C3.316-7.499%2C7.437%20%20c-0.035%2C4.142%2C3.295%2C7.528%2C7.437%2C7.563L348.264%2C482.887z%22%2F%3E%20%3Cpath%20id%3D%22XMLID_70_%22%20d%3D%22M326.031%2C482.887c0.021%2C0%2C0.043%2C0%2C0.064%2C0c4.113%2C0%2C7.464-3.316%2C7.499-7.437%20%20c0.034-4.143-3.295-7.528-7.437-7.563l-0.116-0.001c-0.021%2C0-0.043%2C0-0.064%2C0c-4.113%2C0-7.464%2C3.316-7.499%2C7.437%20%20c-0.034%2C4.143%2C3.295%2C7.528%2C7.437%2C7.563L326.031%2C482.887z%22%2F%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3Cg%3E%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$x() { _extends$x = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$x.apply(this, arguments); }

var _ref$x = /*#__PURE__*/React.createElement("path", {
  d: "M11.55 5.007v7c0 .552-.47 1-1.05 1-.58 0-1.05-.448-1.05-1v-7c0-.552.47-1 1.05-1 .58 0 1.05.448 1.05 1zm0 10c0 .552-.47 1-1.05 1-.58 0-1.05-.448-1.05-1s.47-1 1.05-1c.58 0 1.05.448 1.05 1zM18.9 17c0 .552-.47 1-1.05 1H3.15c-.58 0-1.05-.448-1.05-1V3c0-.552.47-1 1.05-1h14.7c.58 0 1.05.448 1.05 1v14zm0-17H2.1C.94 0 0 .899 0 2.003v16.004C0 19.112.94 20 2.1 20h16.8c1.16 0 2.1-.892 2.1-1.997V2.007C21 .902 19.95 0 18.9 0z",
  fillRule: "evenodd"
});

var SvgComponent$x = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$x({
    width: 21,
    height: 20
  }, props), _ref$x);
};

var important = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20width%3D%2221px%22%20height%3D%2220px%22%20viewBox%3D%220%200%2021%2020%22%20version%3D%221.1%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20xmlns%3Axlink%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2Fxlink%22%3E%20%20%20%20%20%20%20%20%3Ctitle%3Eimportant_message%20%5B%231448%5D%3C%2Ftitle%3E%20%20%20%20%3Cdesc%3ECreated%20with%20Sketch.%3C%2Fdesc%3E%20%20%20%20%3Cdefs%3E%3C%2Fdefs%3E%20%20%20%20%3Cg%20id%3D%22Page-1%22%20stroke%3D%22none%22%20stroke-width%3D%221%22%20fill-rule%3D%22evenodd%22%3E%20%20%20%20%20%20%20%20%3Cg%20id%3D%22Dribbble-Light-Preview%22%20transform%3D%22translate%28-139.000000%2C%20-520.000000%29%22%20fill%3D%22%23000000%22%3E%20%20%20%20%20%20%20%20%20%20%20%20%3Cg%20id%3D%22icons%22%20transform%3D%22translate%2856.000000%2C%20160.000000%29%22%3E%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%20%3Cpath%20d%3D%22M94.55%2C365.007%20L94.55%2C372.007%20C94.55%2C372.559%2094.0796%2C373.007%2093.5%2C373.007%20C92.9204%2C373.007%2092.45%2C372.559%2092.45%2C372.007%20L92.45%2C365.007%20C92.45%2C364.455%2092.9204%2C364.007%2093.5%2C364.007%20C94.0796%2C364.007%2094.55%2C364.455%2094.55%2C365.007%20L94.55%2C365.007%20Z%20M94.55%2C375.007%20C94.55%2C375.559%2094.0796%2C376.007%2093.5%2C376.007%20C92.9204%2C376.007%2092.45%2C375.559%2092.45%2C375.007%20C92.45%2C374.455%2092.9204%2C374.007%2093.5%2C374.007%20C94.0796%2C374.007%2094.55%2C374.455%2094.55%2C375.007%20L94.55%2C375.007%20Z%20M101.9%2C377%20C101.9%2C377.552%20101.4296%2C378%20100.85%2C378%20L86.15%2C378%20C85.5704%2C378%2085.1%2C377.552%2085.1%2C377%20L85.1%2C363%20C85.1%2C362.448%2085.5704%2C362%2086.15%2C362%20L100.85%2C362%20C101.4296%2C362%20101.9%2C362.448%20101.9%2C363%20L101.9%2C377%20Z%20M101.9%2C360%20L85.1%2C360%20C83.93975%2C360%2083%2C360.899%2083%2C362.003%20L83%2C362.007%20L83%2C378.007%20C83%2C379.112%2083.93975%2C380%2085.1%2C380%20L101.9%2C380%20C103.06025%2C380%20104%2C379.108%20104%2C378.003%20L104%2C362.007%20C104%2C360.902%20102.95%2C360%20101.9%2C360%20L101.9%2C360%20Z%22%20id%3D%22important_message-%5B%231448%5D%22%3E%3C%2Fpath%3E%20%20%20%20%20%20%20%20%20%20%20%20%3C%2Fg%3E%20%20%20%20%20%20%20%20%3C%2Fg%3E%20%20%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$y() { _extends$y = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$y.apply(this, arguments); }

var _ref$y = /*#__PURE__*/React.createElement("path", {
  fill: "currentColor",
  d: "M204.3 5C104.9 24.4 24.8 104.3 5.2 203.4c-37 187 131.7 326.4 258.8 306.7 41.2-6.4 61.4-54.6 42.5-91.7-23.1-45.4 9.9-98.4 60.9-98.4h79.7c35.8 0 64.8-29.6 64.9-65.3C511.5 97.1 368.1-26.9 204.3 5zM96 320c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm32-128c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm128-64c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32zm128 64c-17.7 0-32-14.3-32-32s14.3-32 32-32 32 14.3 32 32-14.3 32-32 32z"
});

var SvgComponent$y = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$y({
    "aria-hidden": "true",
    "data-prefix": "fas",
    "data-icon": "palette",
    className: "svg-inline--fa fa-palette fa-w-16",
    viewBox: "0 0 512 512"
  }, props), _ref$y);
};

var palette_solid = "data:image/svg+xml,%3Csvg%20aria-hidden%3D%22true%22%20focusable%3D%22false%22%20data-prefix%3D%22fas%22%20data-icon%3D%22palette%22%20class%3D%22svg-inline--fa%20fa-palette%20fa-w-16%22%20role%3D%22img%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20512%20512%22%3E%3Cpath%20fill%3D%22currentColor%22%20d%3D%22M204.3%205C104.9%2024.4%2024.8%20104.3%205.2%20203.4c-37%20187%20131.7%20326.4%20258.8%20306.7%2041.2-6.4%2061.4-54.6%2042.5-91.7-23.1-45.4%209.9-98.4%2060.9-98.4h79.7c35.8%200%2064.8-29.6%2064.9-65.3C511.5%2097.1%20368.1-26.9%20204.3%205zM96%20320c-17.7%200-32-14.3-32-32s14.3-32%2032-32%2032%2014.3%2032%2032-14.3%2032-32%2032zm32-128c-17.7%200-32-14.3-32-32s14.3-32%2032-32%2032%2014.3%2032%2032-14.3%2032-32%2032zm128-64c-17.7%200-32-14.3-32-32s14.3-32%2032-32%2032%2014.3%2032%2032-14.3%2032-32%2032zm128%2064c-17.7%200-32-14.3-32-32s14.3-32%2032-32%2032%2014.3%2032%2032-14.3%2032-32%2032z%22%3E%3C%2Fpath%3E%3C%2Fsvg%3E";

function _extends$z() { _extends$z = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$z.apply(this, arguments); }

var _ref$z = /*#__PURE__*/React.createElement("path", {
  d: "M38.846 17.264h-1.53v-1.53h1.53zm-.002 4.316h-1.762a4.277 4.277 0 00-4.037-2.877 4.277 4.277 0 00-4.036 2.877h-.325v-8.723h7.955l.002.001h.002c.979 0 1.81.642 2.096 1.528h-2.772v4.225h2.877zm-3.697 3.557c-3.094 2.331-6.597-1.172-4.265-4.265a.324.324 0 01.06-.062c3.096-2.332 6.598 1.171 4.267 4.265a.331.331 0 01-.062.062m-7.812-3.557H13.344a4.278 4.278 0 00-4.037-2.879 4.278 4.278 0 00-4.036 2.88H1.349V1.348h25.987v15.147l-.001.003zm-15.926 3.557c-3.094 2.331-6.597-1.172-4.265-4.265a.324.324 0 01.06-.062c3.095-2.332 6.598 1.171 4.267 4.265a.33.33 0 01-.062.062M28.684 7.102h5.993l1.101 4.407h-7.094zm8.494 4.447l-1.403-5.922h-7.091V0H0v13.622h.001v9.305h5.036v.046a4.275 4.275 0 004.27 4.27 4.275 4.275 0 004.27-4.27v-.046h15.198l-.002.046a4.276 4.276 0 004.272 4.27 4.275 4.275 0 004.27-4.27v-.046h2.878V15.06a3.557 3.557 0 00-3.015-3.511"
});

var _ref2$n = /*#__PURE__*/React.createElement("path", {
  d: "M9.853 21.683a1.405 1.405 0 00-1.836 1.836c.131.338.406.613.745.744a1.404 1.404 0 001.834-1.835 1.311 1.311 0 00-.743-.745M33.591 21.683a1.405 1.405 0 00-1.835 1.836c.13.338.405.613.744.744a1.404 1.404 0 001.834-1.835 1.311 1.311 0 00-.743-.745"
});

var SvgComponent$z = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$z({
    viewBox: "0 0 40.193 27.243",
    height: 102.967,
    width: 151.911
  }, props), _ref$z, _ref2$n);
};

var truck = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2040.193192%2027.24346%22%20%20%20height%3D%2227.24346mm%22%20%20%20width%3D%2240.193192mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-84.903404%2C-134.87827%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%2844.19694%2C560.70949%29%22%20%20%20%20%20%20%20id%3D%22g11004%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3A%23000000%3Bfill-opacity%3A1%3Bfill-rule%3Anonzero%3Bstroke%3Anone%3Bstroke-width%3A0.35277778%22%20%20%20%20%20%20%20%20%20d%3D%22m%2079.551981%2C-408.56716%20h%20-1.529623%20v%20-1.52962%20h%201.529623%20z%20m%20-0.0014%2C4.31601%20h%20-1.762509%20c%20-0.578775%2C-1.67293%20-2.169033%2C-2.87734%20-4.036276%2C-2.87734%20-1.867238%2C0%20-3.457497%2C1.20441%20-4.036271%2C2.87734%20h%20-0.325219%20v%20-8.72298%20h%207.955414%20c%200.0014%2C0%200.0014%2C10e-4%200.0014%2C10e-4%20h%200.0028%20c%200.978408%2C0%201.809366%2C0.64217%202.095998%2C1.52825%20h%20-2.772615%20v%204.22506%20h%202.877344%20z%20m%20-3.697273%2C3.55672%20c%20-3.093699%2C2.33164%20-6.596669%2C-1.17133%20-4.265031%2C-4.26503%200.01655%2C-0.0234%200.03859%2C-0.0441%200.06064%2C-0.062%203.095071%2C-2.33164%206.598045%2C1.17133%204.266406%2C4.26503%20-0.01792%2C0.0234%20-0.03859%2C0.0441%20-0.06201%2C0.062%20m%20-7.812102%2C-3.55672%20H%2054.049976%20c%20-0.578778%2C-1.67293%20-2.169033%2C-2.87871%20-4.036275%2C-2.87871%20-1.867242%2C0%20-3.457497%2C1.20578%20-4.036272%2C2.87871%20h%20-3.921897%20v%20-20.23097%20h%2025.987043%20v%2015.14739%20c%200%2C0%20-0.0014%2C10e-4%20-0.0014%2C0.003%20z%20m%20-15.925986%2C3.55672%20c%20-3.093696%2C2.33164%20-6.596669%2C-1.17133%20-4.265027%2C-4.26503%200.01655%2C-0.0234%200.03858%2C-0.0441%200.06063%2C-0.062%203.095074%2C-2.33164%206.598048%2C1.17133%204.266406%2C4.26503%20-0.01792%2C0.0234%20-0.03858%2C0.0441%20-0.06201%2C0.062%20m%2017.275086%2C-18.03438%20h%205.993088%20l%201.101054%2C4.40696%20H%2069.39031%20Z%20m%208.494229%2C4.44693%20-1.402842%2C-5.92281%20h%20-7.091355%20v%20-5.62653%20H%2040.706464%20v%2013.62191%20c%200%2C0.001%200.0014%2C0.001%200.0014%2C0.001%20v%209.30451%20h%205.035349%20c%200%2C0.0152%200%2C0.0303%200%2C0.0455%200%2C2.35507%201.915474%2C4.27054%204.27054%2C4.27054%202.355067%2C0%204.270541%2C-1.91547%204.270541%2C-4.27054%200%2C-0.0152%200%2C-0.0303%20-0.0014%2C-0.0455%20H%2069.48128%20c%200%2C0.0152%20-0.0014%2C0.0303%20-0.0014%2C0.0455%200%2C2.35507%201.91685%2C4.27054%204.271916%2C4.27054%202.355071%2C0%204.270541%2C-1.91547%204.270541%2C-4.27054%200%2C-0.0152%20-0.0014%2C-0.0303%20-0.0014%2C-0.0455%20h%202.87872%20v%20-7.86722%20c%200%2C-1.77629%20-1.310513%2C-3.25217%20-3.015146%2C-3.51124%22%20%20%20%20%20%20%20%20%20id%3D%22path9417%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3A%23000000%3Bfill-opacity%3A1%3Bfill-rule%3Anonzero%3Bstroke%3Anone%3Bstroke-width%3A0.35277778%22%20%20%20%20%20%20%20%20%20d%3D%22m%2050.559438%2C-404.14779%20c%20-1.18098%2C-0.45475%20-2.290301%2C0.65595%20-1.835549%2C1.83555%200.130916%2C0.33762%200.405144%2C0.61322%200.744142%2C0.74414%201.179601%2C0.45475%202.290297%2C-0.65595%201.83417%2C-1.83555%20-0.129537%2C-0.33762%20-0.405145%2C-0.61323%20-0.742763%2C-0.74414%22%20%20%20%20%20%20%20%20%20id%3D%22path9419%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3A%23000000%3Bfill-opacity%3A1%3Bfill-rule%3Anonzero%3Bstroke%3Anone%3Bstroke-width%3A0.35277778%22%20%20%20%20%20%20%20%20%20d%3D%22m%2074.297522%2C-404.14779%20c%20-1.18098%2C-0.45475%20-2.2903%2C0.65595%20-1.835549%2C1.83555%200.130916%2C0.33762%200.405145%2C0.61322%200.744143%2C0.74414%201.179601%2C0.45475%202.2903%2C-0.65595%201.834169%2C-1.83555%20-0.129536%2C-0.33762%20-0.405144%2C-0.61323%20-0.742763%2C-0.74414%22%20%20%20%20%20%20%20%20%20id%3D%22path9421%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$A() { _extends$A = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$A.apply(this, arguments); }

var _ref$A = /*#__PURE__*/React.createElement("g", {
  fill: "none"
}, /*#__PURE__*/React.createElement("path", {
  d: "M15.875 1.058v30.34H8.467V1.057M33.867 8.82H15.875V5.291h17.992zm0 0M8.467 8.82H.706V5.291h7.76zm0 0"
}), /*#__PURE__*/React.createElement("path", {
  d: "M.353 5.292L8.467.706h7.408l17.992 4.586M8.467 7.056l7.055-5.998M15.522 13.053L8.467 7.056M8.467 19.05l7.055-5.997M15.522 25.047L8.467 19.05M18.344 38.806h-12.7v-7.409h12.7zm0 0M31.044 9.172v5.997M33.514 15.522l-2.421 2.421-2.422-2.42zm0 0M32.563 20.937a1.288 1.288 0 11-2.577 0c0-1.289 1.289-1.104 1.289-2.945"
}));

var SvgComponent$A = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$A({
    viewBox: "0 0 34.572 39.511",
    height: 149.333,
    width: 130.667
  }, props), _ref$A);
};

var crane = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2034.57222%2039.511113%22%20%20%20height%3D%2239.511112mm%22%20%20%20width%3D%2234.57222mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-13.759863%2C-80.012494%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%28-29.934187%2C430.83848%29%22%20%20%20%20%20%20%20id%3D%22g11018%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2059.56905%2C-349.76765%20v%2030.33889%20h%20-7.408333%20v%20-30.33889%22%20%20%20%20%20%20%20%20%20id%3D%22path8321%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2077.560716%2C-342.00654%20H%2059.56905%20v%20-3.52778%20h%2017.991666%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8323%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2052.160717%2C-342.00654%20h%20-7.761111%20v%20-3.52778%20h%207.761111%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8325%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2044.046828%2C-345.53432%208.113889%2C-4.58611%20h%207.408333%20l%2017.991666%2C4.58611%22%20%20%20%20%20%20%20%20%20id%3D%22path8327%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2052.160717%2C-343.77043%207.055555%2C-5.99722%22%20%20%20%20%20%20%20%20%20id%3D%22path8329%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2059.216272%2C-337.77321%20-7.055555%2C-5.99722%22%20%20%20%20%20%20%20%20%20id%3D%22path8331%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2052.160717%2C-331.77598%207.055555%2C-5.99722%22%20%20%20%20%20%20%20%20%20id%3D%22path8333%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2059.216272%2C-325.77876%20-7.055555%2C-5.99722%22%20%20%20%20%20%20%20%20%20id%3D%22path8335%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2062.038495%2C-312.02043%20h%20-12.7%20v%20-7.40833%20h%2012.7%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8337%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2074.738494%2C-341.65376%20v%205.99722%22%20%20%20%20%20%20%20%20%20id%3D%22path8339%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2077.207939%2C-335.30376%20-2.421213%2C2.42121%20-2.421213%2C-2.42121%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8341%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2076.257092%2C-329.88945%20c%200%2C0.71244%20-0.577398%2C1.28846%20-1.288465%2C1.28846%20-0.712446%2C0%20-1.288466%2C-0.57602%20-1.288466%2C-1.28846%200%2C-1.28847%201.288466%2C-1.10381%201.288466%2C-2.94487%22%20%20%20%20%20%20%20%20%20id%3D%22path8343%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$B() { _extends$B = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$B.apply(this, arguments); }

var _ref$B = /*#__PURE__*/React.createElement("g", {
  fill: "none"
}, /*#__PURE__*/React.createElement("path", {
  d: "M21.45 22.848l4.988-5.97-11.584-9.682-4.988 5.968zm0 0"
}), /*#__PURE__*/React.createElement("path", {
  d: "M15.754 22.23l2.038-2.441-4.268-3.568-2.04 2.439zm0 0"
}), /*#__PURE__*/React.createElement("path", {
  d: "M3.433 35.11l11.493-13.75-2.44-2.04L.995 33.072zm0 0M6.747 31.145l-2.44-2.038M32.91.353v13.591l2.26 1.438 2.368-4.82 3.487 4.82V0M32.91 43.039V26.456l2.12 3.042 2.458-2.028.878-2.959 2.151 4.312.46 14.04M29.517 24.95l-4.855 2.295 2.91 3.35zm0 0M28.449 5.252L24.7 8.142l1.446 1.875 3.783 1.032zm0 0M20.857 32.584l-2.285 1.511M33.969 19.355v1.459h1.089zm0 0"
}));

var SvgComponent$B = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$B({
    viewBox: "0 0 41.73 43.039",
    height: 162.667,
    width: 157.72
  }, props), _ref$B);
};

var demolition = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2041.730065%2043.038892%22%20%20%20height%3D%2243.038891mm%22%20%20%20width%3D%2241.730064mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-8.5422976%2C-159.74027%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%28-29.934187%2C430.83848%29%22%20%20%20%20%20%20%20id%3D%22g11030%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2059.92734%2C-248.25033%204.98712%2C-5.96966%20-11.583789%2C-9.6821%20-4.988498%2C5.96828%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9521%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2054.230531%2C-248.86907%202.038118%2C-2.44051%20-4.267784%2C-3.56774%20-2.040875%2C2.43913%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9523%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2041.909491%2C-235.98855%2011.492839%2C-13.75006%20-2.439128%2C-2.0395%20-11.492838%2C13.75144%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9525%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2045.223673%2C-239.95317%20-2.440505%2C-2.03811%22%20%20%20%20%20%20%20%20%20id%3D%22path9527%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2071.387105%2C-270.74543%20v%2013.59159%20l%202.258605%2C1.43729%202.368847%2C-4.819%203.486437%2C4.819%20v%20-15.38166%22%20%20%20%20%20%20%20%20%20id%3D%22path9529%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2071.387105%2C-228.05932%20v%20-16.58331%20l%202.119423%2C3.04271%202.45842%2C-2.02848%200.87781%2C-2.95864%202.151118%2C4.31188%200.458886%2C14.03945%22%20%20%20%20%20%20%20%20%20id%3D%22path9531%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2067.992998%2C-246.14744%20-4.854829%2C2.29443%202.910417%2C3.35001%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9533%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2066.925018%2C-265.8465%20-3.748264%2C2.88974%201.445563%2C1.87551%203.782714%2C1.03215%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9535%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2059.333406%2C-238.5145%20-2.284787%2C1.51171%22%20%20%20%20%20%20%20%20%20id%3D%22path9537%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2072.445439%2C-251.74366%20v%201.45934%20h%201.08865%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9539%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$C() { _extends$C = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$C.apply(this, arguments); }

var _ref$C = /*#__PURE__*/React.createElement("path", {
  d: "M33.867 34.22H.706V.705h33.16zm0 0",
  fill: "none"
});

var _ref2$o = /*#__PURE__*/React.createElement("path", {
  d: "M28.073 17.496c0 6.046-4.901 10.947-10.947 10.947-6.047 0-10.947-4.9-10.947-10.947 0-6.046 4.9-10.946 10.947-10.946 6.046 0 10.947 4.9 10.947 10.946zm0 0",
  fill: "none"
});

var _ref3$1 = /*#__PURE__*/React.createElement("path", {
  d: "M14.136 17.496a1.256 1.256 0 11-2.511 0 1.256 1.256 0 012.51 0M23.107 17.496a1.256 1.256 0 11-2.511 0 1.256 1.256 0 012.51 0"
});

var _ref4 = /*#__PURE__*/React.createElement("path", {
  d: "M17.286 7.056v2.116M17.286 25.4v2.117",
  fill: "none"
});

var SvgComponent$C = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$C({
    viewBox: "0 0 34.572 34.925",
    height: 132,
    width: 130.667
  }, props), _ref$C, _ref2$o, _ref3$1, _ref4);
};

var electric = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2034.572224%2034.924993%22%20%20%20height%3D%2234.924992mm%22%20%20%20width%3D%2234.572224mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-13.583474%2C-1037.8041%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%28-29.934187%2C430.83848%29%22%20%20%20%20%20%20%20id%3D%22g11153%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2077.384328%2C641.1851%20H%2044.223217%20v%20-33.51388%20h%2033.161111%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9457%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2071.591055%2C624.46123%20c%200%2C6.04684%20-4.901682%2C10.94714%20-10.947135%2C10.94714%20-6.046832%2C0%20-10.947135%2C-4.9003%20-10.947135%2C-10.94714%200%2C-6.04545%204.900303%2C-10.94575%2010.947135%2C-10.94575%206.045453%2C0%2010.947135%2C4.9003%2010.947135%2C10.94575%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9459%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3A%23000000%3Bfill-opacity%3A1%3Bfill-rule%3Anonzero%3Bstroke%3Anone%3Bstroke-width%3A0.35277778%22%20%20%20%20%20%20%20%20%20d%3D%22m%2057.653576%2C624.46125%20c%200%2C0.69314%20-0.562239%2C1.25536%20-1.255391%2C1.25536%20-0.693156%2C0%20-1.254016%2C-0.56222%20-1.254016%2C-1.25536%200%2C-0.69317%200.56086%2C-1.25539%201.254016%2C-1.25539%200.693152%2C0%201.255391%2C0.56222%201.255391%2C1.25539%22%20%20%20%20%20%20%20%20%20id%3D%22path9461%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3A%23000000%3Bfill-opacity%3A1%3Bfill-rule%3Anonzero%3Bstroke%3Anone%3Bstroke-width%3A0.35277778%22%20%20%20%20%20%20%20%20%20d%3D%22m%2066.624606%2C624.46125%20c%200%2C0.69314%20-0.56224%2C1.25536%20-1.255392%2C1.25536%20-0.693155%2C0%20-1.254015%2C-0.56222%20-1.254015%2C-1.25536%200%2C-0.69317%200.56086%2C-1.25539%201.254015%2C-1.25539%200.693152%2C0%201.255392%2C0.56222%201.255392%2C1.25539%22%20%20%20%20%20%20%20%20%20id%3D%22path9463%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2060.803772%2C614.02122%20v%202.11667%22%20%20%20%20%20%20%20%20%20id%3D%22path9465%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2060.803772%2C632.36566%20v%202.11667%22%20%20%20%20%20%20%20%20%20id%3D%22path9467%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$D() { _extends$D = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$D.apply(this, arguments); }

var _ref$D = /*#__PURE__*/React.createElement("g", {
  fill: "none"
}, /*#__PURE__*/React.createElement("path", {
  d: "M31.67 23.373L9.615 32.234l8.863-22.051zm0 0"
}), /*#__PURE__*/React.createElement("path", {
  d: "M20.642 21.208l6.648-6.646a1.67 1.67 0 000-2.362 1.67 1.67 0 010-2.363l.59-.591"
}), /*#__PURE__*/React.createElement("path", {
  d: "M35.929 1.344a2.192 2.192 0 010 3.102l-6.656 6.656-3.102-3.1 6.656-6.658a2.192 2.192 0 013.102 0zm0 0M15.057 18.984s-5.59-6.063-10.264-3.849c-6.093 2.886-3.992 8.02-2.432 9.622 0 0 3.511 4.17.78 6.095C.41 32.777-.37 40.794 8.212 40.794c8.583 0 11.704-5.772 11.704-5.772s3.054-4.745.551-7.024"
}));

var SvgComponent$D = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$D({
    viewBox: "0 0 37.272 41.5",
    height: 156.85,
    width: 140.872
  }, props), _ref$D);
};

var finishing = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2037.272316%2041.499963%22%20%20%20height%3D%2241.499962mm%22%20%20%20width%3D%2237.272316mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-12.192615%2C-484.71027%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%28-29.934187%2C430.83848%29%22%20%20%20%20%20%20%20id%3D%22g11084%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2073.795916%2C77.245037%20-22.052745%2C8.860785%208.862164%2C-22.051367%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8865%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2062.768855%2C75.080139%206.647656%2C-6.646278%20c%200.651812%2C-0.651812%200.651812%2C-1.710146%200%2C-2.361958%20-0.65319%2C-0.65319%20-0.65319%2C-1.711523%200%2C-2.363335%20l%200.5898%2C-0.591178%22%20%20%20%20%20%20%20%20%20id%3D%22path8867%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2078.055433%2C55.215718%20c%200.857139%2C0.85714%200.857139%2C2.244824%200%2C3.101964%20l%20-6.655925%2C6.655924%20-3.101964%2C-3.100585%206.655925%2C-6.657303%20c%200.857139%2C-0.857139%202.246202%2C-0.857139%203.101964%2C0%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8869%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2057.183666%2C72.855985%20c%200%2C0%20-5.5907%2C-6.063368%20-10.263628%2C-3.84886%20-6.093685%2C2.885612%20-3.992176%2C8.018804%20-2.432237%2C9.621462%200%2C0%203.511241%2C4.169944%200.779969%2C6.095063%20-2.731271%2C1.92512%20-3.511241%2C9.942545%205.071181%2C9.942545%208.582421%2C0%2011.703678%2C-5.772601%2011.703678%2C-5.772601%200%2C0%203.053733%2C-4.744586%200.551215%2C-7.023861%22%20%20%20%20%20%20%20%20%20id%3D%22path8871%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$E() { _extends$E = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$E.apply(this, arguments); }

var _ref$E = /*#__PURE__*/React.createElement("path", {
  d: "M33.62 12.33l9.772 6.628-19.148 9.595L.733 12.79v-3.3",
  fill: "none"
});

var _ref2$p = /*#__PURE__*/React.createElement("path", {
  d: "M34.247 10.23L20.457.707.705 9.615 16.156 20.29",
  fill: "none"
});

var _ref3$2 = /*#__PURE__*/React.createElement("path", {
  d: "M15.55 22.578s4.586.353 7.408-1.411c3.124-1.953 2.452-5.688 5.644-7.056 2.47-1.058 5.645-1.058 5.645-1.058V10.23s-4.586 0-7.056 1.058c-3.193 1.368-2.52 5.103-5.644 7.055-2.822 1.764-5.997 1.412-5.997 1.412zm0 0",
  fill: "none"
});

var _ref4$1 = /*#__PURE__*/React.createElement("path", {
  d: "M25.075 24.165a.882.882 0 11-1.764 0 .882.882 0 011.764 0M28.955 18.874a.882.882 0 11-1.763 0 .882.882 0 011.763 0M34.6 20.285a.882.882 0 11-1.764 0 .882.882 0 011.764 0M33.541 16.051a.882.882 0 11-1.763 0 .882.882 0 011.763 0"
});

var SvgComponent$E = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$E({
    viewBox: "0 0 44.097 29.258",
    height: 110.583,
    width: 166.666
  }, props), _ref$E, _ref2$p, _ref3$2, _ref4$1);
};

var floorCement = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2044.09708%2029.25849%22%20%20%20height%3D%2229.25849mm%22%20%20%20width%3D%2244.09708mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-8.7824617%2C-653.11928%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%28-29.934187%2C430.83848%29%22%20%20%20%20%20%20%20id%3D%22g11099%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2072.336574%2C234.6101%209.771669%2C6.62837%20-19.147841%2C9.59528%20-23.510709%2C-15.76338%20v%20-3.29903%22%20%20%20%20%20%20%20%20%20id%3D%22path9001%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2072.963581%2C232.51135%20-13.790028%2C-9.525%20-19.751421%2C8.90902%2015.450564%2C10.67566%22%20%20%20%20%20%20%20%20%20id%3D%22path9003%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2054.266359%2C244.85858%20c%200%2C0%204.586111%2C0.35278%207.408333%2C-1.41111%203.124013%2C-1.95268%202.45153%2C-5.68717%205.644444%2C-7.05556%202.469445%2C-1.05833%205.644445%2C-1.05833%205.644445%2C-1.05833%20v%20-2.82222%20c%200%2C0%20-4.586111%2C0%20-7.055556%2C1.05833%20-3.192914%2C1.36839%20-2.520431%2C5.10287%20-5.644444%2C7.05556%20-2.822222%2C1.76388%20-5.997222%2C1.41111%20-5.997222%2C1.41111%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9005%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3A%23000000%3Bfill-opacity%3A1%3Bfill-rule%3Anonzero%3Bstroke%3Anone%3Bstroke-width%3A0.35277778%22%20%20%20%20%20%20%20%20%20d%3D%22m%2063.791359%2C246.44608%20c%200%2C0.48644%20-0.39412%2C0.88194%20-0.881944%2C0.88194%20-0.486449%2C0%20-0.881945%2C-0.3955%20-0.881945%2C-0.88194%200%2C-0.48782%200.395496%2C-0.88195%200.881945%2C-0.88195%200.487824%2C0%200.881944%2C0.39413%200.881944%2C0.88195%22%20%20%20%20%20%20%20%20%20id%3D%22path9007%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3A%23000000%3Bfill-opacity%3A1%3Bfill-rule%3Anonzero%3Bstroke%3Anone%3Bstroke-width%3A0.35277778%22%20%20%20%20%20%20%20%20%20d%3D%22m%2067.671915%2C241.15441%20c%200%2C0.48645%20-0.39412%2C0.88195%20-0.881945%2C0.88195%20-0.486449%2C0%20-0.881944%2C-0.3955%20-0.881944%2C-0.88195%200%2C-0.48782%200.395495%2C-0.88194%200.881944%2C-0.88194%200.487825%2C0%200.881945%2C0.39412%200.881945%2C0.88194%22%20%20%20%20%20%20%20%20%20id%3D%22path9009%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3A%23000000%3Bfill-opacity%3A1%3Bfill-rule%3Anonzero%3Bstroke%3Anone%3Bstroke-width%3A0.35277778%22%20%20%20%20%20%20%20%20%20d%3D%22m%2073.316359%2C242.56552%20c%200%2C0.48645%20-0.39412%2C0.88195%20-0.881945%2C0.88195%20-0.486448%2C0%20-0.881944%2C-0.3955%20-0.881944%2C-0.88195%200%2C-0.48782%200.395496%2C-0.88194%200.881944%2C-0.88194%200.487825%2C0%200.881945%2C0.39412%200.881945%2C0.88194%22%20%20%20%20%20%20%20%20%20id%3D%22path9011%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3A%23000000%3Bfill-opacity%3A1%3Bfill-rule%3Anonzero%3Bstroke%3Anone%3Bstroke-width%3A0.35277778%22%20%20%20%20%20%20%20%20%20d%3D%22m%2072.258026%2C238.33219%20c%200%2C0.48645%20-0.39412%2C0.88194%20-0.881945%2C0.88194%20-0.486449%2C0%20-0.881944%2C-0.39549%20-0.881944%2C-0.88194%200%2C-0.48782%200.395495%2C-0.88194%200.881944%2C-0.88194%200.487825%2C0%200.881945%2C0.39412%200.881945%2C0.88194%22%20%20%20%20%20%20%20%20%20id%3D%22path9013%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$F() { _extends$F = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$F.apply(this, arguments); }

var _ref$F = /*#__PURE__*/React.createElement("g", {
  fill: "none"
}, /*#__PURE__*/React.createElement("path", {
  d: "M30.99 27.466v-2.218l4.125-2.116v2.116"
}), /*#__PURE__*/React.createElement("path", {
  d: "M38.643 18.898l4.043 2.882-19.148 9.595L.705 15.612l5.13-2.358"
}), /*#__PURE__*/React.createElement("path", {
  d: "M20.456.706l22.23 15.077s-1.14 1.69-4.749 3.468c-1.452.715-5.549 1.951-8.466 3.175-3.589 1.504-7.056 2.822-9.525 0-3.126-3.572-6.15-1.81-10.936-5.997C6.187 13.959.705 9.615.705 9.615zm0 0M13.243 24.269V21.72l-3.528-2.47v2.47M9.715 19.251l2.117-1.058M13.596 21.72l3.527-1.41M30.882 25.248l-2.822-2.116M35.115 23.132l-2.86-2.117"
}));

var SvgComponent$F = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$F({
    viewBox: "0 0 43.392 32.081",
    height: 121.25,
    width: 164
  }, props), _ref$F);
};

var layers = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2043.391575%2032.080712%22%20%20%20height%3D%2232.080711mm%22%20%20%20width%3D%2243.391575mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-8.8210385%2C-807.62768%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%28-29.934187%2C430.83848%29%22%20%20%20%20%20%20%20id%3D%22g11120%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2069.745862%2C404.25487%20v%20-2.21727%20l%204.124469%2C-2.11666%20v%202.11666%22%20%20%20%20%20%20%20%20%20id%3D%22path9093%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2077.398108%2C395.68761%204.043164%2C2.88148%20-19.147841%2C9.59528%20-22.832714%2C-15.76338%205.129058%2C-2.35782%22%20%20%20%20%20%20%20%20%20id%3D%22path9095%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2059.21076%2C377.49475%2022.230512%2C15.07711%20c%200%2C0%20-1.139638%2C1.69085%20-4.74872%2C3.46852%20-1.452452%2C0.7152%20-5.54936%2C1.9513%20-8.466667%2C3.175%20-3.588411%2C1.50344%20-7.055555%2C2.82223%20-9.524999%2C0%20-3.125391%2C-3.57187%20-6.150184%2C-1.80936%20-10.936111%2C-5.99722%20-2.822222%2C-2.46944%20-8.304058%2C-6.8144%20-8.304058%2C-6.8144%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9097%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2051.998109%2C401.05783%20v%20-2.548%20l%20-3.527778%2C-2.46944%20v%202.46944%22%20%20%20%20%20%20%20%20%20id%3D%22path9099%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2048.470331%2C396.04039%202.116667%2C-1.05833%22%20%20%20%20%20%20%20%20%20id%3D%22path9101%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2052.350887%2C398.50983%203.527778%2C-1.41111%22%20%20%20%20%20%20%20%20%20id%3D%22path9103%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2069.636997%2C402.03761%20-2.822222%2C-2.11667%22%20%20%20%20%20%20%20%20%20id%3D%22path9105%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2073.87033%2C399.92094%20-2.860807%2C-2.11667%22%20%20%20%20%20%20%20%20%20id%3D%22path9107%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$G() { _extends$G = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$G.apply(this, arguments); }

var _ref$G = /*#__PURE__*/React.createElement("g", {
  fill: "none"
}, /*#__PURE__*/React.createElement("path", {
  d: "M28.516 10.583H2.529A1.823 1.823 0 01.706 8.76V2.53C.706 1.52 1.52.706 2.529.706h25.987c1.007 0 1.823.815 1.823 1.823V8.76a1.823 1.823 0 01-1.823 1.823zm0 0"
}), /*#__PURE__*/React.createElement("path", {
  d: "M31.044 5.644h2.47v7.409l-17.992 2.47v5.644M17.992 34.572h-4.234V21.167h4.234zm0 0M3.175 10.583v9.173a2.117 2.117 0 004.233 0V16.05c0-.682.553-1.234 1.235-1.234h.706c.682 0 1.234.552 1.234 1.234v1.059a1.234 1.234 0 102.47 0v-6.527"
}));

var SvgComponent$G = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$G({
    viewBox: "0 0 34.219 35.278",
    height: 133.333,
    width: 129.333
  }, props), _ref$G);
};

var paintWall = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2034.219444%2035.277783%22%20%20%20height%3D%2235.277782mm%22%20%20%20width%3D%2234.219444mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-13.721278%2C-568.95835%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%28-29.934187%2C430.83848%29%22%20%20%20%20%20%20%20id%3D%22g11090%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2072.171209%2C148.7032%20H%2046.184166%20c%20-1.007346%2C0%20-1.823145%2C-0.8158%20-1.823145%2C-1.82314%20v%20-6.23149%20c%200%2C-1.00735%200.815799%2C-1.82314%201.823145%2C-1.82314%20h%2025.987043%20c%201.007346%2C0%201.823145%2C0.81579%201.823145%2C1.82314%20v%206.23149%20c%200%2C1.00734%20-0.815799%2C1.82314%20-1.823145%2C1.82314%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8817%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2074.699909%2C143.76431%20h%202.469444%20v%207.40834%20l%20-17.991666%2C2.46944%20v%205.64445%22%20%20%20%20%20%20%20%20%20id%3D%22path8819%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2061.64713%2C172.6921%20h%20-4.233333%20v%20-13.40556%20h%204.233333%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8821%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2046.830466%2C148.7032%20v%209.17223%20c%200%2C1.16857%200.94809%2C2.11666%202.116666%2C2.11666%201.168577%2C0%202.116667%2C-0.94809%202.116667%2C-2.11666%20v%20-3.70417%20c%200%2C-0.68213%200.552593%2C-1.23472%201.234722%2C-1.23472%20h%200.705556%20c%200.682129%2C0%201.234722%2C0.55259%201.234722%2C1.23472%20v%201.05833%20c%200%2C0.68213%200.552593%2C1.23472%201.234722%2C1.23472%200.682129%2C0%201.234722%2C-0.55259%201.234722%2C-1.23472%20v%20-6.52639%22%20%20%20%20%20%20%20%20%20id%3D%22path8823%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$H() { _extends$H = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$H.apply(this, arguments); }

var _ref$H = /*#__PURE__*/React.createElement("g", {
  fill: "none"
}, /*#__PURE__*/React.createElement("path", {
  d: "M9.833 5.2a2.247 2.247 0 110-4.494h6.682a2.248 2.248 0 110 4.495zm0 0M17 7.761H9.24v-2.47H17zm0 0"
}), /*#__PURE__*/React.createElement("path", {
  d: "M17 14.817H9.24V7.76H17zm0 0M15.237 23.989h-4.234v-9.172h4.234zm0 0M13.12 24.342v8.466M13.826 35.278h-1.412l-.705-2.822h2.822zm0 0M.792 28.364l6.61 3.59M.045 34.878l7.265.466M26.13 27.87l-6.609 3.59M26.878 34.383l-7.266.465"
}));

var SvgComponent$H = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$H({
    viewBox: "0 0 26.923 36.048",
    height: 136.245,
    width: 101.757
  }, props), _ref$H);
};

var pneumaticHammer = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2026.923302%2036.048036%22%20%20%20height%3D%2236.048035mm%22%20%20%20width%3D%2226.923302mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-17.402352%2C-244.02935%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%28-29.934187%2C430.83848%29%22%20%20%20%20%20%20%20id%3D%22g11042%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2057.169886%2C-181.60841%20c%20-1.241613%2C0%20-2.247581%2C-1.00597%20-2.247581%2C-2.24758%200%2C-1.24161%201.005968%2C-2.24758%202.247581%2C-2.24758%20h%206.682107%20c%201.241612%2C0%202.248958%2C1.00597%202.248958%2C2.24758%200%2C1.24161%20-1.007346%2C2.24758%20-2.248958%2C2.24758%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8433%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2064.33706%2C-179.04801%20h%20-7.76111%20v%20-2.46945%20h%207.76111%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8435%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2064.33706%2C-171.99246%20h%20-7.76111%20v%20-7.05555%20h%207.76111%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8437%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2062.573172%2C-162.82023%20h%20-4.233334%20v%20-9.17223%20h%204.233334%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8439%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2060.456507%2C-162.46746%20v%208.46667%22%20%20%20%20%20%20%20%20%20id%3D%22path8441%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2061.162063%2C-151.53135%20h%20-1.411111%20l%20-0.705556%2C-2.82222%20h%202.822222%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8443%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2048.128578%2C-158.44497%206.610449%2C3.58979%22%20%20%20%20%20%20%20%20%20id%3D%22path8445%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2047.381681%2C-151.93098%207.265017%2C0.46578%22%20%20%20%20%20%20%20%20%20id%3D%22path8447%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2073.466565%2C-158.93968%20-6.609071%2C3.58979%22%20%20%20%20%20%20%20%20%20id%3D%22path8449%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2074.21484%2C-152.42569%20-7.266396%2C0.4644%22%20%20%20%20%20%20%20%20%20id%3D%22path8451%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$I() { _extends$I = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$I.apply(this, arguments); }

var _ref$I = /*#__PURE__*/React.createElement("g", {
  fill: "none"
}, /*#__PURE__*/React.createElement("path", {
  d: "M36.336 16.933H.706v-4.586h35.63zm0 0M28.208 28.222H8.835a4.424 4.424 0 01-4.425-4.425v-6.864h28.222v6.864a4.424 4.424 0 01-4.424 4.425zm0 0"
}), /*#__PURE__*/React.createElement("path", {
  d: "M22.754 31.397h-8.467v-3.175h8.467zm0 0M20.99 39.864h-4.939v-8.467h4.94zm0 0M13.979 12.347h-2.822V9.525h2.822zm0 0M17.86.706H9.392v5.291h2.47v-2.47h5.996v8.82h2.823V.706zm0 0M27.384 12.347h-2.822V9.525h2.822zm0 0"
}));

var SvgComponent$I = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$I({
    viewBox: "0 0 37.042 40.569",
    height: 153.333,
    width: 140
  }, props), _ref$I);
};

var plumbing = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2037.041668%2040.569444%22%20%20%20height%3D%2240.569443mm%22%20%20%20width%3D%2237.041668mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-11.995974%2C-957.5472%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%28-29.934187%2C430.83848%29%22%20%20%20%20%20%20%20id%3D%22g11145%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2078.266272%2C543.64205%20H%2042.635717%20v%20-4.58611%20h%2035.630555%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9281%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2070.138603%2C554.93094%20H%2050.764764%20c%20-2.44464%2C0%20-4.424881%2C-1.98024%20-4.424881%2C-4.42488%20v%20-6.86401%20h%2028.222222%20v%206.86401%20c%200%2C2.44464%20-1.980241%2C4.42488%20-4.423502%2C4.42488%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9283%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2064.684328%2C558.10594%20h%20-8.466667%20v%20-3.175%20h%208.466667%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9285%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2062.920439%2C566.57261%20H%2057.98155%20v%20-8.46667%20h%204.938889%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9287%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2055.908981%2C539.05594%20h%20-2.822222%20v%20-2.82222%20h%202.822222%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9289%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2059.789536%2C527.41428%20H%2051.32287%20v%205.29167%20h%202.469444%20v%20-2.46945%20h%205.997222%20v%208.81945%20h%202.822222%20v%20-11.64167%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9291%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2069.314536%2C539.05594%20h%20-2.822222%20v%20-2.82222%20h%202.822222%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9293%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$J() { _extends$J = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$J.apply(this, arguments); }

var _ref$J = /*#__PURE__*/React.createElement("g", {
  fill: "none"
}, /*#__PURE__*/React.createElement("path", {
  d: "M20.457.706l22.229 15.077-19.15 9.595L.707 9.615zm0 0"
}), /*#__PURE__*/React.createElement("path", {
  d: "M35.956 11.157l-4.647 2.056-3.809-2.57"
}), /*#__PURE__*/React.createElement("path", {
  d: "M23.36 13.042l4.649-2.056-12.2-8.224M14.356 13.042l5.52-2.4M5.354 13.042l5.517-2.4M20.461 5.644l4.586-2.116M14.938 19.553l5.519-2.4M11.162 4.818L29.17 17.154l-5.81 2.399M5.934 7.215l22.364 15.422"
}));

var SvgComponent$J = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$J({
    viewBox: "0 0 43.392 26.083",
    height: 98.583,
    width: 163.999
  }, props), _ref$J);
};

var tiles = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2043.391525%2026.083481%22%20%20%20height%3D%2226.083481mm%22%20%20%20width%3D%2243.391525mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-9.3309208%2C-733.72074%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%28-29.934187%2C430.83848%29%22%20%20%20%20%20%20%20id%3D%22g11110%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2059.722013%2C303.58781%2022.229133%2C15.07711%20-19.149218%2C9.59528%20-22.831337%2C-15.76338%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8913%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2075.220808%2C314.03884%20-4.646745%2C2.05604%20-3.808898%2C-2.57005%22%20%20%20%20%20%20%20%20%20id%3D%22path8915%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2062.625539%2C315.924%204.648123%2C-2.05603%20-12.199772%2C-8.22413%22%20%20%20%20%20%20%20%20%20id%3D%22path8917%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2053.621437%2C315.924%205.519043%2C-2.39916%22%20%20%20%20%20%20%20%20%20id%3D%22path8919%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2044.618715%2C315.924%205.517664%2C-2.39916%22%20%20%20%20%20%20%20%20%20id%3D%22path8921%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2059.726147%2C308.5267%204.58611%2C-2.11667%22%20%20%20%20%20%20%20%20%20id%3D%22path8923%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2054.20297%2C322.43523%205.519042%2C-2.39917%22%20%20%20%20%20%20%20%20%20id%3D%22path8925%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2050.427145%2C307.69987%2018.008202%2C12.3362%20-5.809809%2C2.39916%22%20%20%20%20%20%20%20%20%20id%3D%22path8927%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2045.198868%2C310.09766%2022.364181%2C15.42163%22%20%20%20%20%20%20%20%20%20id%3D%22path8929%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$K() { _extends$K = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$K.apply(this, arguments); }

var _ref$K = /*#__PURE__*/React.createElement("g", {
  fill: "none"
}, /*#__PURE__*/React.createElement("path", {
  d: "M27.164 34.925H3.88V5.292h23.283zm0 0M0 5.292h31.044M21.872 5.292h-12.7V.706h12.7zm0 0M9.29 10.936v19.05M21.99 10.936v19.05M15.64 10.936v19.05"
}));

var SvgComponent$K = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$K({
    viewBox: "0 0 31.044 35.631",
    height: 134.667,
    width: 117.333
  }, props), _ref$K);
};

var trashBin = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2031.044443%2035.630559%22%20%20%20height%3D%2235.630558mm%22%20%20%20width%3D%2231.044443mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-15.341851%2C-325.37768%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%28-29.934187%2C430.83848%29%22%20%20%20%20%20%20%20id%3D%22g11050%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2072.439925%2C-70.535793%20H%2049.156592%20v%20-29.633337%20h%2023.283333%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8529%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2045.276038%2C-100.16911%20H%2076.320482%22%20%20%20%20%20%20%20%20%20id%3D%22path8531%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2067.148258%2C-100.16913%20H%2054.448259%20v%20-4.58611%20h%2012.699999%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8533%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2054.565393%2C-94.524664%20v%2019.049999%22%20%20%20%20%20%20%20%20%20id%3D%22path8535%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2067.265393%2C-94.524664%20v%2019.049999%22%20%20%20%20%20%20%20%20%20id%3D%22path8537%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2060.915393%2C-94.524664%20v%2019.049999%22%20%20%20%20%20%20%20%20%20id%3D%22path8539%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$L() { _extends$L = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$L.apply(this, arguments); }

var _ref$L = /*#__PURE__*/React.createElement("g", {
  fill: "none"
}, /*#__PURE__*/React.createElement("path", {
  d: "M32.772 15.17c0 7.988-6.475 14.463-14.463 14.463-7.989 0-14.464-6.475-14.464-14.464C3.845 7.181 10.32.706 18.309.706c7.988 0 14.463 6.475 14.463 14.463zm0 0"
}), /*#__PURE__*/React.createElement("path", {
  d: "M20.69 15.17a2.734 2.734 0 11-5.469-.001 2.734 2.734 0 015.469 0zm0 0"
}), /*#__PURE__*/React.createElement("path", {
  d: "M15.674 14.412c-1.593-1.43-5.529-1.653-9.405 3.286l9.763-.683M20.886 13.95c2.086-.481 4.53-3.576 2.714-9.587l-5.049 8.385M18.12 18.304c-.565 2.065 1.002 5.684 7.155 6.935l-4.989-8.421M8.628 10.506A11.376 11.376 0 0118.66 4.498M27.288 9.418a11.37 11.37 0 01.23 11.69M19.051 26.316a11.373 11.373 0 01-10.108-5.88M35.76 37.251H33.07a4.846 4.846 0 01-4.845-4.846V30.25"
}), /*#__PURE__*/React.createElement("path", {
  d: "M32.531 33.48l3.38 3.501-3.38 3.5M.856 37.251h2.692a4.846 4.846 0 004.846-4.846V30.25"
}), /*#__PURE__*/React.createElement("path", {
  d: "M4.086 33.48l-3.38 3.501 3.38 3.5M21.864 39.87l-3.555 3.433-3.556-3.434M18.309 43.171v-9.277"
}));

var SvgComponent$L = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$L({
    viewBox: "0 0 36.617 44.009",
    height: 166.333,
    width: 138.396
  }, props), _ref$L);
};

var ventilation = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2036.617176%2044.009004%22%20%20%20height%3D%2244.009003mm%22%20%20%20width%3D%2236.617176mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-12.20822%2C-878.92185%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%28-29.934187%2C430.83848%29%22%20%20%20%20%20%20%20id%3D%22g11136%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2074.914883%2C463.25282%20c%200%2C7.98849%20-6.475401%2C14.46389%20-14.463889%2C14.46389%20-7.988487%2C0%20-14.463888%2C-6.4754%20-14.463888%2C-14.46389%200%2C-7.98849%206.475401%2C-14.46389%2014.463888%2C-14.46389%207.988488%2C0%2014.463889%2C6.4754%2014.463889%2C14.46389%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9153%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2062.832245%2C463.25282%20c%200%2C1.51033%20-1.225076%2C2.73403%20-2.734028%2C2.73403%20-1.51033%2C0%20-2.734027%2C-1.2237%20-2.734027%2C-2.73403%200%2C-1.51033%201.223697%2C-2.73403%202.734027%2C-2.73403%201.508952%2C0%202.734028%2C1.2237%202.734028%2C2.73403%20z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path9155%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2057.816186%2C462.4949%20c%20-1.593012%2C-1.4304%20-5.528689%2C-1.65227%20-9.40511%2C3.28662%20l%209.7634%2C-0.68351%22%20%20%20%20%20%20%20%20%20id%3D%22path9157%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2063.027926%2C462.03326%20c%202.08635%2C-0.48094%204.529612%2C-3.57601%202.714735%2C-9.58701%20l%20-5.049131%2C8.38536%22%20%20%20%20%20%20%20%20%20id%3D%22path9159%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2060.262204%2C466.38786%20c%20-0.564996%2C2.0643%201.001833%2C5.68303%207.154774%2C6.93429%20l%20-4.988498%2C-8.42119%22%20%20%20%20%20%20%20%20%20id%3D%22path9161%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2050.770277%2C458.58954%20c%201.918229%2C-3.57601%205.691297%2C-6.00824%2010.033496%2C-6.00824%22%20%20%20%20%20%20%20%20%20id%3D%22path9163%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2069.430291%2C457.50088%20c%202.151117%2C3.44097%202.38814%2C7.92372%200.230132%2C11.69128%22%20%20%20%20%20%20%20%20%20id%3D%22path9165%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2061.193757%2C474.39977%20c%20-4.056945%2C0.0496%20-8.009158%2C-2.07808%20-10.10791%2C-5.88008%22%20%20%20%20%20%20%20%20%20id%3D%22path9167%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2077.90247%2C485.3345%20h%20-2.691308%20c%20-2.676151%2C0%20-4.845183%2C-2.17041%20-4.845183%2C-4.84656%20v%20-2.15388%22%20%20%20%20%20%20%20%20%20id%3D%22path9169%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2074.673726%2C481.56419%203.380328%2C3.50022%20-3.380328%2C3.50022%22%20%20%20%20%20%20%20%20%20id%3D%22path9171%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2042.998141%2C485.3345%20h%202.692687%20c%202.67615%2C0%204.845182%2C-2.17041%204.845182%2C-4.84656%20v%20-2.15388%22%20%20%20%20%20%20%20%20%20id%3D%22path9173%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2046.228263%2C481.56419%20-3.380328%2C3.50022%203.380328%2C3.50022%22%20%20%20%20%20%20%20%20%20id%3D%22path9175%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2064.006333%2C487.95278%20-3.555339%2C3.43407%20-3.555338%2C-3.43407%22%20%20%20%20%20%20%20%20%20id%3D%22path9177%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Around%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2060.450995%2C491.25455%20V%20481.9776%22%20%20%20%20%20%20%20%20%20id%3D%22path9179%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$M() { _extends$M = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$M.apply(this, arguments); }

var _ref$M = /*#__PURE__*/React.createElement("g", {
  fill: "none"
}, /*#__PURE__*/React.createElement("path", {
  d: "M26.017 1.842L22.786.764.706 12.344v33.929h2.692l22.62-11.849zm0 0M6.898 11.536v4.622M6.898 20.96v4.623M6.898 30.385v4.624M10.939 14.23v4.621M10.939 23.654v4.621M10.939 33.078V37.7M6.898 39.54v4.623M14.44 8.035v4.623M14.44 17.46v4.623M14.44 26.885v4.622M18.478 10.728v4.623M18.478 19.613v4.623M18.478 28.769v4.623M14.44 36.04v4.623M22.248 3.727V8.35M22.248 13.152v4.621M22.248 22.576v4.623M22.248 31.732v4.623"
}), /*#__PURE__*/React.createElement("path", {
  d: "M3.398 46.003V13.689l22.62-11.847M3.398 18.345l22.62-11.85M3.398 22.999l22.62-11.848M3.398 27.654l22.62-11.848M3.398 32.309l22.62-11.848M3.398 36.963l22.62-11.849M3.398 41.618l22.62-11.849"
}));

var SvgComponent$M = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$M({
    viewBox: "0 0 26.723 46.978",
    height: 177.555,
    width: 101
  }, props), _ref$M);
};

var wall = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2026.722916%2046.978219%22%20%20%20height%3D%2246.978218mm%22%20%20%20width%3D%2226.722916mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-17.469542%2C-400.80097%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cg%20%20%20%20%20%20%20transform%3D%22translate%28-29.934187%2C430.83848%29%22%20%20%20%20%20%20%20id%3D%22g11078%22%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2073.42109%2C-28.195553%20-3.231499%2C-1.077626%20-22.080306%2C11.579655%20v%2033.928677%20h%202.692687%20L%2073.42109%2C4.3867812%20Z%20m%200%2C0%22%20%20%20%20%20%20%20%20%20id%3D%22path8593%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2054.302189%2C-18.501054%20v%204.62194%22%20%20%20%20%20%20%20%20%20id%3D%22path8595%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2054.302189%2C-9.0766519%20v%204.6219401%22%20%20%20%20%20%20%20%20%20id%3D%22path8597%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2054.302189%2C0.34775106%20V%204.9710692%22%20%20%20%20%20%20%20%20%20id%3D%22path8599%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2058.342597%2C-15.808368%20v%204.62194%22%20%20%20%20%20%20%20%20%20id%3D%22path8601%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2058.342597%2C-6.3839653%20v%204.6219399%22%20%20%20%20%20%20%20%20%20id%3D%22path8603%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2058.342597%2C3.0404376%20v%204.623318%22%20%20%20%20%20%20%20%20%20id%3D%22path8605%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2054.302189%2C9.5034366%20V%2014.125377%22%20%20%20%20%20%20%20%20%20id%3D%22path8607%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2061.842813%2C-22.002649%20v%204.623318%22%20%20%20%20%20%20%20%20%20id%3D%22path8609%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2061.842813%2C-12.578247%20v%204.6233184%22%20%20%20%20%20%20%20%20%20id%3D%22path8611%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2061.842813%2C-3.1524659%20V%201.4694743%22%20%20%20%20%20%20%20%20%20id%3D%22path8613%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2065.881844%2C-19.309963%20v%204.623318%22%20%20%20%20%20%20%20%20%20id%3D%22path8615%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2065.881844%2C-10.424373%20v%204.6233179%22%20%20%20%20%20%20%20%20%20id%3D%22path8617%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2065.881844%2C-1.2686877%20V%203.3546302%22%20%20%20%20%20%20%20%20%20id%3D%22path8619%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2061.842813%2C6.0018416%20V%2010.62516%22%20%20%20%20%20%20%20%20%20id%3D%22path8621%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2069.652156%2C-26.310397%20v%204.62194%22%20%20%20%20%20%20%20%20%20id%3D%22path8623%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2069.652156%2C-16.885994%20v%204.62194%22%20%20%20%20%20%20%20%20%20id%3D%22path8625%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22m%2069.652156%2C-7.4615912%20v%204.623318%22%20%20%20%20%20%20%20%20%20id%3D%22path8627%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2069.652156%2C1.6940943%20V%206.3174124%22%20%20%20%20%20%20%20%20%20id%3D%22path8629%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2050.801971%2C15.965057%20V%20-16.34856%20L%2073.42109%2C-28.195553%22%20%20%20%20%20%20%20%20%20id%3D%22path8631%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2050.801971%2C-11.692168%2073.42109%2C-23.541918%22%20%20%20%20%20%20%20%20%20id%3D%22path8633%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2050.801971%2C-7.0385335%2073.42109%2C-18.886905%22%20%20%20%20%20%20%20%20%20id%3D%22path8635%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2050.801971%2C-2.3835206%2073.42109%2C-14.231893%22%20%20%20%20%20%20%20%20%20id%3D%22path8637%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2050.801971%2C2.2714923%2073.42109%2C-9.5768796%22%20%20%20%20%20%20%20%20%20id%3D%22path8639%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2050.801971%2C6.9251272%2073.42109%2C-4.9232449%22%20%20%20%20%20%20%20%20%20id%3D%22path8641%22%20%2F%3E%20%20%20%20%20%20%3Cpath%20%20%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%20%20%20%20%20%20%20%20d%3D%22M%2050.801971%2C11.58014%2073.42109%2C-0.26823175%22%20%20%20%20%20%20%20%20%20id%3D%22path8643%22%20%2F%3E%20%20%20%20%3C%2Fg%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function _extends$N() { _extends$N = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends$N.apply(this, arguments); }

var _ref$N = /*#__PURE__*/React.createElement("g", {
  fill: "none"
}, /*#__PURE__*/React.createElement("path", {
  d: "M7.024 39.239H.706v-26.6h6.318zm0 0M7.024 30.594c6.977 0 13.3-5.326 13.3-12.303M7.024 27.27c6.06 0 10.64-4.915 10.64-10.974"
}), /*#__PURE__*/React.createElement("path", {
  d: "M27.77 24.577l-17.5-14.321L17.853.993 38.905 18.22zm0 0"
}), /*#__PURE__*/React.createElement("path", {
  d: "M36.61 19.351l1.642 1.342-5.055 6.177-4.374-3.58M3.8 1.798s.785 2.829 4.449.51c3.663-2.317 6.222.32 6.598 2.403"
}));

var SvgComponent$N = function SvgComponent(props) {
  return /*#__PURE__*/React.createElement("svg", _extends$N({
    viewBox: "0 0 40.146 39.944",
    height: 150.971,
    width: 151.733
  }, props), _ref$N);
};

var camera = "data:image/svg+xml,%3C%3Fxml%20version%3D%221.0%22%20encoding%3D%22UTF-8%22%20standalone%3D%22no%22%3F%3E%3Csvg%20%20%20xmlns%3Adc%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Felements%2F1.1%2F%22%20%20%20xmlns%3Acc%3D%22http%3A%2F%2Fcreativecommons.org%2Fns%23%22%20%20%20xmlns%3Ardf%3D%22http%3A%2F%2Fwww.w3.org%2F1999%2F02%2F22-rdf-syntax-ns%23%22%20%20%20xmlns%3Asvg%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20%20%20id%3D%22svg8296%22%20%20%20version%3D%221.1%22%20%20%20viewBox%3D%220%200%2040.145897%2039.944482%22%20%20%20height%3D%2239.944481mm%22%20%20%20width%3D%2240.145897mm%22%3E%20%20%3Cdefs%20%20%20%20%20id%3D%22defs8290%22%20%2F%3E%20%20%3Cmetadata%20%20%20%20%20id%3D%22metadata8293%22%3E%20%20%20%20%3Crdf%3ARDF%3E%20%20%20%20%20%20%3Ccc%3AWork%20%20%20%20%20%20%20%20%20rdf%3Aabout%3D%22%22%3E%20%20%20%20%20%20%20%20%3Cdc%3Aformat%3Eimage%2Fsvg%2Bxml%3C%2Fdc%3Aformat%3E%20%20%20%20%20%20%20%20%3Cdc%3Atype%20%20%20%20%20%20%20%20%20%20%20rdf%3Aresource%3D%22http%3A%2F%2Fpurl.org%2Fdc%2Fdcmitype%2FStillImage%22%20%2F%3E%20%20%20%20%20%20%20%20%3Cdc%3Atitle%3E%3C%2Fdc%3Atitle%3E%20%20%20%20%20%20%3C%2Fcc%3AWork%3E%20%20%20%20%3C%2Frdf%3ARDF%3E%20%20%3C%2Fmetadata%3E%20%20%3Cg%20%20%20%20%20transform%3D%22translate%28-10.711641%2C-1111.5272%29%22%20%20%20%20%20id%3D%22layer1%22%3E%20%20%20%20%3Cpath%20%20%20%20%20%20%20id%3D%22path9353%22%20%20%20%20%20%20%20d%3D%22m%2017.735503%2C1150.7661%20h%20-6.318306%20v%20-26.6003%20h%206.318306%20z%20m%200%2C0%22%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%2F%3E%20%20%20%20%3Cpath%20%20%20%20%20%20%20id%3D%22path9355%22%20%20%20%20%20%20%20d%3D%22m%2017.735503%2C1142.1217%20c%206.977008%2C0%2013.299447%2C-5.3262%2013.299447%2C-12.3032%22%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%2F%3E%20%20%20%20%3Cpath%20%20%20%20%20%20%20id%3D%22path9357%22%20%20%20%20%20%20%20d%3D%22m%2017.735503%2C1138.7965%20c%206.059234%2C0%2010.639833%2C-4.9141%2010.639833%2C-10.9734%22%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%2F%3E%20%20%20%20%3Cpath%20%20%20%20%20%20%20id%3D%22path9359%22%20%20%20%20%20%20%20d%3D%22m%2038.481869%2C1136.1038%20-17.499707%2C-14.3206%207.581966%2C-9.2632%2021.052289%2C17.2283%20z%20m%200%2C0%22%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%2F%3E%20%20%20%20%3Cpath%20%20%20%20%20%20%20id%3D%22path9361%22%20%20%20%20%20%20%20d%3D%22m%2047.321983%2C1130.8782%201.641243%2C1.3423%20-5.054644%2C6.1763%20-4.373893%2C-3.5801%22%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%2F%3E%20%20%20%20%3Cpath%20%20%20%20%20%20%20id%3D%22path9363%22%20%20%20%20%20%20%20d%3D%22m%2014.512272%2C1113.3248%20c%200%2C0%200.784104%2C2.8291%204.448307%2C0.5112%203.662826%2C-2.3178%206.221843%2C0.3184%206.598047%2C2.402%22%20%20%20%20%20%20%20style%3D%22fill%3Anone%3Bstroke-width%3A1.41111112%3Bstroke-linecap%3Abutt%3Bstroke-linejoin%3Amiter%3Bstroke-miterlimit%3A10%3Bstroke-opacity%3A1%22%20%2F%3E%20%20%3C%2Fg%3E%3C%2Fsvg%3E";

function ownKeys$1(object, enumerableOnly) { var keys = keys$3(object); if (getOwnPropertySymbols$2) { var symbols = getOwnPropertySymbols$2(object); if (enumerableOnly) symbols = filter$2(symbols).call(symbols, function (sym) { return getOwnPropertyDescriptor$3(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { var _context; forEach$2(_context = ownKeys$1(Object(source), true)).call(_context, function (key) { defineProperty$8(target, key, source[key]); }); } else if (getOwnPropertyDescriptors$2) { defineProperties$1(target, getOwnPropertyDescriptors$2(source)); } else { var _context2; forEach$2(_context2 = ownKeys$1(Object(source))).call(_context2, function (key) { defineProperty$1(target, key, getOwnPropertyDescriptor$3(source, key)); }); } } return target; }

var getWrappedComponentWithStroke = function getWrappedComponentWithStroke(SvgComponent) {
  return function (_ref) {
    var color = _ref.color,
        style = _ref.style,
        rest = objectWithoutProperties(_ref, ["color", "style"]);

    return /*#__PURE__*/React.createElement(SvgComponent, _extends_1({}, rest, {
      style: _objectSpread({}, style, {
        stroke: color || '#000'
      })
    }));
  };
};

var getWrappedComponentFill = function getWrappedComponentFill(SvgComponent) {
  return function (_ref2) {
    var color = _ref2.color,
        style = _ref2.style,
        rest = objectWithoutProperties(_ref2, ["color", "style"]);

    return /*#__PURE__*/React.createElement(SvgComponent, _extends_1({}, rest, {
      style: _objectSpread({}, style, {
        fill: color || '#000',
        color: color || '#000'
      })
    }));
  };
};

var CraneComponent = getWrappedComponentWithStroke(SvgComponent$A);
var DemolitionComponent = getWrappedComponentWithStroke(SvgComponent$B);
var ElectricComponent = getWrappedComponentWithStroke(SvgComponent$C);
var FinishingComponent = getWrappedComponentWithStroke(SvgComponent$D);
var FloorCementComponent = getWrappedComponentWithStroke(SvgComponent$E);
var LayersComponent = getWrappedComponentWithStroke(SvgComponent$F);
var PaintWallComponent = getWrappedComponentWithStroke(SvgComponent$G);
var PneumaticHammerComponent = getWrappedComponentWithStroke(SvgComponent$H);
var PlumbingComponent = getWrappedComponentWithStroke(SvgComponent$I);
var TilesComponent = getWrappedComponentWithStroke(SvgComponent$J);
var TrashBinComponent = getWrappedComponentWithStroke(SvgComponent$K);
var VentilationComponent = getWrappedComponentWithStroke(SvgComponent$L);
var WallComponent = getWrappedComponentWithStroke(SvgComponent$M);
var CameraComponent = getWrappedComponentWithStroke(SvgComponent$N);
var BlocksLightComponent = getWrappedComponentFill(SvgComponent);
var BlocksRegularComponent = getWrappedComponentFill(SvgComponent$1);
var BlocksSolidComponent = getWrappedComponentFill(SvgComponent$2);
var CameraLightComponent = getWrappedComponentFill(SvgComponent$3);
var CameraRegularComponent = getWrappedComponentFill(SvgComponent$4);
var CameraSolidComponent = getWrappedComponentFill(SvgComponent$5);
var ClockLightComponent = getWrappedComponentFill(SvgComponent$6);
var ClockRegularComponent = getWrappedComponentFill(SvgComponent$7);
var ClockSolidComponent = getWrappedComponentFill(SvgComponent$8);
var CloudSolidComponent = getWrappedComponentFill(SvgComponent$9);
var CubeSolidComponent = getWrappedComponentFill(SvgComponent$a);
var DiogrammRegularComponent = getWrappedComponentFill(SvgComponent$b);
var HomeLightComponent = getWrappedComponentFill(SvgComponent$c);
var HomeRegularComponent = getWrappedComponentFill(SvgComponent$d);
var HomeSolidComponent = getWrappedComponentFill(SvgComponent$e);
var ImagesRegularComponent = getWrappedComponentFill(SvgComponent$f);
var IssueLightComponent = getWrappedComponentFill(SvgComponent$g);
var IssueRegularComponent = getWrappedComponentFill(SvgComponent$h);
var IssueSolidComponent = getWrappedComponentFill(SvgComponent$i);
var PrescriptionLightComponent = getWrappedComponentFill(SvgComponent$j);
var PrescriptionRegularComponent = getWrappedComponentFill(SvgComponent$k);
var PrescriptionSolidComponent = getWrappedComponentFill(SvgComponent$l);
var RubLightComponent = getWrappedComponentFill(SvgComponent$m);
var RubRegularComponent = getWrappedComponentFill(SvgComponent$n);
var RubSolidComponent = getWrappedComponentFill(SvgComponent$o);
var UserLightComponent = getWrappedComponentFill(SvgComponent$p);
var UserRegularComponent = getWrappedComponentFill(SvgComponent$q);
var UserSolidComponent = getWrappedComponentFill(SvgComponent$r);
var ChartBarsComponent = getWrappedComponentFill(SvgComponent$s);
var DoughnutChartComponent = getWrappedComponentFill(SvgComponent$t);
var PersonMessageComponent = getWrappedComponentFill(SvgComponent$u);
var PiechartComponent = getWrappedComponentFill(SvgComponent$v);
var WebcamComponent = getWrappedComponentFill(SvgComponent$w);
var ImportantComponent = getWrappedComponentFill(SvgComponent$x);
var PaletteSolidComponent = getWrappedComponentFill(SvgComponent$y);
var TruckComponent = getWrappedComponentFill(SvgComponent$z);

var ChartLine = function ChartLine(_ref3) {
  var color = _ref3.color,
      rest = objectWithoutProperties(_ref3, ["color"]);

  return /*#__PURE__*/React.createElement("svg", _extends_1({}, rest, {
    width: "20px",
    height: "14px",
    viewBox: "0 0 20 14",
    version: "1.1"
  }), /*#__PURE__*/React.createElement("title", null, "show_chart"), /*#__PURE__*/React.createElement("desc", null, "Created with Sketch."), /*#__PURE__*/React.createElement("g", {
    id: "Icons",
    stroke: "none",
    strokeWidth: "1",
    fill: "none",
    fillRule: "evenodd"
  }, /*#__PURE__*/React.createElement("g", {
    id: "Outlined",
    transform: "translate(-510.000000, -2107.000000)"
  }, /*#__PURE__*/React.createElement("g", {
    id: "Editor",
    transform: "translate(100.000000, 1960.000000)"
  }, /*#__PURE__*/React.createElement("g", {
    id: "Outlined-/-Editor-/-show_chart",
    transform: "translate(408.000000, 142.000000)"
  }, /*#__PURE__*/React.createElement("g", null, /*#__PURE__*/React.createElement("polygon", {
    id: "Path",
    points: "0 0 24 0 24 24 0 24"
  }), /*#__PURE__*/React.createElement("polygon", {
    id: "\uD83D\uDD39-Icon-Color",
    fill: color || '#000',
    points: "3.5 18.49 9.5 12.48 13.5 16.48 22 6.92 20.59 5.51 13.5 13.48 9.5 9.48 2 16.99"
  })))))));
};
var index = '';
var iconNames = ['blocksLight', 'blocksRegular', 'blocksSolid', 'cameraLight', 'cameraRegular', 'cameraSolid', 'clockLight', 'clockRegular', 'clockSolid', 'cloudSolid', 'cubeSolid', 'diagramRegular', 'homeLight', 'homeRegular', 'homeSolid', 'imagesRegular', 'issueLight', 'issueRegular', 'issueSolid', 'prescriptionLight', 'prescriptionRegular', 'prescriptionSolid', 'rubLight', 'rubRegular', 'rubSolid', 'userLight', 'userRegular', 'userSolid', 'chartBars', 'chartLine', 'doughnutChart', 'personMessage', 'piechart', 'webcam', 'important', 'paletteSolid', 'truck', 'camera', 'demolition', 'finishing', 'layers', 'pneumaticHammer', 'tiles', 'wall', 'crane', 'electric', 'floorCement', 'paintWall', 'plumbing', 'trashBin', 'ventilation'];
var iconsMap = {
  chartBars: {
    src: chart_bars,
    Component: ChartBarsComponent
  },
  chartLine: {
    src: chart_line,
    Component: ChartLine
  },
  doughnutChart: {
    src: doughnut_chart,
    Component: DoughnutChartComponent
  },
  personMessage: {
    src: person_message,
    Component: PersonMessageComponent
  },
  piechart: {
    src: piechart,
    Component: PiechartComponent
  },
  webcam: {
    src: webcam,
    Component: WebcamComponent
  },
  important: {
    src: important,
    Component: ImportantComponent
  },
  blocksLight: {
    src: blocks_light,
    Component: BlocksLightComponent
  },
  blocksRegular: {
    src: blocks_regular,
    Component: BlocksRegularComponent
  },
  blocksSolid: {
    src: blocks_solid,
    Component: BlocksSolidComponent
  },
  cameraLight: {
    src: camera_light,
    Component: CameraLightComponent
  },
  cameraRegular: {
    src: camera_regular,
    Component: CameraRegularComponent
  },
  cameraSolid: {
    src: camera_solid,
    Component: CameraSolidComponent
  },
  clockLight: {
    src: clock_light,
    Component: ClockLightComponent
  },
  clockRegular: {
    src: clock_regular,
    Component: ClockRegularComponent
  },
  clockSolid: {
    src: clock_solid,
    Component: ClockSolidComponent
  },
  cloudSolid: {
    src: cloud_solid,
    Component: CloudSolidComponent
  },
  cubeSolid: {
    src: cube_solid,
    Component: CubeSolidComponent
  },
  diagramRegular: {
    src: diogramm_regular,
    Component: DiogrammRegularComponent
  },
  homeLight: {
    src: home_light,
    Component: HomeLightComponent
  },
  homeRegular: {
    src: home_regular,
    Component: HomeRegularComponent
  },
  homeSolid: {
    src: home_solid,
    Component: HomeSolidComponent
  },
  imagesRegular: {
    src: images_regular,
    Component: ImagesRegularComponent
  },
  issueLight: {
    src: issue_light,
    Component: IssueLightComponent
  },
  issueRegular: {
    src: issue_regular,
    Component: IssueRegularComponent
  },
  issueSolid: {
    src: issue_solid,
    Component: IssueSolidComponent
  },
  prescriptionLight: {
    src: prescription_light,
    Component: PrescriptionLightComponent
  },
  prescriptionRegular: {
    src: prescription_regular,
    Component: PrescriptionRegularComponent
  },
  prescriptionSolid: {
    src: prescription_solid,
    Component: PrescriptionSolidComponent
  },
  rubLight: {
    src: rub_light,
    Component: RubLightComponent
  },
  rubRegular: {
    src: rub_regular,
    Component: RubRegularComponent
  },
  rubSolid: {
    src: rub_solid,
    Component: RubSolidComponent
  },
  userLight: {
    src: user_light,
    Component: UserLightComponent
  },
  userRegular: {
    src: user_regular,
    Component: UserRegularComponent
  },
  userSolid: {
    src: user_solid,
    Component: UserSolidComponent
  },
  paletteSolid: {
    src: palette_solid,
    Component: PaletteSolidComponent
  },
  truck: {
    src: truck,
    Component: TruckComponent
  },
  camera: {
    src: camera,
    Component: CameraComponent
  },
  demolition: {
    src: demolition,
    Component: DemolitionComponent
  },
  finishing: {
    src: finishing,
    Component: FinishingComponent
  },
  layers: {
    src: layers,
    Component: LayersComponent
  },
  pneumaticHammer: {
    src: pneumaticHammer,
    Component: PneumaticHammerComponent
  },
  tiles: {
    src: tiles,
    Component: TilesComponent
  },
  wall: {
    src: wall,
    Component: WallComponent
  },
  crane: {
    src: crane,
    Component: CraneComponent
  },
  electric: {
    src: electric,
    Component: ElectricComponent
  },
  floorCement: {
    src: floorCement,
    Component: FloorCementComponent
  },
  paintWall: {
    src: paintWall,
    Component: PaintWallComponent
  },
  plumbing: {
    src: plumbing,
    Component: PlumbingComponent
  },
  trashBin: {
    src: trashBin,
    Component: TrashBinComponent
  },
  ventilation: {
    src: ventilation,
    Component: VentilationComponent
  }
};
var getIcons = function getIcons() {
  return iconsMap;
};

export default index;
export { BlocksLightComponent, BlocksRegularComponent, BlocksSolidComponent, CameraComponent, CameraLightComponent, CameraRegularComponent, CameraSolidComponent, ChartBarsComponent, ChartLine, ClockLightComponent, ClockRegularComponent, ClockSolidComponent, CloudSolidComponent, CraneComponent, CubeSolidComponent, DemolitionComponent, DiogrammRegularComponent, DoughnutChartComponent, ElectricComponent, FinishingComponent, FloorCementComponent, HomeLightComponent, HomeRegularComponent, HomeSolidComponent, ImagesRegularComponent, ImportantComponent, IssueLightComponent, IssueRegularComponent, IssueSolidComponent, LayersComponent, PaintWallComponent, PaletteSolidComponent, PersonMessageComponent, PiechartComponent, PlumbingComponent, PneumaticHammerComponent, PrescriptionLightComponent, PrescriptionRegularComponent, PrescriptionSolidComponent, RubLightComponent, RubRegularComponent, RubSolidComponent, TilesComponent, TrashBinComponent, TruckComponent, UserLightComponent, UserRegularComponent, UserSolidComponent, VentilationComponent, WallComponent, WebcamComponent, getIcons, iconNames };
//# sourceMappingURL=index.es.js.map
