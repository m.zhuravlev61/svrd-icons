import React, { FunctionComponent } from 'react';
interface WrapperProps extends React.SVGAttributes<SVGElement> {
    color?: string;
}
declare const CraneComponent: React.FunctionComponent<WrapperProps>;
declare const DemolitionComponent: React.FunctionComponent<WrapperProps>;
declare const ElectricComponent: React.FunctionComponent<WrapperProps>;
declare const FinishingComponent: React.FunctionComponent<WrapperProps>;
declare const FloorCementComponent: React.FunctionComponent<WrapperProps>;
declare const LayersComponent: React.FunctionComponent<WrapperProps>;
declare const PaintWallComponent: React.FunctionComponent<WrapperProps>;
declare const PneumaticHammerComponent: React.FunctionComponent<WrapperProps>;
declare const PlumbingComponent: React.FunctionComponent<WrapperProps>;
declare const TilesComponent: React.FunctionComponent<WrapperProps>;
declare const TrashBinComponent: React.FunctionComponent<WrapperProps>;
declare const VentilationComponent: React.FunctionComponent<WrapperProps>;
declare const WallComponent: React.FunctionComponent<WrapperProps>;
declare const CameraComponent: React.FunctionComponent<WrapperProps>;
declare const BlocksLightComponent: React.FunctionComponent<WrapperProps>;
declare const BlocksRegularComponent: React.FunctionComponent<WrapperProps>;
declare const BlocksSolidComponent: React.FunctionComponent<WrapperProps>;
declare const CameraLightComponent: React.FunctionComponent<WrapperProps>;
declare const CameraRegularComponent: React.FunctionComponent<WrapperProps>;
declare const CameraSolidComponent: React.FunctionComponent<WrapperProps>;
declare const ClockLightComponent: React.FunctionComponent<WrapperProps>;
declare const ClockRegularComponent: React.FunctionComponent<WrapperProps>;
declare const ClockSolidComponent: React.FunctionComponent<WrapperProps>;
declare const CloudSolidComponent: React.FunctionComponent<WrapperProps>;
declare const CubeSolidComponent: React.FunctionComponent<WrapperProps>;
declare const DiogrammRegularComponent: React.FunctionComponent<WrapperProps>;
declare const HomeLightComponent: React.FunctionComponent<WrapperProps>;
declare const HomeRegularComponent: React.FunctionComponent<WrapperProps>;
declare const HomeSolidComponent: React.FunctionComponent<WrapperProps>;
declare const ImagesRegularComponent: React.FunctionComponent<WrapperProps>;
declare const IssueLightComponent: React.FunctionComponent<WrapperProps>;
declare const IssueRegularComponent: React.FunctionComponent<WrapperProps>;
declare const IssueSolidComponent: React.FunctionComponent<WrapperProps>;
declare const PrescriptionLightComponent: React.FunctionComponent<WrapperProps>;
declare const PrescriptionRegularComponent: React.FunctionComponent<WrapperProps>;
declare const PrescriptionSolidComponent: React.FunctionComponent<WrapperProps>;
declare const RubLightComponent: React.FunctionComponent<WrapperProps>;
declare const RubRegularComponent: React.FunctionComponent<WrapperProps>;
declare const RubSolidComponent: React.FunctionComponent<WrapperProps>;
declare const UserLightComponent: React.FunctionComponent<WrapperProps>;
declare const UserRegularComponent: React.FunctionComponent<WrapperProps>;
declare const UserSolidComponent: React.FunctionComponent<WrapperProps>;
declare const ChartBarsComponent: React.FunctionComponent<WrapperProps>;
declare const DoughnutChartComponent: React.FunctionComponent<WrapperProps>;
declare const PersonMessageComponent: React.FunctionComponent<WrapperProps>;
declare const PiechartComponent: React.FunctionComponent<WrapperProps>;
declare const WebcamComponent: React.FunctionComponent<WrapperProps>;
declare const ImportantComponent: React.FunctionComponent<WrapperProps>;
declare const PaletteSolidComponent: React.FunctionComponent<WrapperProps>;
declare const TruckComponent: React.FunctionComponent<WrapperProps>;
interface ChartLineProps extends React.SVGAttributes<SVGElement> {
    color?: string;
}
declare const ChartLine: FunctionComponent<ChartLineProps>;
export { ChartLine, BlocksLightComponent, BlocksRegularComponent, BlocksSolidComponent, CameraLightComponent, CameraRegularComponent, CameraSolidComponent, ClockLightComponent, ClockRegularComponent, ClockSolidComponent, CloudSolidComponent, CubeSolidComponent, DiogrammRegularComponent, HomeLightComponent, HomeRegularComponent, HomeSolidComponent, ImagesRegularComponent, IssueLightComponent, IssueRegularComponent, IssueSolidComponent, PrescriptionLightComponent, PrescriptionRegularComponent, PrescriptionSolidComponent, RubLightComponent, RubRegularComponent, RubSolidComponent, UserLightComponent, UserRegularComponent, UserSolidComponent, ChartBarsComponent, DoughnutChartComponent, PersonMessageComponent, PiechartComponent, WebcamComponent, ImportantComponent, PaletteSolidComponent, TruckComponent, CraneComponent, DemolitionComponent, ElectricComponent, FinishingComponent, FloorCementComponent, LayersComponent, PaintWallComponent, PneumaticHammerComponent, PlumbingComponent, TilesComponent, TrashBinComponent, VentilationComponent, WallComponent, CameraComponent, };
declare const _default: "";
export default _default;
export interface Icon {
    src: string;
    Component: FunctionComponent<WrapperProps>;
}
export declare const iconNames: readonly ["blocksLight", "blocksRegular", "blocksSolid", "cameraLight", "cameraRegular", "cameraSolid", "clockLight", "clockRegular", "clockSolid", "cloudSolid", "cubeSolid", "diagramRegular", "homeLight", "homeRegular", "homeSolid", "imagesRegular", "issueLight", "issueRegular", "issueSolid", "prescriptionLight", "prescriptionRegular", "prescriptionSolid", "rubLight", "rubRegular", "rubSolid", "userLight", "userRegular", "userSolid", "chartBars", "chartLine", "doughnutChart", "personMessage", "piechart", "webcam", "important", "paletteSolid", "truck", "camera", "demolition", "finishing", "layers", "pneumaticHammer", "tiles", "wall", "crane", "electric", "floorCement", "paintWall", "plumbing", "trashBin", "ventilation"];
export declare type IconNames = typeof iconNames[number];
export declare type CardIcons = Record<IconNames, Icon>;
export declare const getIcons: () => Record<"camera" | "blocksLight" | "blocksRegular" | "blocksSolid" | "cameraLight" | "cameraRegular" | "cameraSolid" | "clockLight" | "clockRegular" | "clockSolid" | "cloudSolid" | "cubeSolid" | "diagramRegular" | "homeLight" | "homeRegular" | "homeSolid" | "imagesRegular" | "issueLight" | "issueRegular" | "issueSolid" | "prescriptionLight" | "prescriptionRegular" | "prescriptionSolid" | "rubLight" | "rubRegular" | "rubSolid" | "userLight" | "userRegular" | "userSolid" | "chartBars" | "chartLine" | "doughnutChart" | "personMessage" | "piechart" | "webcam" | "important" | "paletteSolid" | "truck" | "demolition" | "finishing" | "layers" | "pneumaticHammer" | "tiles" | "wall" | "crane" | "electric" | "floorCement" | "paintWall" | "plumbing" | "trashBin" | "ventilation", Icon>;
