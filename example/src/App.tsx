import React, { Component } from 'react';

import {
  BlocksLightComponent,
  BlocksRegularComponent,
  BlocksSolidComponent,
  CameraLightComponent,
  CameraRegularComponent,
  CameraSolidComponent,
  ClockLightComponent,
  ClockRegularComponent,
  ClockSolidComponent,
  CloudSolidComponent,
  CubeSolidComponent,
  DiogrammRegularComponent,
  HomeLightComponent,
  HomeRegularComponent,
  HomeSolidComponent,
  ImagesRegularComponent,
  IssueLightComponent,
  IssueRegularComponent,
  IssueSolidComponent,
  PrescriptionLightComponent,
  PrescriptionRegularComponent,
  PrescriptionSolidComponent,
  RubLightComponent,
  RubRegularComponent,
  RubSolidComponent,
  UserLightComponent,
  UserRegularComponent,
  UserSolidComponent,
  ChartBarsComponent,
  ChartLine,
  DoughnutChartComponent,
  PersonMessageComponent,
  PiechartComponent,
  WebcamComponent,
  ImportantComponent,
  PaletteSolidComponent,
  TruckComponent,
  CraneComponent,
  DemolitionComponent,
  ElectricComponent,
  FinishingComponent,
  FloorCementComponent,
  LayersComponent,
  PaintWallComponent,
  PneumaticHammerComponent,
  PlumbingComponent,
  TilesComponent,
  TrashBinComponent,
  VentilationComponent,
  WallComponent,
  CameraComponent,
} from 'cardicons';

export default class App extends Component {
  render() {
    return (
      <div className="icons-list">
        <div className="icon-def">
          <span className="icon-img">
            <BlocksLightComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">BlocksLight</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <BlocksRegularComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">BlocksRegular</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <BlocksSolidComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">BlocksSolid</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <CameraLightComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">CameraLight</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <CameraRegularComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">CameraRegular</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <CameraSolidComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">CameraSolid</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <ClockLightComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">ClockLight</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <ClockRegularComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">ClockRegular</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <ClockSolidComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">ClockSolid</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <CloudSolidComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">CloudSolid</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <CubeSolidComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">CubeSolid</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <DiogrammRegularComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">DiogrammRegular</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <HomeLightComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">HomeLight</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <HomeRegularComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">HomeRegular</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <HomeSolidComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">HomeSolid</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <ImagesRegularComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">ImagesRegular</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <IssueLightComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">IssueLight</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <IssueRegularComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">IssueRegular</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <IssueSolidComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">IssueSolid</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <PrescriptionLightComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">PrescriptionLight</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <PrescriptionRegularComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">PrescriptionRegular</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <PrescriptionSolidComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">PrescriptionSolid</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <RubLightComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">RubLight</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <RubRegularComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">RubRegular</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <RubSolidComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">RubSolid</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <UserLightComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">UserLight</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <UserRegularComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">UserRegular</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <UserSolidComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">UserSolid</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <ChartBarsComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">ChartBars</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <ChartLine color={'red'} className="svg-icon blocks_light-svg" />
          </span>
          <span className="icon-name">ChartLine</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <DoughnutChartComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">DoughnutChart</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <PersonMessageComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">PersonMessage</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <PiechartComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Piechart</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <WebcamComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Webcam</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <ImportantComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Important</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <PaletteSolidComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">PaletteSolid</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <TruckComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Truck</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <CraneComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Crane</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <DemolitionComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Demolition</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <ElectricComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Electric</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <FinishingComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Finishing</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <FloorCementComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">FloorCement</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <LayersComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Layers</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <PaintWallComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">PaintWall</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <PneumaticHammerComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">PneumaticHammer</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <PlumbingComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Plumbing</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <TilesComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Tiles</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <TrashBinComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">TrashBin</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <VentilationComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Ventilation</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <WallComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Wall</span>
        </div>
        <div className="icon-def">
          <span className="icon-img">
            <CameraComponent
              color={'red'}
              className="svg-icon blocks_light-svg"
            />
          </span>
          <span className="icon-name">Camera</span>
        </div>
      </div>
    );
  }
}
